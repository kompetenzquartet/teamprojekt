import { mapState } from "vuex";

export default {
  computed: {
    ...mapState("globalSettingStore", ["globalSettings"]),
    getCompletedTasks() {
      return this.chapter.tasks.filter(task =>
        this.isTaskSolvedAlready(
          this.user,
          this.$route.params.semesterId,
          this.$route.params.courseId,
          this.$route.params.chapterId,
          task
        )
      ).length;
    }
  },
  methods: {
    isTaskSolvedAlready(user, semesterId, courseId, chapterId, task) {
      const solved = user.solved.find(
        solve =>
          solve.semesterId === semesterId &&
          solve.courseId === courseId &&
          solve.chapterId === chapterId &&
          solve.taskId === task._id
      );
      if (solved) {
        if(task.tag === "multiple-choice-task" || task.tag === "single-choice-task"){
          return true
        } else {
          return ( task.data.solutions.length === Object.keys(solved.result).length ||
          (Object.keys(solved.result).length > 0 && task.data.solutions.length === 0));
        }
      }
      return false;
    },
    getNextIncompleteTask(user, tasks) {
      return tasks.find(
        task =>
          !this.isTaskSolvedAlready(
            user,
            this.$route.params.semesterId,
            this.$route.params.courseId,
            this.$route.params.chapterId,
            task
          )
      );
    },
    getTaskTime(task) {
      if (task.data.taskTime) {
        return task.data.taskTime;
      }
      switch (task.tag) {
        case "koan-task":
          return this.globalSettings.koanTime;
        case "code-task":
          return this.globalSettings.codetaskTime;
        default:
          return 0;
      }
    }
  }
};
