import { cloneDeep } from "lodash";
import store from "@/store/store";

export default {
  methods: {
    isUserStudent(user) {
      return user.permissions === "student";
    },
    isUserCasual(user) {
      return user.permissions === "casual";
    },
    isUserAdmin(user) {
      return user.permissions === "admin";
    },
    async saveProgress(
      user,
      semesterId,
      courseId,
      chapterId,
      taskId,
      result,
      solvedWith = {}
    ) {
      const updatedUser = cloneDeep(user);
      let solvedObject = updatedUser.solved.find(
        solved =>
          solved.semesterId === semesterId &&
          solved.courseId === courseId &&
          solved.chapterId === chapterId &&
          solved.taskId === taskId
      );
      if (solvedObject) {
        Object.assign(solvedObject.result, result);
        Object.assign(solvedObject.solvedWith, solvedWith);
        updatedUser.solved = updatedUser.solved.map(solved =>
          solved._id === solvedObject._id ? solvedObject : solved
        );
      } else {
        solvedObject = {
          userId: updatedUser._id,
          semesterId: semesterId,
          courseId: courseId,
          chapterId: chapterId,
          taskId: taskId,
          result: result,
          solvedWith: solvedWith
        };
        updatedUser.solved.push(solvedObject);
      }
      return store.dispatch("userStore/updateUser", updatedUser, {
        root: true
      });
    }
  }
};
