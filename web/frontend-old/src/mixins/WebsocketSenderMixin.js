// sending messages from front to backend
export default {
  methods: {
    messageHandler: function(msg) {
      switch (msg) {
        case "submit":
          this.$socket.sendObj({ type: "submit", msg: "submit" });
          break;
        default:
          this.$notify({
            title: "Fehler",
            message: "Nachricht konnte nicht übertragen werden.",
            type: "error"
          });
          break;
      }
    }
  }
};
