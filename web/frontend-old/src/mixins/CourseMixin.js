export default {
  methods: {
    numberOfChaptersInCourse: function(course) {
      return course.chapters.length;
    },
    getOverallProgressOfCourse(user, course, semester) {
      const totalTasks = course.chapters.reduce((total, chapter) => {
        return total + chapter.tasks.length;
      }, 0);
      const totalSolved = user.solved.filter(
        solved =>
          solved.courseId === course._id && solved.semesterId === semester._id
      ).length;
      const total = (totalSolved / totalTasks) * 100;
      return Math.round(total * 100) / 100;
    },
    getOverallProgressOfCourseCount(user, course, semester) {
      return user.solved.filter(
        solved =>
          solved.courseId === course._id && solved.semesterId === semester._id
      ).length;
    },
    getCoursesSortedByName(courses) {
      return courses
        .map(course => course)
        .sort((a, b) => {
          if (a.title < b.title) {
            return -1;
          }
          if (a.title > b.title) {
            return 1;
          }
          return 0;
        });
    }
  }
};
