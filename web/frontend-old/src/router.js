import Vue from "vue";
import store from "@/store/store";
import Router from "vue-router";
import AuthPage from "@/pages/AuthPage";
import DashboardPage from "@/pages/DashboardPage";
import AppShell from "@/components/shells/AppShell";
import AdminShell from "./components/shells/AdminShell";
import QuizduellPage from "./pages/QuizduellPage";
import EditSemester from "@/components/adminpage/EditSemester";
import RankingPoints from "@/components/adminpage/statistics/RankingPoints.vue";
import Ide from "@/components/ide/Ide";
import EditLecture from "@/components/adminpage/EditLecture";
import DashboardOverview from "@/components/dashboard/DashboardOverview";
import AssignKoans from "@/components/adminpage/koans/AssignKoans";
import ManageKoans from "@/components/adminpage/koans/ManageKoans";
import SynchronizeKoans from "@/components/adminpage/koans/SynchronizeKoans";
import UserShell from "@/components/shells/user/UserShell";
import Profile from "@/components/user/Profile";
import Settings from "@/components/user/Settings";

// Learning
import CoursePage from "@/components/learn/CoursePage";
import ChapterStart from "@/components/learn/chapter/ChapterStart";
import ChapterPanel from "@/components/learn/chapter/ChapterPanel";
import BaseTask from "@/components/learn/task/BaseTask";
import LectureShell from "@/components/shells/lectures/LectureShell";
import LectureOverview from "@/components/learn/LectureOverview";
import DashboardStatistics from "@/components/dashboard/DashboardStatistics";
import HTWGLogin from "@/components/login/HTWGLogin";
import EditQuizTaskSet from "@/components/adminpage/quiz/EditQuizTaskSet";
import StartQuiz from "@/components/adminpage/quiz/EditQuiz";
import ManageQuiz from "@/components/adminpage/quiz/ManageQuiz";
import SettingsQuiz from "@/components/adminpage/quiz/SettingsQuiz";
import DashboardCalendar from "@/components/dashboard/DashboardCalendar.vue";
import ManageCourseDeadlines from "@/components/adminpage/koans/ManageCourseDeadlines.vue";
import ManageQuizduellDeadlines from "@/components/adminpage/quiz/ManageQuizduellDeadlines.vue";
import ManageStatistics from "@/components/adminpage/statistics/ManageStatistics.vue";
import UserSpecificScore from "@/components/adminpage/statistics/UserSpecificScore.vue";
import AdminDashboard from "./components/adminpage/statistics/AdminDashboard.vue";

Vue.use(Router);

const router = new Router({
  mode: "history",
  routes: [
    {
      path: "/auth",
      component: AuthPage,
      children: [
        {
          path: "login",
          name: "student-login",
          component: HTWGLogin
        },
        {
          path: "*",
          redirect: { name: "student-login" }
        }
      ]
    },
    {
      path: "/",
      name: "app",
      component: AppShell,
      redirect: { name: "dashboard" },
      meta: {
        requiresAuth: true
      },
      children: [
        {
          path: "dashboard",
          component: DashboardPage,
          children: [
            {
              path: "",
              component: DashboardOverview,
              name: "dashboard"
            },
            {
              path: "statistics",
              component: DashboardStatistics,
              name: "dashboard-statistics"
            },
            {
              path: "calendar",
              component: DashboardCalendar,
              name: "dashboard-calendar"
            }
          ]
        },
        {
          path: "admin",
          component: AdminShell,
          name: "admin",
          meta: {
            requiresAdmin: true
          },
          children: [
            {
              path: "lecture",
              component: EditLecture,
              children: [
                {
                  path: "new",
                  name: "new-lecture"
                },
                {
                  path: "edit/:id",
                  name: "edit-lecture"
                }
              ]
            },
            {
              path: "quizTaskSet",
              component: ManageQuiz,
              children: [
                {
                  path: "new",
                  component: EditQuizTaskSet,
                  name: "new-quizTaskSet"
                },
                {
                  path: "start",
                  component: StartQuiz,
                  name: "start-quiz"
                },
                {
                  path: "edit/:id",
                  name: "edit-quizTaskSet"
                },
                {
                  path: "deadlines",
                  component: ManageQuizduellDeadlines,
                  name: "manage-quizduell-deadlines"
                },
                {
                  path: "settings",
                  component: SettingsQuiz,
                  name: "settings-koans"
                }
              ]
            },
            {
              path: "semester",
              component: EditSemester,
              children: [
                {
                  path: "new",
                  name: "new-semester"
                },
                {
                  path: "edit/:id",
                  name: "edit-semester"
                }
              ]
            },
            {
              path: "koans",
              component: ManageKoans,
              children: [
                {
                  path: "assign",
                  component: AssignKoans,
                  name: "assign-koans"
                },
                {
                  path: "synchronize",
                  component: SynchronizeKoans,
                  name: "synchronize-koans"
                },
                {
                  path: "deadlines",
                  component: ManageCourseDeadlines,
                  name: "manage-course-deadlines"
                }
              ]
            },
            {
              path: "managekoans",
              component: ManageKoans,
              name: "managekoans"
            },
            {
              path: "statistics",
              component: ManageStatistics,
              children: [
                {
                  path: "admin-dashboard",
                  component: AdminDashboard,
                  name: "admin-dashboard"
                },
                {
                  path: "ranking-points",
                  component: RankingPoints,
                  name: "ranking-points"
                },
                {
                  path: "user-specific-score/semesterId=:semesterId&lectureId=:lectureId&userId=:userId&courseId=:courseId?&chapterId=:chapterId?&quizDuelId=:quizDuelId?",
                  component: UserSpecificScore,
                  name: "user-specific-score-with-params"
                },
                {
                  path: "user-specific-score",
                  component: UserSpecificScore,
                  name: "user-specific-score"
                },
              ]
            }
          ]
        },
        {
          path: "quizduell",
          component: QuizduellPage,
          name: "quizduell"
        },
        {
          path: "ide",
          component: Ide,
          name: "ide"
        },
        {
          path: "user",
          component: UserShell,
          name: "user",
          redirect: { name: "user-profile" },
          children: [
            {
              path: "profile",
              component: Profile,
              name: "user-profile"
            },
            {
              path: "settings",
              component: Settings,
              name: "user-settings"
            }
          ]
        },
        {
          path: "lecture/:lectureId/:semesterId",
          component: LectureShell,
          name: "lecture",
          redirect: { name: "lecture-overview" },
          children: [
            {
              path: "",
              component: LectureOverview,
              name: "lecture-overview"
            },
            {
              path: "course/:courseId",
              component: CoursePage,
              name: "course",
              children: [
                {
                  path: "chapters/:chapterId/",
                  component: ChapterPanel,
                  redirect: { name: "chapter-start" },
                  children: [
                    {
                      path: "",
                      name: "chapter-start",
                      component: ChapterStart
                    },
                    {
                      path: "task/:taskId",
                      component: BaseTask,
                      name: "task"
                    }
                  ]
                }
              ]
            }
          ]
        }
      ]
    },
    {
      path: "*",
      redirect: { name: "app" }
    }
  ]
});

router.beforeEach(async (to, from, next) => {
  if (to.matched.some(route => route.meta.requiresAdmin)) {
    try {
      await store.dispatch("userStore/checkIfAdmin", null, { root: true });
      next();
    } catch (error) {
      next({ name: "dashboard" });
    }
  } else {
    next();
  }
});

export default router;
