import axiosInstance from "@/utils/axios";

export default {
  namespaced: true,
  state: {
    globalSettings: {}
  },
  mutations: {
    setGlobalSettings(state, globalSettings) {
      state.globalSettings = globalSettings;
    }
  },
  actions: {
    async fetchGlobalSettings({ commit }) {
      let response = await axiosInstance.get("/global-settings");
      commit("setGlobalSettings", response.data);
      return response;
    },
    async createGlobalSetting({ commit }, globalSetting) {
      let response = await axiosInstance.post(
        "/global-settings",
        globalSetting
      );
      commit("setGlobalSettings", response.data);
      return response;
    },
    async updateGlobalSetting({ commit }, globalSetting) {
      let response = await axiosInstance.patch(
        "/global-settings/" + globalSetting._id,
        globalSetting
      );
      commit("setGlobalSettings", response.data);
      return response;
    }
  }
};
