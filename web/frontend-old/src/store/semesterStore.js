import axiosInstance from "@/utils/axios";

export default {
  namespaced: true,
  state: {
    semesters: []
  },
  mutations: {
    setSemesters(state, semesters) {
      state.semesters = semesters;
    },
    addSemester(state, semester) {
      state.semesters.push(semester);
    },
    updateSemester(state, semester) {
      state.semesters = state.semesters.map(s =>
        s._id === semester._id ? semester : s
      );
    },
    removeSemester(state, semester) {
      state.semesters = state.semesters.filter(s => s._id !== semester._id);
    }
  },
  actions: {
    async fetchSemesters({ commit }) {
      let response = await axiosInstance.get("/semesters");
      commit("setSemesters", response.data);
      return response;
    },
    async createSemester({ commit }, semester) {
      let response = await axiosInstance.post("/semesters", semester);
      commit("addSemester", response.data);
      return response;
    },
    async updateSemester({ commit }, semester) {
      let response = await axiosInstance.patch(
        "/semesters/" + semester._id,
        semester
      );
      commit("updateSemester", response.data);
      return response;
    },
    async deleteSemester({ commit }, semester) {
      let response = await axiosInstance.delete("/semesters/" + semester._id);
      commit("removeSemester", semester);
      return response;
    }
  }
};
