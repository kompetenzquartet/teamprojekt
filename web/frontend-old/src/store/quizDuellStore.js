import axiosInstance from "@/utils/axios";

export default {
  namespaced: true,
  state: {
    quizDuells: []
  },
  mutations: {
    setQuizDuells(state, quizDuells) {
      state.quizDuells = quizDuells;
    }
  },
  actions: {
    async fetchQuizDuells({ commit }) {
      let response = await axiosInstance.get("/quizduell/data");
      commit("setQuizDuells", response.data);
      return response;
    }
  }
};
