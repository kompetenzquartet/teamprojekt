import axiosInstance from "@/utils/axios";

export default {
  namespaced: true,
  state: {
    courseDeadlines: [],
  },
  mutations: {
    setCourseDeadlines(state, courseDeadlines) {
      state.courseDeadlines = courseDeadlines;
    },
  },
  actions: {
    async fetchCourseDeadlines({ commit }) {
      let response = await axiosInstance.get("/coursedeadlines");
      commit("setCourseDeadlines", response.data);
      return response;
    },
    async deleteCourseDeadline({ commit }, courseDeadlineId) {
      return await axiosInstance.delete(
        "/coursedeadline/" + courseDeadlineId
      );
    },
    async createCourseDeadline({ commit }, courseDeadline) {
      return await axiosInstance.post(
        "/coursedeadline",
        courseDeadline
      );
    },
    async updateCourseDeadline({ commit }, courseDeadline) {
      return await axiosInstance.put(
        "/coursedeadline/ " + courseDeadline.courseDeadlineId,
        courseDeadline
      );
    },
    async fetchSubscribedCourseDeadlines({ commit }) {
      let response = await axiosInstance.get("/coursedeadlines/subscribed");
      commit("setCourseDeadlines", response.data);
      return response;
    }
  }
}