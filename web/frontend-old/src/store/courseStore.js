import router from "../router";

import users from "./userStore";

import {
  ChapterProgress,
  CourseProgress,
  TaskProgress
} from "../models/UserProgress";
import axiosInstance from "@/utils/axios";

//const coursesRef = db.ref("courses");
const coursesRef = "aa";
let progressRef = users.getters.currentUser;

export default {
  namespaced: true,
  state: {
    courses: [],
    progress: [],
    dataRefsReady: 0,
    isKoansSyncing: false
  },
  mutations: {
    setCourses(state, courses) {
      state.courses = courses;
    },
    updateCourse(state, course) {
      state.courses = state.courses.map(c =>
        c._id === course._id ? course : c
      );
    },
    setKoansSyncing(state, value) {
      state.isKoansSyncing = value;
    }
  },
  actions: {
    // TODO: User implicit, currentUser
    ENROLL_TO_COURSE: ({ getters }, { course }) => {
      if (getters.progress.some(c => c.id === course.id)) {
        console.error("Already enrolled");
        return;
      }

      // Create empty progress
      const emptyProgress = new CourseProgress(course.id, course.title);
      emptyProgress.chapters = course.chapters.map(chapter => {
        const chapterProgress = new ChapterProgress(chapter._id, chapter.title);
        chapterProgress.tasks = chapter.tasks.map(
          task => new TaskProgress(task._id)
        );
        return chapterProgress;
      });
      progressRef.push(emptyProgress);
    },

    ADD_NEW_COURSE: (context, course) => {
      coursesRef.push(course);
    },

    REMOVE_COURSE: (context, course) => {
      return progressRef.child(course[".key"]).remove();
    },

    // Set current task (determined by app state, e. g. route) as solved
    CURRENT_TASK_SOLVED: ({ getters }) => {
      console.log("Update task progress");
      // Grab ref to current course
      const courseId = getters.currentCourse.id;
      const courseProgress = getters.progress.find(
        courseProgress => courseProgress.id === courseId
      );

      // Define and alter path to 'solved' attribute of current task
      const chapterIndex = getters.currentChapter.id - 1;
      const taskIndex = getters.currentTask.id - 1;
      const taskSolvedPath = `chapters/${chapterIndex}/tasks/${taskIndex}/solved`;

      progressRef
        .child(courseProgress[".key"])
        .update({ [taskSolvedPath]: true });
    },
    async fetchCourses({ commit }) {
      let response = await axiosInstance.get("/courses");
      commit("setCourses", response.data);
      return response;
    },
    async updateCourse({ commit }, course) {
      let response = await axiosInstance.patch(
        "/courses/" + course._id,
        course
      );
      commit("updateCourse", response.data);
      return response;
    },
    async synchronizeCourses({ commit }) {
      let response = await axiosInstance.get("/synchronize");
      commit("setCourses", response.data);
      return response;
    },
    async checkCodeTask(storeData, payload) {
      return await axiosInstance.post("/courses/check-codetask", payload);
    },
    async getCodeSolution(storeData, payload) {
      return await axiosInstance.get("/courses/get-code-solution", {
        params: payload
      });
    }
  },
  getters: {
    courses: state => state.courses,
    progress: state => state.progress,

    // currently active course, chapter and task derived from route

    currentCourse: (state, getters) => {
      const { course } = router.app.$route.params;
      if (!course || state.courses.length === 0) return;

      return getters.courses.find(c => c.id === Number.parseInt(course));
    },

    currentChapter: (state, getters) => {
      const { chapter } = router.app.$route.params;
      const course = getters.currentCourse;
      if (!course || !chapter) return;

      return course.chapters.find(c => c.id === Number.parseInt(chapter));
    },

    currentTask: (state, getters) => {
      const { task } = router.app.$route.params;
      const chapter = getters.currentChapter;
      if (!task || !chapter) return;

      return chapter.tasks.find(t => t.id === Number.parseInt(task));
    },

    // Current course's progress

    currentCourseProgress: (state, getters) => {
      const course = getters.currentCourse;
      if (!course) return;

      return getters.progress.find(
        courseProgress => course.id === courseProgress.id
      );
    },

    currentChapterProgress: (state, getters) => {
      const courseProgress = getters.currentCourseProgress;
      const chapter = getters.currentChapter;
      if (!chapter || !courseProgress) return;

      return courseProgress.chapters.find(
        chapterProgress => chapter._id === chapterProgress.id
      );
    },

    currentTaskProgress: (state, getters) => {
      const chapterProgress = getters.currentChapterProgress;
      const task = getters.currentTask;
      if (!task || !chapterProgress) return;

      return chapterProgress.tasks.find(
        taskProgress => task._id === taskProgress.id
      );
    },

    currentChapterPercentage: (state, getters) => {
      if (!getters.currentChapterProgress || !getters.currentSolvedTasks)
        return;

      const numberOfTasks = getters.currentChapterProgress.tasks.length;
      const finishedTasks = getters.currentSolvedTasks.length;

      if (!numberOfTasks || !finishedTasks) {
        return 0;
      } else {
        return Math.ceil((finishedTasks * 100) / numberOfTasks);
      }
    },

    // current chapter's tasks filtered by various criteria

    currentSolvedTasks: (state, getters) => {
      if (getters.currentChapterProgress)
        return getters.currentChapterProgress.tasks.filter(task => task.solved);
    },

    currentHighestSolvedTask: (state, getters) => {
      if (!getters.currentSolvedTasks) return;
      const solvedTasks = getters.currentSolvedTasks;
      return solvedTasks.length === 0
        ? { id: 0 }
        : solvedTasks[solvedTasks.length - 1];
    },

    currentKoanTasks: (state, getters) => {
      if (getters.currentChapter)
        return getters.currentChapter.tasks.filter(
          task => task.tag === "koan-task"
        );
    },
    currentCodeTasks: (state, getters) => {
      if (getters.currentChapter)
        return getters.currentChapter.tasks.filter(
          task => task.tag === "code-task"
        );
    },
    currentVideoTasks: (state, getters) => {
      if (getters.currentChapter)
        return getters.currentChapter.tasks.filter(
          task => task.tag === "video-task"
        );
    }
  }
};
