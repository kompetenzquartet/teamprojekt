import axiosInstance from "@/utils/axios";

export default {
  namespaced: true,
  state: {
    user: null,
    users: []
  },
  getters: {
    isAuthenticated: state => state.user !== null,
    isAdmin: state => state.user.permissions === "admin",
    currentUser: state => state.user,
    lectures: state => state.lectures
  },
  mutations: {
    setUsers(state, users) {
      state.users = users;
    },
    setUser(state, user) {
      state.user = user;
    },
    addSubscription(state, subscription) {
      state.user.subscriptions.push(subscription);
    },
    removeSubscription(state, subscription) {
      state.user.subscriptions = state.user.subscriptions.filter(
        s => s._id !== subscription._id
      );
    }
  },
  actions: {
    async loginCasualUser() {},
    async loginStudentUser({ commit }, credentials) {
      let response = await axiosInstance.post("/auth/htwg", null, {
        params: credentials
      });
      commit("setUser", response.data);
      return response;
    },
    async registerCasualUser() {},
    async logoutUser() {
      return await axiosInstance.post("/auth/logout");
    },
    async updateUser({ commit }, user) {
      let response = await axiosInstance.patch("/users/" + user._id, user);
      commit("setUser", response.data);
      return response;
    },
    async checkToken({ commit }) {
      let response = await axiosInstance.post("/auth/validate");
      commit("setUser", response.data);
      return response;
    },
    async checkIfAdmin() {
      return await axiosInstance.get("/auth/is-admin");
    },
    async createSubscription({ commit }, subscription) {
      let response = await axiosInstance.post("/subscriptions", subscription);
      commit("addSubscription", response.data);
      return response;
    },
    async deleteUserSubscription({ commit }, subscription) {
      let response = await axiosInstance.delete(
        "/subscriptions/" + subscription._id
      );
      commit("removeSubscription", subscription);
      return response;
    },
    async fetchAllUsers({ commit }) {
      let response = await axiosInstance.get("/users");
      commit("setUsers", response.data);
      return response;
    }
  }
};
