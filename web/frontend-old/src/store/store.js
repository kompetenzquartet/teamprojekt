/* eslint-disable no-unused-vars */
import Vue from "vue";
import Vuex, { mapState } from "vuex";
import semesterStore from "@/store/semesterStore";
import userStore from "@/store/userStore";
import courseStore from "@/store/courseStore";
import lectureStore from "@/store/lectureStore";
import quizTaskSetStore from "@/store/quizTaskSetStore";
import subscriptionStore from "./subscriptionStore";
import axiosInstance from "@/utils/axios";
import globalSettingStore from "@/store/globalSettingStore";
import quizDuellStore from "./quizDuellStore";
import quizduellDeadlineStore from "./quizduellDeadlineStore"
import courseDeadlineStore from "@/store/courseDeadlineStore";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    userStore,
    courseStore,
    semesterStore,
    lectureStore,
    quizTaskSetStore,
    subscriptionStore,
    globalSettingStore,
    quizDuellStore,
    quizduellDeadlineStore,
    courseDeadlineStore,
  },
  state: {
    data: {
      currTask: null,
      duration: 0,
      scoreboard: [{ username: "" }, { score: "" }],
      isFinished: false,
      tasks: null
    },
    socket: {
      isConnected: false,
      message: "",
      froToBackMsg: {},
      endToFroMsg: {},
      reconnectError: false
    }
  },
  mutations: {
    ...mapState("courseStore", [["fetchCourses"], "courses"]),

    setFrontendMessage(state, payload) {
      this.froToBackMsg = payload;
    },

    SOCKET_ONOPEN(state, event) {
      state.socket.isConnected = true;
    },
    SOCKET_ONCLOSE(state, event) {
      state.socket.isConnected = false;
    },
    SOCKET_ONERROR(state, event) {
      console.log("ERROR ON WEBSOCKET");
      console.error(state, event);
    },

    // reacting on message from backend to frontend
    SOCKET_ONMESSAGE(state, payload) {
      state.socket.message = payload;
      switch (payload.type) {
        case "testResponse":
          break;
        case "task":
          state.data.currTask = payload.msg.task;
          state.data.duration = payload.msg.duration;
          break;
        case "scoreUpdate":
          state.data.scoreboard = payload.msg.scores;
          break;
        case "finish":
          state.data.scoreboard = payload.msg.scores;
          state.data.tasks = payload.msg.tasks;
          state.data.isFinished = true;
          state.data.currTask = null;
          state.data.duration = null;
          break;
        default:
          console.log("Error in received message");
          break;
      }
    },
    // mutations for reconnect methods
    SOCKET_RECONNECT(state, count) {},
    SOCKET_RECONNECT_ERROR(state) {
      state.socket.reconnectError = true;
    }
  },
  actions: {
    async checkForWebsocket(storeData) {
      return await axiosInstance.get("/quizduell");
    }
  }
});
