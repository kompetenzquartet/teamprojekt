import axiosInstance from "@/utils/axios";

export default {
  namespaced: true,
  state: {
    subscriptions: null
  },
  mutations: {
    setSubscriptions(state, subscriptions) {
      state.subscriptions = subscriptions;
    }
  },
  actions: {
    async fetchSubscriptions({ commit }) {
      let response = await axiosInstance.get("/subscriptions");
      commit("setSubscriptions", response.data);
      return response;
    }
  }
};
