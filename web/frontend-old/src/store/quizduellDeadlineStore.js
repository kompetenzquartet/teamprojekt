import axiosInstance from "@/utils/axios";

export default {
  namespaced: true,
  state: {
    quizduellDeadlines: []
  },
  mutations: {
    setQuizduellDeadlines(state, quizduellDeadlines) {
      state.quizduellDeadlines = quizduellDeadlines;
    }
  },
  actions: {
    async fetchQuizduellDeadlines({ commit }) {
      let response = await axiosInstance("/quizduelldeadlines");
      commit("setQuizduellDeadlines", response.data);
      return response;
    },
    async createQuizduellDeadline({ commit }, quizduellDeadline) {
      return await axiosInstance.post(
        "/quizduelldeadline",
        quizduellDeadline
      );
    },
    async updateQuizduellDeadline({ commit }, quizduellDeadline) {
      return await axiosInstance.put(
        "/quizduelldeadline/" + quizduellDeadline.quizduellDeadlineId,
        quizduellDeadline
      );
    },
    async deleteQuizduellDeadline({ commit }, quizduellDeadlineId) {
      return await axiosInstance.delete(
        "/quizduelldeadline/" + quizduellDeadlineId
      );
    },
    async fetchSubscribedQuizduellDeadlines({ commit }) {
      let response = await axiosInstance.get("/quizduelldeadlines/subscribed");
      commit("setQuizduellDeadlines", response.data);
      return response;
    }
  }
};