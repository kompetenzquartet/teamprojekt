import axiosInstance from "@/utils/axios";

export default {
  namespaced: true,
  state: {
    lectures: []
  },
  mutations: {
    setLectures(state, lectures) {
      state.lectures = lectures;
    },
    addLecture(state, lecture) {
      state.lectures.push(lecture);
    },
    updateLecture(state, lecture) {
      state.lectures = state.lectures.map(l =>
        l._id === lecture._id ? lecture : l
      );
    },
    removeLecture(state, lecture) {
      state.lectures = state.lectures.filter(l => l._id !== lecture._id);
    }
  },
  actions: {
    async fetchLectures({ commit }) {
      let response = await axiosInstance.get("/lectures");
      commit("setLectures", response.data);
      return response;
    },
    async createLecture({ commit }, lecture) {
      let response = await axiosInstance.post("/lectures", lecture);
      commit("addLecture", response.data);
      return response;
    },
    async updateLecture({ commit }, lecture) {
      let response = await axiosInstance.patch(
        "/lectures/" + lecture._id,
        lecture
      );
      commit("updateLecture", response.data);
      return response;
    },
    async deleteLecture({ commit }, lecture) {
      let response = await axiosInstance.delete("/lectures/" + lecture._id);
      commit("removeLecture", lecture);
      return response;
    }
  }
};
