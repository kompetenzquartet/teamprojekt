import axiosInstance from "@/utils/axios";

export default {
  namespaced: true,
  state: {
    quizTaskSets: []
  },
  mutations: {
    setQuizTaskSets(state, quizTaskSets) {
      state.quizTaskSets = quizTaskSets;
    },
    addQuizTaskSet(state, quizTaskSet) {
      state.quizTaskSets.push(quizTaskSet);
    },
    updateQuizTaskSet(state, quizTaskSet) {
      state.quizTaskSets = state.quizTaskSets.map(l =>
        l._id === quizTaskSet._id ? quizTaskSet : l
      );
    },
    removeQuizTaskSet(state, quizTaskSet) {
      state.quizTaskSets = state.quizTaskSets.filter(
        l => l._id !== quizTaskSet._id
      );
    }
  },
  actions: {
    async fetchQuizTaskSets({ commit }) {
      let response = await axiosInstance.get("/quiztasksets");
      commit("setQuizTaskSets", response.data);
      return response;
    },
    async createQuizTaskSet({ commit }, quizTaskSet) {
      let response = await axiosInstance.post("/quiztasksets", quizTaskSet);
      commit("addQuizTaskSet", response.data);
      return response;
    },
    async updateQuizTaskSet({ commit }, quizTaskSet) {
      let response = await axiosInstance.patch(
        "/quiztasksets/" + quizTaskSet._id,
        quizTaskSet
      );
      commit("updateQuizTaskSet", response.data);
      return response;
    },
    async deleteQuizTaskSet({ commit }, quizTaskSet) {
      let response = await axiosInstance.delete(
        "/quiztasksets/" + quizTaskSet._id
      );
      commit("removeQuizTaskSet", quizTaskSet);
      return response;
    }
  }
};
