import axios from "axios";
import { getApiURL } from "@/utils/connection";
import router from "@/router";

const axiosInstance = axios.create({
  baseURL: getApiURL(),
  withCredentials: true,
});

axiosInstance.interceptors.response.use(
  function(response) {
    if (
      router.currentRoute.matched.length &&
      !router.currentRoute.matched.some(record => record.meta.requiresAuth)
    ) {
      if (router.currentRoute.query.redirect_to) {
        router.push({ path: `${router.currentRoute.query.redirect_to}` });
      } else {
        router.push({ name: "dashboard" });
      }
    }
    return response;
  },
  function(error) {
    if (
      error.response.status === 401 &&
      router.currentRoute.matched.some(record => record.meta.requiresAuth)
    ) {
      router.push({
        name: "student-login",
        query: { redirect_to: router.currentRoute.fullPath }
      });
    }
    return Promise.reject(error);
  }
);

export default axiosInstance;
