const isProduction = () => {
  return process.env.NODE_ENV === "production";
};

const getOpenSemesters = semesters => {
  const todayTimestamp = Date.now();
  return semesters
    .filter(
      semester =>
        todayTimestamp >= semester.opensAt &&
        todayTimestamp <= semester.closesAt
    )
    .sort((a, b) => b.opensAt - a.opensAt);
};

const getUpcomingSemesters = semesters => {
  const todayTimestamp = Date.now();
  return semesters
    .filter(semester => semester.opensAt > todayTimestamp)
    .sort((a, b) => b.opensAt - a.opensAt);
};
const getClosedSemesters = semesters => {
  const todayTimestamp = Date.now();
  return semesters
    .filter(semester => semester.closesAt < todayTimestamp)
    .sort((a, b) => b.opensAt - a.opensAt);
};

export {
  isProduction,
  getOpenSemesters,
  getUpcomingSemesters,
  getClosedSemesters
};
