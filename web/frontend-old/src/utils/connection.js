// For Productiv Deployment. Run's over Docker with nginx
// const getApiURL = () => {
//   return location.protocol + "//api." + location.host;
// };

// For hotreload Deployment without Docker. Use "npm run serve" to start in: CodeTask/frontend
// const getApiURL = () => {
//   return location.protocol + "//api." + location.hostname + ":8000";
// };

// For hotreload Deployment without Docker. Use "npm run serve" to start in: CodeTask/frontend
const getApiURL = () => {
  return location.protocol + "//api." + location.hostname;
};

const getSocketURL = () => {
  const websocketProtocol =
    process.env.NODE_ENV !== "production" ? "ws:" : "wss:";
  return websocketProtocol + "//api." + location.hostname + "/quizduell/socket";
};

export { getApiURL, getSocketURL };
