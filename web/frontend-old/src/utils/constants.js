const taskNotSolved = 0;
const taskSolvedWithHint = 1;
const taskSolvedSuccessfully = 2;
export { taskNotSolved, taskSolvedWithHint, taskSolvedSuccessfully };
