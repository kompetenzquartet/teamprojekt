import BaseDeadline from "@/models/BaseDeadline";

export default class QuizduellDeadline extends BaseDeadline {
  constructor(lectureId, lectureName, deadlineAt, quizduellDeadlineId, taskSetId, taskSetName, semesterName) {
    super(lectureId, lectureName, deadlineAt);
    this.quizduellDeadlineId = quizduellDeadlineId;
    this.taskSetId = taskSetId;
    this.taskSetName = taskSetName;
    this.semesterName = semesterName;
  }
}