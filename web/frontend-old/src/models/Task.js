//ToDo @Refactor

export default class Task {
  id;
  tag;
  data;
}

class TaskData {
  description;

  // koan and code tasks
  code;
  mode;

  // koan tasks
  solutions;

  // video tasks
  url;
}
