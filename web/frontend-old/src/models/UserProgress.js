// In the firebase 'users' db, for each user there is an array of CourseProgress data

export class UserProgress {
  courses;
}

export class CourseProgress {
  chapters;
}

export class ChapterProgress {
  tasks;
}

export class TaskProgress {
  solved;
}
