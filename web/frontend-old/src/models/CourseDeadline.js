import BaseDeadline from "@/models/BaseDeadline";

export default class CourseDeadline extends BaseDeadline {
  constructor(lectureId, lectureName, deadlineAt, courseDeadlineId, courseName, semesterName) {
    super(lectureId, lectureName, deadlineAt);
    this.courseDeadlineId = courseDeadlineId;
    this.courseName = courseName;
    this.semesterName = semesterName;
  }
}