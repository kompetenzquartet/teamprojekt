export default class BaseDeadline {
  constructor(lectureId, lectureName, deadlineAt) {
    this.lectureId = lectureId;
    this.lectureName = lectureName;
    this.deadlineAt = deadlineAt;
  }
}
