// Vue and core components
import Vue from "vue";
import App from "./App";
import router from "./router";
import store from "./store/store";
import Vuelidate from "vuelidate";

// import "./registerServiceWorker"; We will inherit this later

// UI and decoration
import VueParticles from "vue-particles";
import Element from "element-ui";
import locale from "element-ui/lib/locale/lang/de";
import "element-ui/lib/theme-chalk/display.css";
import VueDisqus from "vue-disqus";
import vueAwesomeCountdown from "vue-awesome-countdown"; //Countdown Timer Quizduell;

// Youtube
import VueYoutubeEmbed from "vue-youtube-embed";
import VueNativeSock from "vue-native-websocket";
import VueFriendlyIframe from "vue-friendly-iframe";
import { getSocketURL } from "@/utils/connection";

Vue.config.productionTip = false;

Vue.use(VueNativeSock, getSocketURL(), {
  store: store,
  format: "json",
  reconnection: true,
  reconnectionAttempts: 3,
  reconnectionDelay: 3000,
  connectManually: true
});

Vue.use(VueYoutubeEmbed);
Vue.use(VueParticles);
Vue.use(VueFriendlyIframe);
Vue.use(Vuelidate);
Vue.use(Element, { locale });
Vue.use(VueDisqus);
Vue.use(vueAwesomeCountdown, "vac"); // Component name, `countdown` and `vac` by default;

// Socket

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
