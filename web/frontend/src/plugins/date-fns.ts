import { setDefaultOptions } from 'date-fns'
import { de } from 'date-fns/locale'
setDefaultOptions({ locale: de })
