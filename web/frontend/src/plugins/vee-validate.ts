import { configure, defineRule } from 'vee-validate'
import { localize, setLocale } from '@vee-validate/i18n'
// eslint-disable-next-line camelcase
import { confirmed, email, is_not, max, min, min_value, required } from '@vee-validate/rules'
import de from '@vee-validate/i18n/dist/locale/de.json'
import en from '@vee-validate/i18n/dist/locale/en.json'
import { format, isAfter } from 'date-fns'

configure({
  generateMessage: localize({
    de,
    en,
  }),
})

setLocale('de')

defineRule('required', required)
defineRule('email', email)
defineRule('min', min)
defineRule('max', max)
defineRule('min_value', min_value)
defineRule('is_not', is_not)
defineRule('confirmed', confirmed)
defineRule('isDateAfter', (value: number, [min] : [number | undefined], { field }: {field: string}) => {
  if (!min) {
    return true
  }
  const date = new Date(value)
  const minDate = new Date(min)
  if (!isAfter(date, minDate)) {
    return `${field} muss nach dem ${format(minDate, 'dd.MM.yyyy')} sein`
  }

  return true
})
