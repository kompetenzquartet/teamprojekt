import {
  BarController,
  BarElement,
  CategoryScale,
  Chart,
  Legend,
  LinearScale,
  Title,
  Tooltip,
} from 'chart.js'

import autocolors from 'chartjs-plugin-autocolors'

Chart.register(BarElement, BarController, CategoryScale, LinearScale, Title, Legend, Tooltip, autocolors)
