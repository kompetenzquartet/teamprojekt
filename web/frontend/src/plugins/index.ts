/**
 * plugins/index.ts
 *
 * Automatically included in `./src/main.ts`
 */

// Plugins
import router from '@/router'
import vuetify from './vuetify'
import './date-fns'
import { createManager } from '@vue-youtube/core'
import './vee-validate'
import './chart'

// Types
import type { App } from 'vue'

export function registerPlugins (app: App) {
  app
    .use(vuetify)
    .use(router)
    .use(createManager())
}
