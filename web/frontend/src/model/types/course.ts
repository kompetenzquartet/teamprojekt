import type { Chapter, ChapterView } from './chapter'
import type { Lecture } from './lecture'

export interface Course {
  _id: string;
  chapters: Chapter[];
  description: string;
  lectures: Array<Lecture['_id']>;
  title: string;
}

export interface CourseView extends Pick<Course, '_id' | 'description' | 'title'> {
  chapters: ChapterView[];
  solvedPercentage: number;
}

export interface CourseChapter extends Pick<Course, '_id' | 'title'> {
  chapter: Chapter;
}
