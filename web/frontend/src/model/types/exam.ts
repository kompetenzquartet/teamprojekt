export interface Exam {
  _id: string;
  name: string;
  date: number;
  duration: number;
  lecture: string;
  semester: string;
  isOpen: boolean;
  tasks: ExamTask[];
  startTime: number;
  endTime: number;
  examType: 'exam' | 'test-exam' | 'old-exam';
  password: string;
  gradeThresholds: GradeThresholds;
}

interface ExamTask {
  _id: String;
  tag: String;
  data: ExamTaskData;
}

interface ExamTaskData {
  code: string;
  description: string;
  mode: string;
  codeSolution: string;
  solutions: Solution[];
  maxPoints: number;
}

interface Solution {
  _id: string;
  solution: string;
  correctness: boolean;
}

interface GradeThresholds {
  top: number;
  bottom: number;
}
