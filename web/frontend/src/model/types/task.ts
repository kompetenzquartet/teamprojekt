export interface TaskSolution {
  _id: string;
  correctness: boolean;
  solution: string;
}

export type TaskDataMode = 'info' | 'reflection' | 'single-choice' | 'multiple-choice' | 'video' | 'scala';

export interface TaskData {
  code: string;
  description: string;
  mode: TaskDataMode;
  solutions: TaskSolution[];
  taskTime: number;
  url: string;
}

export type TaskTag = 'info-task' | 'reflection-task' | 'single-choice-task' | 'multiple-choice-task' | 'video-task' | 'code-task' | 'koan-task';

export interface Task {
  _id: string;
  data: TaskData;
  tag: TaskTag;
}
