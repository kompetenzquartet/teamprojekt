import type { User } from './user'

export interface SigninRequest extends Pick<User, 'email'> {
  password: string;
}

export interface SignupRequest extends Pick<User, 'email' | 'username'> {
  password: string;
}
