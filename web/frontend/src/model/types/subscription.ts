import type { Lecture } from './lecture'
import type { Semester } from './semester'
import type { User } from './user'

export interface Subscription {
  _id: string;
  lecture: Lecture['_id'];
  semester: Semester['_id'];
  user: User['_id'];
}
