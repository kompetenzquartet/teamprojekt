import type { QuizDuelUser, QuizDuelUserScore } from './quizDuel'
import type { QuizTask } from './quizTaskSet'
import type { Task } from './task'

type QuizDuelEventScore = Pick<QuizDuelUser, 'username'> & Pick<QuizDuelUserScore, 'score'>
type QuizDuelEventTask = Task & Pick<QuizTask, 'chapterId' | 'courseId'>

export interface QuizDuelFinishEvent {
  msg: {
    scores: QuizDuelEventScore[];
  }
  type: 'finish';
}

export interface QuizDuelTaskEvent {
  msg: {
    duration: number;
    task: QuizDuelEventTask;
  }
  type: 'task';
}

export interface QuizDuelScoreboardEvent {
  msg: {
    scores: QuizDuelEventScore[];
    task: QuizDuelEventTask;
  }
  type: 'scoreboard'
}

export type QuizDuelEvent = QuizDuelFinishEvent | QuizDuelTaskEvent | QuizDuelScoreboardEvent;

export interface QuizDuelSubmitMessage {
  msg: {
    solutions: string[];
    task: QuizTask;
  }
  type: 'submit'
}

export type QuizDuelSubmitMessageState = Omit<QuizDuelSubmitMessage['msg'], 'solutions' | 'tasks'>
  & {solutions: Record<string, string>, tasks: QuizDuelEventTask[]}
