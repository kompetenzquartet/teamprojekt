import type { Chapter } from './chapter'
import type { Course } from './course'
import type { Task } from './task'

export interface QuizTask {
  _id: string;
  chapterId: Chapter['_id'];
  courseId: Course['_id'];
  taskId: Task['_id'];
}

export type QuizTaskRequest = Omit<QuizTask, '_id'>

export interface QuizTaskSet {
  _id: string;
  name: string;
  tasks: QuizTask[];
}

export type QuizTaskSetState = Omit<QuizTaskSet, '_id' | 'tasks'> & {tasks: string[]}

export type QuizTaskSetRequest = Omit<QuizTaskSet, '_id' | 'tasks'> & {tasks: QuizTaskRequest[]}
