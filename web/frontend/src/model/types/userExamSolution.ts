import { ExamSolution } from '@/model/types/examSolution'

export interface UserExamSolution {
  userId: string;
  userName: string,
  studentNumber: string,
  examSolutions: ExamSolution[],
  mark: number;
}
