import type { Task } from './task'

export interface Chapter {
  _id: string;
  tasks: Task[];
  title: string;
}

export type ChapterView = Pick<Chapter, '_id' | 'title'> & { solvedPercentage: number }
