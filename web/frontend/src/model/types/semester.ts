import type { Lecture, LectureView } from './lecture'

export interface Semester {
  _id: string;
  closesAt: number;
  lectures: Array<Lecture['_id']>;
  name: string;
  opensAt: number;
}

export interface SemesterView extends Pick<Semester, '_id' | 'closesAt' | 'opensAt' | 'name'> {
  lectures: LectureView[]
}
