import type { Course, CourseView } from './course'

export interface Lecture {
  _id: string;
  courses: Course['_id'];
  description: string;
  name: string;
}

export type LectureRequest = Pick<Lecture, 'name' | 'description'>

export type LectureView = Pick<Lecture, '_id' | 'description' | 'name'>

export interface SubscribedLecture extends Pick<Lecture, '_id' | 'name'> {
  courses: CourseView[],
}
