export interface UserSettings {
  autoNext: boolean;
  autoPlay: boolean;
  infoText: boolean;
}

export interface User {
  _id: string;
  email: string;
  permissions: string;
  studentNumber: string;
  username: string;
}

export type UserProfileRequest = Pick<User, 'username'>
