export interface ExamSolution {
  _id: string;
  userId: string;
  examId: string;
  examTaskId: string;
  solvedWith: Solution[];
  points: number;
}

interface Solution {
  _id: string;
  solution: string;
  correctness: boolean;
}
