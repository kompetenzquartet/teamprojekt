export interface CourseDeadline {
  _id: string;
  semester: string;
  lecture: string;
  course: string;
  deadline: number;
}
