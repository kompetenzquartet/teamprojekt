export interface QuizDuelDeadline {
  _id: string;
  lecture: string;
  semester: string;
  taskSet: string;
  deadline: number;
}
