import type { Chapter } from '@/model/types/chapter'
import type { Course } from '@/model/types/course'
import type { Lecture } from '@/model/types/lecture'
import type { Semester } from '@/model/types/semester'
import type { Task } from '@/model/types/task'
import type { User } from '@/model/types/user'

export interface TaskProgressSolution {
  correctness: boolean;
  solution: string;
}

export interface TaskProgress {
  _id: string;
  userId: User['_id'];
  semesterId: Semester['_id'];
  lectureId: Lecture['_id'];
  courseId: Course['_id'];
  chapterId: Chapter['_id'];
  taskId: Task['_id'];
  solutions: TaskProgressSolution[];
  cheatedSolutions: boolean[];
  // lectureName: Lecture['name'];
}

export type TaskProgressRequest = Omit<TaskProgress, 'solutions' | '_id'> & { solutions: Array<string | null> }
export type TaskProgressState = Omit<TaskProgressRequest, 'solutions' | 'cheatedSolutions'>
  & Pick<TaskProgress, '_id'>
  & { solutions: Record<string, string>; cheatedSolutions: Record<string, boolean> }
