export interface Settings {
  _id: string;
  codeTaskTime: number;
  koanTime: number;
}
