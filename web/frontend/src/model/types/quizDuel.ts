import type { Lecture } from './lecture'
import type { QuizTask, QuizTaskSet } from './quizTaskSet'
import type { Semester } from './semester'
import type { User } from './user'

export interface QuizDuelUserScore {
  score: number;
  task: QuizTask;
}

export interface QuizDuelUser {
  _id: string;
  scores: QuizDuelUserScore[];
  userId: User['_id'];
  username: User['username'];
}

export interface QuizDuel {
  _id: string;
  lectureId: Lecture['_id'];
  participants: QuizDuelUser[];
  quizTaskSetId: QuizTaskSet['_id'];
  semesterId: Semester['_id'];
}

export type QuizDuelRequest = Omit<QuizDuel, '_id' | 'participants'>

export interface QuizDuelView {
  _id: string;
  isRunning: Boolean;
  lecture: string;
  participants: QuizDuelUser[];
  quizTaskSet: string;
  remainingTime: number;
  semester: string;
}
