export default class BaseDeadline {
  lectureId: string
  lectureName: string
  deadlineAt: Date

  constructor(lectureId: string, lectureName: string, deadlineAt: Date) {
    this.lectureId = lectureId
    this.lectureName = lectureName
    this.deadlineAt = deadlineAt
  }
}

export class CourseDeadline extends BaseDeadline {
  courseDeadlineId: string
  courseName: string
  semesterName: string

  constructor(lectureId: string, lectureName: string, deadlineAt: Date, courseDeadlineId: string, courseName: string, semesterName: string) {
    super(lectureId, lectureName, deadlineAt)
    this.courseDeadlineId = courseDeadlineId
    this.courseName = courseName
    this.semesterName = semesterName
  }
}

export class QuizDuelDeadline extends BaseDeadline {
  quizDuelDeadlineId: string
  taskSetId: string
  taskSetName: string
  semesterName: string

  constructor(lectureId: string, lectureName: string, deadlineAt: Date, quizDuelDeadlineId: string, taskSetId: string, taskSetName: string, semesterName: string) {
    super(lectureId, lectureName, deadlineAt)
    this.quizDuelDeadlineId = quizDuelDeadlineId
    this.taskSetId = taskSetId
    this.taskSetName = taskSetName
    this.semesterName = semesterName
  }
}
