import type { Lecture } from '@/model/types/lecture'
import type { Ref } from 'vue'
import type { Semester } from './types/semester'
import type { Subscription } from './types/subscription'

export const useSubscription = (userSubscriptions: Ref<Subscription[]>) => {
  function getSubscription(semesterId: Semester['_id'], lectureId: Lecture['_id']) {
    return userSubscriptions.value.find(subscription => subscription.lecture === lectureId && subscription.semester === semesterId)
  }

  function hasSubscription(semesterId: Semester['_id'], lectureId: Lecture['_id']) {
    return Boolean(getSubscription(semesterId, lectureId))
  }

  return {
    hasSubscription,
    getSubscription,
  }
}
