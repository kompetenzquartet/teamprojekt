import type { Semester } from '@/model/types/semester'
import { differenceInDays, isAfter } from 'date-fns'

export const useSemester = () => {
  function getSemesterTitle(semester: Pick<Semester, 'closesAt' | 'name'>) {
    const today = new Date()
    const closeDate = new Date(semester.closesAt)
    const days = differenceInDays(closeDate, today)

    return isAfter(closeDate, today)
      ? `Semester: ${semester.name} (endet ${days === 1 ? 'morgen' : 'in ' + days + ' Tagen'})`
      : `Semester: ${semester.name}`
  }

  return {
    getSemesterTitle,
  }
}
