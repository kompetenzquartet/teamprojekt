import { Course } from '@/model/types/course'
import { QuizTask, QuizTaskSet } from '@/model/types/quizTaskSet'

export default {
  methods: {
    getPointsOfTaskSet(courses: Course[], quizTaskSet: QuizTaskSet) {
      return quizTaskSet.tasks.map((quizTask: QuizTask) => {
        const course: Course | undefined = courses.find(course => course._id === quizTask.courseId)
        const chapter = course?.chapters.find(chapter => chapter._id === quizTask.chapterId)
        const task = chapter.tasks.find(task => task._id === quizTask.taskId)
        if (task.tag === 'koan-task') {
          return task.data.solutions.length
        } else {
          return 1
        }
      }).reduce((a, b) => a + b, 0)
    },
  },
}
