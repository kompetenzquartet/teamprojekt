import { Course } from '@/model/types/course'

export default {
  methods: {
    getCorrectnessOfSolutionsInPoints(courses: Course[], courseId: string, chapterId: string, taskId: string, solutions: any[], cheatedSolutions: boolean[]): number {
      const course = courses.find(course => course._id === courseId)
      if (!course) return 0
      const chapter = course.chapters.find(chapter => chapter._id === chapterId)
      if (!chapter) return 0
      const task = chapter.tasks.find(task => task._id === taskId)
      if (!task || task.data.solutions.length !== solutions.length) return 0
      if (task.data.solutions.length === 0) return 1
      let pointsOfSolutions = 0
      for (let i = 0; i < solutions.length; i++) {
        pointsOfSolutions += task.data.solutions[i].solution === solutions[i].solution && !cheatedSolutions[i] ? 1 : 0
      }
      return pointsOfSolutions
    },
  },
}
