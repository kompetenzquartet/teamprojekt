import type { VDataTable } from 'vuetify/components'

type DataTableHeaders = VDataTable['$props']['headers']
