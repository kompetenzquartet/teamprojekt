import { RouteRecordRaw } from 'vue-router'

export const routes: RouteRecordRaw[] = [
  {
    path: '/signin',
    name: 'signin',
    component: () => import('@/pages/auth/Signin.vue'),
  },
  {
    path: '/signup',
    name: 'signup',
    component: () => import('@/pages/auth/Signup.vue'),
  },
  {
    path: '/',
    name: 'home',
    redirect: { name: 'lectures' },
    children: [
      {
        path: 'lectures',
        name: 'lectures',
        redirect: { name: 'lectures-list' },
        children: [
          { path: '', name: 'lectures-list', component: () => import('@/pages/lecture/Lectures.vue') },
          {
            path: ':lectureId/semester/:semesterId',
            name: 'lectures-lecture',
            redirect: { name: 'lectures-lecture-view' },
            children: [
              { path: '', name: 'lectures-lecture-view', component: () => import('@/pages/lecture/Lecture.vue'), props: true },
              {
                path: 'courses/:courseId/chapters/:chapterId/tasks/:taskId',
                name: 'chapter',
                props: true,
                component: () => import('@/pages/chapter/Chapter.vue'),
              },
            ],
          },
        ],
      },
      { path: 'statistics', name: 'statistics', component: () => import('@/pages/Statistics.vue') },
      { path: 'calendar', name: 'calendar', component: () => import('@/pages/calendar/Calendar.vue') },
      { path: 'quiz-duel', name: 'quiz-duel', component: () => import('@/pages/quiz-duel/QuizDuel.vue') },
      { path: 'scala', name: 'ide-scala', component: () => import('@/pages/ScalaIde.vue') },
      {
        path: 'user',
        name: 'user',
        component: () => import('@/pages/user/UserPage.vue'),
        redirect: { name: 'user-profile' },
        children: [
          { path: 'profile', name: 'user-profile', component: () => import('@/pages/user/UserProfile.vue') },
          { path: 'settings', name: 'user-settings', component: () => import('@/pages/user/UserSettings.vue') },
        ],
      },
      {
        path: 'exam',
        name: 'exam',
        redirect: { name: 'exam-overview' },
        children: [
          { path: 'overview', name: 'exam-overview', component: () => import('@/pages/exam/ExamOverview.vue') },
          { path: 'start/:examId', name: 'exam-start', component: () => import('@/pages/exam/ExamStart.vue') },
          { path: ':examId/task/:taskId', name: 'exam-task', component: () => import('@/pages/exam/ExamTask.vue') },
        ],
      },
      {
        path: 'admin',
        name: 'admin',
        redirect: { name: 'admin-lectures' },
        children: [
          {
            path: 'lectures',
            name: 'admin-lectures',
            redirect: { name: 'admin-lectures-list' },
            children: [
              { path: 'list', name: 'admin-lectures-list', component: () => import('@/pages/admin/AdminLectureList.vue') },
              { path: 'lecture/:id?', name: 'admin-lectures-lecture', component: () => import('@/pages/admin/AdminLecture.vue'), props: true },
            ],
          },
          {
            path: 'semesters',
            name: 'admin-semesters',
            redirect: { name: 'admin-semesters-list' },
            children: [
              { path: 'list', name: 'admin-semesters-list', component: () => import('@/pages/admin/AdminSemesterList.vue') },
              { path: 'semester/:id?', name: 'admin-semesters-semester', component: () => import('@/pages/admin/AdminSemester.vue'), props: true },
            ],
          },
          {
            path: 'courses',
            name: 'admin-courses',
            redirect: { name: 'admin-courses-list' },
            children: [
              { path: 'list', name: 'admin-courses-list', component: () => import('@/pages/admin/AdminCourseList.vue') },
              { path: 'course/:id', name: 'admin-courses-course', component: () => import('@/pages/admin/AdminCourse.vue'), props: true },
            ],
          },
          {
            path: 'course-deadlines',
            name: 'admin-course-deadlines',
            component: () => import('@/pages/admin/AdminCourseDeadlineList.vue'),
          },
          {
            path: 'quiz-duel',
            name: 'admin-quiz-duels',
            redirect: { name: 'admin-quiz-duels-list' },
            children: [
              { path: 'task-set-list', name: 'admin-quiz-duels-task-set-list', component: () => import('@/pages/admin/AdminQuizTaskSetList.vue') },
              { path: 'task-set/:id?', name: 'admin-quiz-duels-task-set', component: () => import('@/pages/admin/AdminQuizTaskSet.vue'), props: true },
              { path: 'list', name: 'admin-quiz-duels-list', component: () => import('@/pages/admin/AdminQuizDuelList.vue') },
              { path: 'quiz-duel-deadlines', name: 'admin-quiz-duel-deadlines', component: () => import('@/pages/admin/AdminQuizDuelDeadlineList.vue') },
              { path: 'quiz-duel/:id?', name: 'admin-quiz-duels-duel', component: () => import('@/pages/admin/AdminQuizDuel.vue'), props: true },
              { path: 'settings', name: 'admin-quiz-duels-settings', component: () => import('@/pages/admin/AdminQuizDuelSettings.vue') },
            ],
          },
          {
            path: 'statistics',
            name: 'admin-statistics',
            redirect: { name: 'admin-statistic-dashboard' },
            children: [
              { path: 'dashboard', name: 'admin-statistic-dashboard', component: () => import('@/pages/admin/statistic/AdminDashboard.vue') },
              { path: 'ranking', name: 'admin-statistic-ranking', component: () => import('@/pages/admin/statistic/AdminRankingPoints.vue') },
              {
                path: 'user-specific/tabChoice=:tabChoice?&semesterId=:semesterId&lectureId=:lectureId&userId=:userId&courseId=:courseId?&chapterId=:chapterId?&quizDuelId=:quizDuelId?&examId=:examId?',
                name: 'admin-statistic-user-specific-with-params',
                component: () => import('@/pages/admin/statistic/AdminUserSpecificScore.vue'),
              },
              { path: 'user-specific', name: 'admin-statistic-user-specific', component: () => import('@/pages/admin/statistic/AdminUserSpecificScore.vue') },
            ],
          },
          {
            path: 'exams',
            name: 'admin-exams',
            redirect: { name: 'admin-exams-list' },
            children: [
              { path: 'list', name: 'admin-exams-list', component: () => import('@/pages/admin/exam/AdminExamList.vue') },
              {
                path: 'edit/:examId?',
                name: 'admin-exams-exam',
                component: () => import('@/pages/admin/exam/AdminExams.vue'),
              },
              {
                path: 'points/:examId',
                name: 'admin-exam-points',
                component: () => import('@/pages/admin/exam/AdminExamPoints.vue'),
              },
              {
                name: 'admin-grade-exams',
                path: 'grade-exam/:examId',
                component: () => import('@/pages/admin/exam/AdminExamGradeList'),
              },
              {
                name: 'admin-grade-exam-thresholds',
                path: 'grade-exam-thresholds/:examId',
                component: () => import('@/pages/admin/exam/AdminExamGradeThresholds'),
              },
              {
                name: 'admin-grade-exams-exam',
                path: 'grade-exam/:examId/user/:userId/task/:taskIndex?',
                component: () => import('@/pages/admin/exam/AdminExamGrade'),
              },
              {
                name: 'admin-current-exam-overview',
                path: 'current-exam/:examId/',
                component: () => import('@/pages/admin/exam/AdminExamCurrent'),
              },
            ],
          },

        ],
      },
    ],
  },
]
