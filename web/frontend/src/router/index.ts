import { createRouter, createWebHistory } from 'vue-router'
import { routes } from './routes'
import { apiClient } from '@/utils/apiClient'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes,
})

router.beforeEach(async (to, _, next) => {
  if (to.name && ['signin', 'signup'].includes(to.name.toString())) {
    next()
    return
  }

  try {
    await apiClient.get('/auth/is-authenticated')
  } catch (error) {
    next({ name: 'signin' })
    return
  }
  next()
})

export default router
