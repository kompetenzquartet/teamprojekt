import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import { apiClient } from '@/utils/apiClient'
import type { Course, CourseChapter } from '@/model/types/course'
import type { Chapter } from '@/model/types/chapter'

const [provideCourseChapter, useCourseChapter] = createInjectionState(() => {
  const isLoadingCourseChapter = ref(false)
  const courseChapter = ref<CourseChapter>()

  async function getCourseChapter(id: Course['_id'], chapterId: Chapter['_id']) {
    isLoadingCourseChapter.value = true

    const response = await apiClient.get<CourseChapter>(`/courses/${id}/chapter/${chapterId}`).finally(() => {
      isLoadingCourseChapter.value = false
    })
    courseChapter.value = response.data

    return response.data
  }

  return {
    isLoadingCourseChapter,
    courseChapter,
    getCourseChapter,
  }
})

export { provideCourseChapter }

export function useCourseChapterOrThrow() {
  const courseChapter = useCourseChapter()
  if (!courseChapter) {
    throw new Error('Please call `provideCourseChapter` on the appropriate parent component')
  }
  return courseChapter
}
