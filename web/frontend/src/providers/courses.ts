import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import { apiClient } from '@/utils/apiClient'
import type { Course } from '@/model/types/course'

const [provideCourses, useCourses] = createInjectionState(() => {
  const isLoadingCourses = ref(false)
  const isSynchronizingCourses = ref(false)
  const courses = ref<Course[]>([])

  async function getCourses() {
    isLoadingCourses.value = true
    const response = await apiClient.get<Course[]>('/courses').finally(() => {
      isLoadingCourses.value = false
    })
    courses.value = response.data

    return response
  }

  async function getSubscribedCourses() {
    isLoadingCourses.value = true
    const response = await apiClient.get<Course[]>(`/courses/subscribed/list`).finally(() => {
      isLoadingCourses.value = false
    })
    courses.value = response.data

    return response
  }

  async function synchronizeCourses() {
    isSynchronizingCourses.value = true

    const response = await apiClient.post('/synchronize').finally(() => {
      isSynchronizingCourses.value = false
    })

    return response
  }

  return {
    getCourses,
    getSubscribedCourses,
    isLoadingCourses,
    courses,
    isSynchronizingCourses,
    synchronizeCourses,
  }
})

export { provideCourses }

export function useCoursesOrThrow() {
  const courses = useCourses()
  if (!courses) {
    throw new Error('Please call `provideCourses` on the appropriate parent component')
  }
  return courses
}
