import { useStorage } from '@vueuse/core'

export const visitedChapterTask = useStorage<{path: string; name: string} | null>('visited-chapter-task', null)
