import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { ExamSolution } from '@/model/types/examSolution'
import { apiClient } from '@/utils/apiClient'
import { UserExamSolution } from '@/model/types/userExamSolution'

const [provideUserExamSolutions, useUserExamSolutions] = createInjectionState(() => {
  const isLoadingUserExamSolutions = ref(false)
  const userExamSolutions = ref<UserExamSolution[]>([])

  async function getUserExamSolutions(id: string) {
    isLoadingUserExamSolutions.value = true

    const response = await apiClient.get<ExamSolution[]>('/exam-solution/exam/' + id).finally(() => {
      isLoadingUserExamSolutions.value = false
    })
    userExamSolutions.value = response.data

    return response.data
  }

  return {
    getUserExamSolutions,
    isLoadingUserExamSolutions,
    userExamSolutions,
  }
})

export { provideUserExamSolutions }

export function useUserExamSolutionsOrThrow() {
  const userExamSolutions = useUserExamSolutions()
  if (!userExamSolutions) {
    throw new Error('Please call `provideExamSolutions` on the appropriate parent component')
  }
  return userExamSolutions
}
