import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import { apiClient } from '@/utils/apiClient'
import type { QuizTaskSet, QuizTaskSetRequest } from '@/model/types/quizTaskSet'

const [provideQuizTaskSet, useQuizTaskSet] = createInjectionState(() => {
  const isLoadingQuizTaskSet = ref(false)
  const isCreatingQuizTaskSet = ref(false)
  const isUpdatingQuizTaskSet = ref(false)
  const isDeletingQuizTaskSet = ref(false)
  const quizTaskSet = ref<QuizTaskSet>()

  async function getQuizTaskSet(id: QuizTaskSet['_id']) {
    isLoadingQuizTaskSet.value = true

    const response = await apiClient.get<QuizTaskSet>(`/quiztasksets/${id}`).finally(() => {
      isLoadingQuizTaskSet.value = false
    })
    quizTaskSet.value = response.data

    return response.data
  }

  async function createQuizTaskSet(data: QuizTaskSetRequest) {
    isCreatingQuizTaskSet.value = true
    await apiClient.post<QuizTaskSet>(`/quiztasksets`, data).finally(() => {
      isCreatingQuizTaskSet.value = false
    })
  }

  async function updateQuizTaskSet(id: QuizTaskSet['_id'], data: QuizTaskSetRequest) {
    isUpdatingQuizTaskSet.value = true
    await apiClient.patch<QuizTaskSet>(`/quiztasksets/${id}`, data).finally(() => {
      isUpdatingQuizTaskSet.value = false
    })
  }

  async function deleteQuizTaskSet(id: QuizTaskSet['_id']) {
    isDeletingQuizTaskSet.value = true
    await apiClient.delete<never>(`/quiztasksets/${id}`).finally(() => {
      isDeletingQuizTaskSet.value = false
    })
  }

  return {
    isLoadingQuizTaskSet,
    isCreatingQuizTaskSet,
    isUpdatingQuizTaskSet,
    isDeletingQuizTaskSet,
    quizTaskSet,
    getQuizTaskSet,
    createQuizTaskSet,
    updateQuizTaskSet,
    deleteQuizTaskSet,
  }
})

export { provideQuizTaskSet }

export function useQuizTaskSetOrThrow() {
  const quizTaskSet = useQuizTaskSet()
  if (!quizTaskSet) {
    throw new Error('Please call `provideQuizTaskSet` on the appropriate parent component')
  }
  return quizTaskSet
}
