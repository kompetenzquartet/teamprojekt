import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import { apiClient } from '@/utils/apiClient'
import type { TaskProgress } from '@/model/types/taskProgress'

export function loadUserChapterTaskProgresses({ userId, semesterId, lectureId, courseId, chapterId }: Pick<TaskProgress, 'userId' | 'semesterId' | 'lectureId' | 'courseId' | 'chapterId'>) {
  return apiClient.get<TaskProgress[]>(
    `/taskProgresses/user/${userId}/semester/${semesterId}/lecture/${lectureId}/course/${courseId}/chapter/${chapterId}`
  )
}

const [provideTaskProgresses, useTaskProgresses] = createInjectionState(() => {
  const isLoadingTaskProgresses = ref(false)
  const isLoadingUserTaskProgresses = ref(false)
  const isLoadingUserChapterTaskProgresses = ref(false)
  const taskProgresses = ref<TaskProgress[]>([])
  const hasLoadedUserChapterTaskProgresses = ref(false)
  const userTaskProgresses = ref<TaskProgress[]>([])
  const userChapterTaskProgresses = ref<TaskProgress[]>([])

  async function getUserTaskProgresses(userId: TaskProgress['userId']) {
    isLoadingUserTaskProgresses.value = true

    userTaskProgresses.value = (await apiClient.get<TaskProgress[]>(`/taskProgresses/user/${userId}`).finally(() => {
      isLoadingUserTaskProgresses.value = false
    })).data
  }

  async function getUserChapterTaskProgresses(params: Parameters<typeof loadUserChapterTaskProgresses>[0]) {
    isLoadingUserChapterTaskProgresses.value = true

    userChapterTaskProgresses.value = (await loadUserChapterTaskProgresses(params).finally(() => {
      hasLoadedUserChapterTaskProgresses.value = true
      isLoadingUserChapterTaskProgresses.value = false
    })).data
  }

  async function getTaskProgresses() {
    isLoadingTaskProgresses.value = true

    const response = await apiClient.get<TaskProgress[]>('/taskProgresses').finally(() => {
      isLoadingTaskProgresses.value = false
    })
    taskProgresses.value = response.data

    return response
  }

  return {
    isLoadingUserChapterTaskProgresses,
    isLoadingUserTaskProgresses,
    hasLoadedUserChapterTaskProgresses,
    isLoadingTaskProgresses,
    userChapterTaskProgresses,
    userTaskProgresses,
    taskProgresses,
    getUserChapterTaskProgresses,
    getUserTaskProgresses,
    getTaskProgresses,
  }
})

export { provideTaskProgresses }

export function useTaskProgressesOrThrow() {
  const taskProgresses = useTaskProgresses()
  if (!taskProgresses) {
    throw new Error('Please call `provideTaskProgresses` on the appropriate parent component')
  }
  return taskProgresses
}
