import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { Lecture, SubscribedLecture } from '@/model/types/lecture'
import { apiClient } from '@/utils/apiClient'
import type { Semester } from '@/model/types/semester'
import { useAuthOrThrow } from './auth'

const [provideLectures, useLectures] = createInjectionState(() => {
  const isLoadingLectures = ref(false)
  const isLoadingSubscribedLectures = ref(false)
  const hasLoadedSubscribedLectures = ref(false)
  const lectures = ref<Lecture[]>([])
  const subscribedLectures = ref<SubscribedLecture[]>([])
  const { user } = useAuthOrThrow()

  async function getLectures() {
    isLoadingLectures.value = true

    const response = await apiClient.get<Lecture[]>('/lectures').finally(() => {
      isLoadingLectures.value = false
    })
    lectures.value = response.data

    return response
  }

  async function getSubscribedLectures(semesterId: Semester['_id']) {
    isLoadingSubscribedLectures.value = true

    const response = await apiClient.get<SubscribedLecture[]>(`users/${user.value?._id}/semesters/${semesterId}/lectures`).finally(() => {
      isLoadingSubscribedLectures.value = false
      hasLoadedSubscribedLectures.value = true
    })

    subscribedLectures.value = response.data

    return response.data
  }

  return {
    getLectures,
    isLoadingLectures,
    lectures,
    getSubscribedLectures,
    isLoadingSubscribedLectures,
    subscribedLectures,
    hasLoadedSubscribedLectures,
  }
})

export { provideLectures }

export function useLecturesOrThrow() {
  const lectures = useLectures()
  if (!lectures) {
    throw new Error('Please call `provideLectures` on the appropriate parent component')
  }
  return lectures
}
