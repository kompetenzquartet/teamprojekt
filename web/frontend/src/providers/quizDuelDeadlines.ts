import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { QuizDuelDeadline } from '@/model/types/quizDuelDeadline'
import { apiClient } from '@/utils/apiClient'

const [provideQuizDuelDeadlines, useQuizDuelDeadlines] = createInjectionState(() => {
  const isLoadingQuizDuelDeadlines = ref(false)
  const quizDuelDeadlines = ref<QuizDuelDeadline[]>([])

  async function getQuizDuelDeadlines() {
    isLoadingQuizDuelDeadlines.value = true

    const response = await apiClient.get<QuizDuelDeadline[]>('/quiz-duel-deadlines').finally(() => {
      isLoadingQuizDuelDeadlines.value = false
    })
    quizDuelDeadlines.value = response.data

    return response
  }

  async function getSubscribedQuizDuelDeadlines() {
    isLoadingQuizDuelDeadlines.value = true

    const response = await apiClient.get<QuizDuelDeadline[]>('/quiz-duel-deadlines/subscribed').finally(() => {
      isLoadingQuizDuelDeadlines.value = false
    })
    quizDuelDeadlines.value = response.data

    return response
  }

  return {
    getQuizDuelDeadlines,
    getSubscribedQuizDuelDeadlines,
    isLoadingQuizDuelDeadlines,
    quizDuelDeadlines,
  }
})

export { provideQuizDuelDeadlines }

export function useQuizDuelDeadlinesOrThrow() {
  const quizDuelDeadlines = useQuizDuelDeadlines()
  if (!quizDuelDeadlines) {
    throw new Error('Please call `provideQuizDuelDeadlines` on the appropriate parent component')
  }
  return quizDuelDeadlines
}
