import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { User, UserProfileRequest } from '@/model/types/user'
import { apiClient } from '@/utils/apiClient'
import { useAuthOrThrow } from './auth'

const [provideUser, useUser] = createInjectionState(() => {
  const isUpdatingUser = ref(false)
  const { user } = useAuthOrThrow()

  async function updateUserProfile(id: User['_id'], data: UserProfileRequest) {
    isUpdatingUser.value = true

    const response = await apiClient.patch<User>(`/users/${id}`, data).finally(() => {
      isUpdatingUser.value = false
    })
    user.value = response.data

    return response
  }

  return {
    updateUserProfile,
    isUpdatingUser,
  }
})

export { provideUser }

export function useUserOrThrow() {
  const user = useUser()
  if (!user) {
    throw new Error('Please call `provideUser` on the appropriate parent component')
  }
  return user
}
