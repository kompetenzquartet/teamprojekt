import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import { apiClient } from '@/utils/apiClient'
import type { Course } from '@/model/types/course'

const [provideCourse, useCourse] = createInjectionState(() => {
  const isLoadingCourse = ref(false)
  const isUpdatingCourse = ref(false)
  const course = ref<Course>()

  async function getCourse(id: Course['_id']) {
    isLoadingCourse.value = true

    const response = await apiClient.get<Course>(`/courses/${id}`).finally(() => {
      isLoadingCourse.value = false
    })
    course.value = response.data

    return response.data
  }

  async function updateCourse(id: Course['_id'], data: Pick<Course, 'lectures'>) {
    isUpdatingCourse.value = true
    await apiClient.patch<Course>(`/courses/${id}`, data).finally(() => {
      isUpdatingCourse.value = false
    })
  }

  return {
    isLoadingCourse,
    isUpdatingCourse,
    course,
    getCourse,
    updateCourse,
  }
})

export { provideCourse }

export function useCourseOrThrow() {
  const course = useCourse()
  if (!course) {
    throw new Error('Please call `provideCourse` on the appropriate parent component')
  }
  return course
}
