import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { Semester, SemesterView } from '@/model/types/semester'
import { apiClient } from '@/utils/apiClient'

const [provideSemesters, useSemesters] = createInjectionState(() => {
  const isLoadingSemesters = ref(false)
  const semesters = ref<Semester[]>([])
  const isLoadingSemesterViews = ref(false)
  const semesterViews = ref<SemesterView[]>([])

  async function getSemesters() {
    isLoadingSemesters.value = true

    const response = await apiClient.get<Semester[]>('/semesters').finally(() => {
      isLoadingSemesters.value = false
    })
    semesters.value = response.data

    return response
  }

  async function getSemesterViews() {
    isLoadingSemesterViews.value = true

    const response = await apiClient.get<SemesterView[]>('/semesters/view').finally(() => {
      isLoadingSemesterViews.value = false
    })
    semesterViews.value = response.data

    return response
  }

  return {
    getSemesters,
    isLoadingSemesters,
    semesters,
    isLoadingSemesterViews,
    getSemesterViews,
    semesterViews,
  }
})

export { provideSemesters }

export function useSemestersOrThrow() {
  const semesters = useSemesters()
  if (!semesters) {
    throw new Error('Please call `provideSemesters` on the appropriate parent component')
  }
  return semesters
}
