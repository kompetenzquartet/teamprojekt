import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import { apiClient } from '@/utils/apiClient'
import type { Settings } from '@/model/types/settings'

const [provideSettings, useSettings] = createInjectionState(() => {
  const isLoadingSettings = ref(false)
  const isUpdatingSettings = ref(false)
  const settings = ref<Settings>()

  async function getSettings() {
    isLoadingSettings.value = true

    const response = await apiClient.get<Settings>('/settings').finally(() => {
      isLoadingSettings.value = false
    })

    settings.value = response.data

    return response
  }

  async function updateSettings(data: Settings) {
    isUpdatingSettings.value = true

    return apiClient.patch<Settings>(`/settings`, data).finally(() => {
      isUpdatingSettings.value = false
    })
  }

  return {
    isLoadingSettings,
    isUpdatingSettings,
    settings,
    getSettings,
    updateSettings,
  }
})

export { provideSettings }

export function useSettingsOrThrow() {
  const settings = useSettings()
  if (!settings) {
    throw new Error('Please call `provideSettings` on the appropriate parent component')
  }
  return settings
}
