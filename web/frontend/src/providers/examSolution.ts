import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import { apiClient } from '@/utils/apiClient'
import type { ExamSolution } from '@/model/types/examSolution'

const [provideExamSolution, useExamSolution] = createInjectionState(() => {
  const isLoadingExamSolution = ref(false)
  const isUpdatingExamSolution = ref(false)
  const isCreatingExamSolution = ref(false)
  const isDeletingExamSolution = ref(false)
  const examSolution = ref<ExamSolution>()

  async function getExamSolution(id: ExamSolution['_id']) {
    isLoadingExamSolution.value = true
    const response = await apiClient.get<ExamSolution>(`/exam-solution/${id}`).finally(() => {
      isLoadingExamSolution.value = false
    })
    examSolution.value = response.data
    return response.data
  }

  async function updateExamSolution(id: ExamSolution['_id'], data: Pick<ExamSolution, '_id'>) {
    isUpdatingExamSolution.value = true
    await apiClient.put<ExamSolution>(`/exam-solution/${id}`, data).finally(() => {
      isUpdatingExamSolution.value = false
    })
  }

  async function createExamSolution(data: Omit<ExamSolution, '_id'>) {
    isCreatingExamSolution.value = true
    await apiClient.post<ExamSolution>(`/exam-solution`, data).finally(() => {
      isCreatingExamSolution.value = false
    })
  }

  async function deleteExamSolution(id: ExamSolution['_id']) {
    isDeletingExamSolution.value = true
    await apiClient.delete<ExamSolution>(`/exam-solution/${id}`).finally(() => {
      isDeletingExamSolution.value = false
    })
  }

  return {
    isLoadingExamSolution,
    isUpdatingExamSolution,
    isCreatingExamSolution,
    isDeletingExamSolution,
    examSolution,
    getExamSolution,
    updateExamSolution,
    createExamSolution,
    deleteExamSolution,
  }
})

export { provideExamSolution }

export function useExamSolutionOrThrow() {
  const examSolution = useExamSolution()
  if (!examSolution) {
    throw new Error('Please call `provideExamSolution` on the appropriate parent component')
  }
  return examSolution
}
