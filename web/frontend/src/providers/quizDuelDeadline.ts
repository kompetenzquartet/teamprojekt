import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import { apiClient } from '@/utils/apiClient'
import type { QuizDuelDeadline } from '@/model/types/quizDuelDeadline'

const [provideQuizDuelDeadline, useQuizDuelDeadline] = createInjectionState(() => {
  const isCreatingQuizDuelDeadline = ref(false)
  const isUpdatingQuizDuelDeadline = ref(false)
  const isDeletingQuizDuelDeadline = ref(false)

  async function createQuizDuelDeadline(data: Omit<QuizDuelDeadline, '_id'>) {
    isCreatingQuizDuelDeadline.value = true
    await apiClient.post<QuizDuelDeadline>(`/quiz-duel-deadline`, data).finally(() => {
      isCreatingQuizDuelDeadline.value = false
    })
  }

  async function updateQuizDuelDeadline(id: QuizDuelDeadline['_id'], data: Omit<QuizDuelDeadline, '_id'>) {
    isUpdatingQuizDuelDeadline.value = true
    await apiClient.put<QuizDuelDeadline>(`/quiz-duel-deadline/${id}`, data).finally(() => {
      isUpdatingQuizDuelDeadline.value = false
    })
  }

  async function deleteQuizDuelDeadline(id: QuizDuelDeadline['_id']) {
    isDeletingQuizDuelDeadline.value = true
    await apiClient.delete<QuizDuelDeadline>(`/quiz-duel-deadline/${id}`).finally(() => {
      isDeletingQuizDuelDeadline.value = false
    })
  }

  return {
    isCreatingQuizDuelDeadline,
    isUpdatingQuizDuelDeadline,
    isDeletingQuizDuelDeadline,
    createQuizDuelDeadline,
    updateQuizDuelDeadline,
    deleteQuizDuelDeadline,
  }
})

export { provideQuizDuelDeadline }

export function useQuizDuelDeadlineOrThrow() {
  const quizDuelDeadline = useQuizDuelDeadline()
  if (!quizDuelDeadline) {
    throw new Error('Please call `provideQuizDuelDeadline` on the appropriate parent component')
  }
  return quizDuelDeadline
}
