import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import { apiClient } from '@/utils/apiClient'
import type { Exam } from '@/model/types/exam'

const [provideExam, useExam] = createInjectionState(() => {
  const isLoadingExam = ref(false)
  const isUpdatingExam = ref(false)
  const isCreatingExam = ref(false)
  const isDeletingExam = ref(false)
  const exam = ref<Exam>()

  async function getExam(id: Exam['_id']) {
    isLoadingExam.value = true
    const response = await apiClient.get<Exam>(`/exam/${id}`).finally(() => {
      isLoadingExam.value = false
    })
    exam.value = response.data
    return response.data
  }

  async function getUserExam(userId: string, examId: string) {
    isLoadingExam.value = true
    const response = await apiClient.get<Exam>(`/exam/${examId}/user/${userId}`).finally(() => {
      isLoadingExam.value = false
    })
    exam.value = response.data
    return response.data
  }

  async function updateExam(id: Exam['_id'], data: Pick<Exam, '_id'>) {
    isUpdatingExam.value = true
    return await apiClient.put<Exam>(`/exam/${id}`, data).finally(() => {
      isUpdatingExam.value = false
    })
  }

  async function createExam(data: Omit<Exam, '_id'>) {
    isCreatingExam.value = true
    return await apiClient.post<Exam>(`/exam`, data).finally(() => {
      isCreatingExam.value = false
    })
  }

  async function deleteExam(id: Exam['_id']) {
    isDeletingExam.value = true
    await apiClient.delete<Exam>(`/exam/${id}`).finally(() => {
      isDeletingExam.value = false
    })
  }

  async function startExam(id: Exam['_id'], data: Omit<Exam, '_id'>) {
    isUpdatingExam.value = true
    const response = await apiClient.put<Exam>(`/exam/start/${id}`, data)
      .finally(() => {
        isUpdatingExam.value = false
      })
    exam.value = response.data

    return response
  }

  async function assignTasks(id: Exam['_id'], data: any) {
    isUpdatingExam.value = true
    await apiClient.put<Exam>(`/exam/assign-tasks/${id}`, data).finally(() => {
      isUpdatingExam.value = false
    })
  }

  return {
    isLoadingExam,
    isUpdatingExam,
    isCreatingExam,
    isDeletingExam,
    exam,
    getExam,
    getUserExam,
    updateExam,
    createExam,
    deleteExam,
    startExam,
    assignTasks,
  }
})

export { provideExam }

export function useExamOrThrow() {
  const exam = useExam()
  if (!exam) {
    throw new Error('Please call `provideExam` on the appropriate parent component')
  }
  return exam
}
