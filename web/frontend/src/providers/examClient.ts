import { store } from '@/utils/apiClient'
import { createInjectionState, useWebSocket } from '@vueuse/core'

const socketProtocol = !import.meta.env.PROD ? 'ws:' : 'wss:'
const socketUrl = socketProtocol + (!import.meta.env.PROD ? '//' : '//api.') + location.hostname + '/examws/connect?access_token='

const [provideExamClient, useExamClient] = createInjectionState(() => {
  const socket = useWebSocket<string>(socketUrl + store.value.accessToken, {
    autoReconnect: {
      retries: 3,
      delay: 3000,
    },
    immediate: true,
  })

  return socket
})

export { provideExamClient }

export function useExamClientOrThrow() {
  const examClient = useExamClient()
  if (!examClient) {
    throw new Error('Please call `provideExamClient` on the appropriate parent component')
  }
  return examClient
}
