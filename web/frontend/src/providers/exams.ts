import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { Exam } from '@/model/types/exam'
import { apiClient } from '@/utils/apiClient'

const [provideExams, useExams] = createInjectionState(() => {
  const isLoadingExams = ref(false)
  const exams = ref<Exam[]>([])

  async function getExams() {
    isLoadingExams.value = true

    const response = await apiClient.get<Exam[]>('/exam').finally(() => {
      isLoadingExams.value = false
    })
    exams.value = response.data

    return response
  }

  async function getUsersExams(userId: string) {
    isLoadingExams.value = true

    const response = await apiClient.get<Exam[]>(`/exam/user/${userId}`).finally(() => {
      isLoadingExams.value = false
    })
    exams.value = response.data

    return response
  }
  return {
    getExams,
    getUsersExams,
    isLoadingExams,
    exams,
  }
})

export { provideExams }

export function useExamsOrThrow() {
  const exams = useExams()
  if (!exams) {
    throw new Error('Please call `provideExams` on the appropriate parent component')
  }
  return exams
}
