import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import { apiClient } from '@/utils/apiClient'
import type { QuizTaskSet } from '@/model/types/quizTaskSet'

const [provideQuizTaskSets, useQuizTaskSets] = createInjectionState(() => {
  const isLoadingQuizTaskSets = ref(false)
  const quizTaskSets = ref<QuizTaskSet[]>([])

  async function getQuizTaskSets() {
    isLoadingQuizTaskSets.value = true
    const response = await apiClient.get<QuizTaskSet[]>('/quiztasksets').finally(() => {
      isLoadingQuizTaskSets.value = false
    })
    quizTaskSets.value = response.data

    return response
  }

  return {
    isLoadingQuizTaskSets,
    quizTaskSets,
    getQuizTaskSets,
  }
})

export { provideQuizTaskSets }

export function useQuizTaskSetsOrThrow() {
  const quizTaskSets = useQuizTaskSets()
  if (!quizTaskSets) {
    throw new Error('Please call `provideQuizTaskSets` on the appropriate parent component')
  }
  return quizTaskSets
}
