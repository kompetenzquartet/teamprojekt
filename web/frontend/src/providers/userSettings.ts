import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { User, UserSettings } from '@/model/types/user'
import { apiClient } from '@/utils/apiClient'

const [provideUserSettings, useUserSettings] = createInjectionState(() => {
  const isLoadingUserSettings = ref(false)
  const isUpdatingUserSettings = ref(false)
  const userSettings = ref<UserSettings>()

  async function getUserSettings(id: User['_id']) {
    isLoadingUserSettings.value = true

    const response = await apiClient.get<UserSettings>(`/users/${id}/settings`).finally(() => {
      isLoadingUserSettings.value = false
    })
    userSettings.value = response.data

    return response
  }

  async function updateUserSettings(id: User['_id'], data: UserSettings) {
    isUpdatingUserSettings.value = true

    return apiClient.patch<UserSettings>(`/users/${id}/settings`, data).finally(() => {
      isUpdatingUserSettings.value = false
    })
  }

  return {
    getUserSettings,
    isLoadingUserSettings,
    userSettings,
    updateUserSettings,
    isUpdatingUserSettings,
  }
})

export { provideUserSettings }

export function useUserSettingsOrThrow() {
  const userSettings = useUserSettings()
  if (!userSettings) {
    throw new Error('Please call `provideUserSettings` on the appropriate parent component')
  }
  return userSettings
}
