import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import { apiClient } from '@/utils/apiClient'
import type { QuizDuel, QuizDuelRequest, QuizDuelView } from '@/model/types/quizDuel'

const [provideQuizDuel, useQuizDuel] = createInjectionState(() => {
  const isLoadingQuizDuel = ref(false)
  const isCreatingQuizDuel = ref(false)
  const isDeletingQuizDuel = ref(false)
  const isLoadingActiveQuizDuel = ref(false)
  const isStartingQuizDuel = ref(false)
  const isStoppingQuizDuel = ref(false)
  const isSkippingQuizDuelTask = ref(false)
  const quizDuel = ref<QuizDuel>()
  const activeQuizDuel = ref<QuizDuelView>()

  async function getQuizDuel(id: QuizDuel['_id']) {
    isLoadingQuizDuel.value = true

    const response = await apiClient.get<QuizDuel>(`/quizduels/${id}`).finally(() => {
      isLoadingQuizDuel.value = false
    })
    quizDuel.value = response.data

    return response.data
  }

  async function getActiveQuizDuel() {
    isLoadingActiveQuizDuel.value = true

    const response = await apiClient.get<QuizDuelView>(`/quizduel`).finally(() => {
      isLoadingActiveQuizDuel.value = false
    })
    activeQuizDuel.value = response.data

    return response.data
  }

  async function createQuizDuel(data: QuizDuelRequest) {
    isCreatingQuizDuel.value = true
    await apiClient.post<QuizDuel>(`/quizduels`, data).finally(() => {
      isCreatingQuizDuel.value = false
    })
  }

  async function deleteQuizDuel(id: QuizDuel['_id']) {
    isDeletingQuizDuel.value = true
    await apiClient.delete<never>(`/quizduels/${id}`).finally(() => {
      isDeletingQuizDuel.value = false
    })
  }

  async function startQuizDuel(id: QuizDuel['_id']) {
    isStartingQuizDuel.value = true
    await apiClient.post<never>(`/quizduels/${id}/start`).finally(() => {
      isStartingQuizDuel.value = false
    })
  }

  async function stopQuizDuel(id: QuizDuel['_id']) {
    isStoppingQuizDuel.value = true
    await apiClient.post<never>(`/quizduels/${id}/stop`).finally(() => {
      isStoppingQuizDuel.value = false
    })
  }

  async function skipQuizDuelTask(id: QuizDuel['_id']) {
    isSkippingQuizDuelTask.value = true
    await apiClient.post<never>(`/quizduels/${id}/skip-task`).finally(() => {
      isSkippingQuizDuelTask.value = false
    })
  }

  return {
    isLoadingQuizDuel,
    isDeletingQuizDuel,
    isLoadingActiveQuizDuel,
    isCreatingQuizDuel,
    isStartingQuizDuel,
    isStoppingQuizDuel,
    isSkippingQuizDuelTask,
    activeQuizDuel,
    quizDuel,
    getQuizDuel,
    getActiveQuizDuel,
    deleteQuizDuel,
    createQuizDuel,
    startQuizDuel,
    stopQuizDuel,
    skipQuizDuelTask,
  }
})

export { provideQuizDuel }

export function useQuizDuelOrThrow() {
  const quizDuel = useQuizDuel()
  if (!quizDuel) {
    throw new Error('Please call `provideQuizDuel` on the appropriate parent component')
  }
  return quizDuel
}
