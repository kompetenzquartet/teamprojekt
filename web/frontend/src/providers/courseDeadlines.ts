import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { CourseDeadline } from '@/model/types/courseDeadline'
import { apiClient } from '@/utils/apiClient'

const [provideCourseDeadlines, useCourseDeadlines] = createInjectionState(() => {
  const isLoadingCourseDeadlines = ref(false)
  const courseDeadlines = ref<CourseDeadline[]>([])

  async function getCourseDeadlines() {
    isLoadingCourseDeadlines.value = true

    const response = await apiClient.get<CourseDeadline[]>('/course-deadlines').finally(() => {
      isLoadingCourseDeadlines.value = false
    })
    courseDeadlines.value = response.data

    return response
  }

  async function getSubscribedCourseDeadlines() {
    isLoadingCourseDeadlines.value = true

    const response = await apiClient.get<CourseDeadline[]>('/course-deadlines/subscribed').finally(() => {
      isLoadingCourseDeadlines.value = false
    })
    courseDeadlines.value = response.data

    return response
  }

  return {
    getCourseDeadlines,
    getSubscribedCourseDeadlines,
    isLoadingCourseDeadlines,
    courseDeadlines,
  }
})

export { provideCourseDeadlines }

export function useCourseDeadlinesOrThrow() {
  const courseDeadlines = useCourseDeadlines()
  if (!courseDeadlines) {
    throw new Error('Please call `provideCourseDeadlines` on the appropriate parent component')
  }
  return courseDeadlines
}
