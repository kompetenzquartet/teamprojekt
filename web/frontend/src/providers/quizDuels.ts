import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import { apiClient } from '@/utils/apiClient'
import type { QuizDuel, QuizDuelView } from '@/model/types/quizDuel'

const [provideQuizDuels, useQuizDuels] = createInjectionState(() => {
  const isLoadingQuizDuels = ref(false)
  const quizDuels = ref<QuizDuelView[]>([])
  const isLoadingOriginalQuizDuels = ref(false)
  const originalQuizDuels = ref<QuizDuel[]>([])

  async function getQuizDuels() {
    isLoadingQuizDuels.value = true
    const response = await apiClient.get<QuizDuelView[]>('/quizduels').finally(() => {
      isLoadingQuizDuels.value = false
    })
    quizDuels.value = response.data

    return response
  }

  async function getOriginalQuizDuels() {
    isLoadingOriginalQuizDuels.value = true
    const response = await apiClient.get<QuizDuel[]>('/quizduels/original').finally(() => {
      isLoadingOriginalQuizDuels.value = false
    })
    originalQuizDuels.value = response.data

    return response
  }

  return {
    getQuizDuels,
    getOriginalQuizDuels,
    isLoadingQuizDuels,
    isLoadingOriginalQuizDuels,
    quizDuels,
    originalQuizDuels,
  }
})

export { provideQuizDuels }

export function useQuizDuelsOrThrow() {
  const quizDuels = useQuizDuels()
  if (!quizDuels) {
    throw new Error('Please call `provideQuizDuels` on the appropriate parent component')
  }
  return quizDuels
}
