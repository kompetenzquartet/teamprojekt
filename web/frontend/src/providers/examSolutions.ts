import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { ExamSolution } from '@/model/types/examSolution'
import { apiClient } from '@/utils/apiClient'

const [provideExamSolutions, useExamSolutions] = createInjectionState(() => {
  const isLoadingExamSolutions = ref(false)
  const examSolutions = ref<ExamSolution[]>([])

  async function getExamSolutions() {
    isLoadingExamSolutions.value = true

    const response = await apiClient.get<ExamSolution[]>('/exam-solution').finally(() => {
      isLoadingExamSolutions.value = false
    })
    examSolutions.value = response.data

    return response
  }

  async function updateExamSolutions(data: Pick<ExamSolution[]>) {
    isLoadingExamSolutions.value = true
    await apiClient.put<ExamSolution[]>('/exam-solutions/', data).finally(() => {
      isLoadingExamSolutions.value = false
    })
  }

  async function authenticateUser(examId: string, authCode: string) {
    return await apiClient.post('/exam-solution/auth/' + examId, {
      authCode,
    })
  }

  async function getUserExamSolutions(examId: string) {
    isLoadingExamSolutions.value = true
    const response = await apiClient.get<ExamSolution[]>(`/exam-solution/user/${examId}`).finally(() => {
      isLoadingExamSolutions.value = false
    })
    examSolutions.value = response.data
    return response
  }
  return {
    getExamSolutions,
    isLoadingExamSolutions,
    examSolutions,
    updateExamSolutions,
    authenticateUser,
    getUserExamSolutions,
  }
}

)

export { provideExamSolutions }

export function useExamSolutionsOrThrow() {
  const examSolutions = useExamSolutions()
  if (!examSolutions) {
    throw new Error('Please call `provideExamSolutions` on the appropriate parent component')
  }
  return examSolutions
}
