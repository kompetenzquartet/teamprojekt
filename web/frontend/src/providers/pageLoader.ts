import { createInjectionState } from '@vueuse/core'
import { type Ref, ref, watch } from 'vue'

const [providePageLoader, usePageLoader] = createInjectionState((loader: Ref<boolean>) => {
  const loading = ref(loader.value)

  watch(loader, value => {
    loading.value = value
  })

  return {
    loading,
  }
})

export { providePageLoader, usePageLoader }
