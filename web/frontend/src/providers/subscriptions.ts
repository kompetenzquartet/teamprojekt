import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { Subscription } from '@/model/types/subscription'
import { apiClient } from '@/utils/apiClient'
import type { User } from '@/model/types/user'

const [provideSubscriptions, useSubscriptions] = createInjectionState(() => {
  const isLoadingUserSubscriptions = ref(false)
  const isLoadingSubscriptions = ref(false)
  const userSubscriptions = ref<Subscription[]>([])
  const subscriptions = ref<Subscription[]>([])

  async function getUserSubscriptions(userId: User['_id']) {
    isLoadingUserSubscriptions.value = true
    userSubscriptions.value = (await apiClient.get<Subscription[]>(`/subscriptions/user/${userId}`).finally(() => {
      isLoadingUserSubscriptions.value = false
    })).data
  }

  async function getSubscriptions() {
    isLoadingSubscriptions.value = true
    const response = await apiClient.get<Subscription[]>('/subscriptions').finally(() => {
      isLoadingSubscriptions.value = false
    })
    subscriptions.value = response.data
    return response
  }

  return {
    isLoadingUserSubscriptions,
    isLoadingSubscriptions,
    userSubscriptions,
    subscriptions,
    getUserSubscriptions,
    getSubscriptions,
  }
})

export { provideSubscriptions }

export function useSubscriptionsOrThrow() {
  const subscriptions = useSubscriptions()
  if (!subscriptions) {
    throw new Error('Please call `provideSubscriptions` on the appropriate parent component')
  }
  return subscriptions
}
