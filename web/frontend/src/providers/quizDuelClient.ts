import { usePageSnackbarOrThrow } from '@/providers/pageSnackbar'
import { store } from '@/utils/apiClient'
import { createInjectionState, useWebSocket } from '@vueuse/core'

const socketProtocol = !import.meta.env.PROD ? 'ws:' : 'wss:'
const socketUrl = socketProtocol + (!import.meta.env.PROD ? '//' : '//api.') + location.hostname + '/quizduel/connect?access_token='

const [provideQuizDuelClient, useQuizDuelClient] = createInjectionState(() => {
  const { snackbar, show } = usePageSnackbarOrThrow()

  return useWebSocket<string>(socketUrl + store.value.accessToken, {
    autoReconnect: {
      retries: 3,
      delay: 3000,
      onFailed() {
        snackbar.value = {
          text: `Konnte keine Verbindung zum Quiz Duell herstellen`,
          color: 'error',
        }
        show()
      },
    },
    immediate: false,
  })
})

export { provideQuizDuelClient }

export function useQuizDuelClientOrThrow() {
  const quizDuelClient = useQuizDuelClient()
  if (!quizDuelClient) {
    throw new Error('Please call `provideQuizDuelClient` on the appropriate parent component')
  }
  return quizDuelClient
}
