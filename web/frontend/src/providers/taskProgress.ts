import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import { apiClient } from '@/utils/apiClient'
import type { TaskProgress, TaskProgressRequest } from '@/model/types/taskProgress'

const [provideTaskProgress, useTaskProgress] = createInjectionState(() => {
  const isCreatingTaskProgress = ref(false)
  const isUpdatingTaskProgress = ref(false)

  async function createTaskProgress(data: TaskProgressRequest) {
    isCreatingTaskProgress.value = true

    return apiClient.post<TaskProgress>('/taskProgresses', data).finally(() => {
      isCreatingTaskProgress.value = false
    })
  }

  async function updateTaskProgress(id: TaskProgress['_id'], data: TaskProgressRequest) {
    isUpdatingTaskProgress.value = true

    return apiClient.patch<TaskProgress>(`/taskProgresses/${id}`, data).finally(() => {
      isUpdatingTaskProgress.value = false
    })
  }

  return {
    isCreatingTaskProgress,
    isUpdatingTaskProgress,
    createTaskProgress,
    updateTaskProgress,
  }
})

export { provideTaskProgress }

export function useTaskProgressOrThrow() {
  const taskProgress = useTaskProgress()
  if (!taskProgress) {
    throw new Error('Please call `provideTaskProgress` on the appropriate parent component')
  }
  return taskProgress
}
