import { ref } from 'vue'
import { VAlert } from 'vuetify/components'
import { createInjectionState } from '@vueuse/core'

type Alert = Pick<VAlert['$props'], 'title' | 'text' | 'type'> & { isCode?: boolean }

const [providePageAlert, usePageAlert] = createInjectionState(() => {
  const alert = ref<Alert>()

  function resetAlert() {
    alert.value = undefined
  }

  return {
    alert,
    resetAlert,
  }
})

export { providePageAlert, usePageAlert }

export function usePageAlertOrThrow() {
  const pageAlert = usePageAlert()
  if (!pageAlert) {
    throw new Error('Please call `providePageAlert` on the appropriate parent component')
  }
  return pageAlert
}
