import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { User } from '@/model/types/user'
import type { SigninRequest, SignupRequest } from '@/model/types/auth'
import { apiClient, store } from '@/utils/apiClient'

const [provideAuth, useAuth] = createInjectionState(() => {
  const isSigningUp = ref(false)
  const isSigningIn = ref(false)
  const isSigningInHtwg = ref(false)
  const isSigningOut = ref(false)
  const isRefreshingToken = ref(false)
  const isLoadingUser = ref(false)
  const user = ref<User>()

  async function getUser() {
    isLoadingUser.value = true

    const response = await apiClient.get<User>(`/auth/user`).finally(() => isLoadingUser.value = false)
    user.value = response.data

    return response
  }

  async function signUp(data: SignupRequest) {
    isSigningUp.value = true

    const response = await apiClient.post<User & {accessToken: string}>(`/auth/signup`, data)
      .finally(() => isSigningUp.value = false)

    const { accessToken, ...responseUser } = response.data
    store.value.accessToken = accessToken
    user.value = responseUser

    return response
  }

  async function signIn(data: SigninRequest) {
    isSigningIn.value = true

    const response = await apiClient.post<User & {accessToken: string}>(`/auth/signin`, data)
      .finally(() => isSigningIn.value = false)

    const { accessToken, ...responseUser } = response.data
    store.value.accessToken = accessToken
    user.value = responseUser

    return response
  }

  async function signInHtwg(data: SigninRequest) {
    isSigningInHtwg.value = true

    const response = await apiClient.post<User & {accessToken: string}>(`/auth/htwg`, data)
      .finally(() => isSigningInHtwg.value = false)

    const { accessToken, ...responseUser } = response.data
    store.value.accessToken = accessToken
    user.value = responseUser

    return response
  }

  async function signOut() {
    isSigningOut.value = true

    store.value.accessToken = ''
    user.value = undefined

    isSigningOut.value = false
  }

  async function refreshToken(id: User['_id']) {
    isRefreshingToken.value = true

    return apiClient.post<User>(`/auth/refresh/${id}`).finally(() => {
      isRefreshingToken.value = false
    })
  }

  return {
    isSigningIn,
    isSigningInHtwg,
    isSigningUp,
    isSigningOut,
    isRefreshingToken,
    isLoadingUser,
    signIn,
    signInHtwg,
    signUp,
    signOut,
    refreshToken,
    getUser,
    user,
  }
})

export { provideAuth }

export function useAuthOrThrow() {
  const auth = useAuth()
  if (!auth) {
    throw new Error('Please call `function` on the appropriate parent component')
  }
  return auth
}
