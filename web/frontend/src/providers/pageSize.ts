import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'

const [providePageSize, usePageSize] = createInjectionState(() => {
  const size = ref<'small' | 'normal'>('normal')

  return {
    size,
  }
})

export { providePageSize }

export function usePageSizeOrThrow() {
  const PageSize = usePageSize()
  if (!PageSize) {
    throw new Error('Please call `providePageSize` on the appropriate parent component')
  }
  return PageSize
}
