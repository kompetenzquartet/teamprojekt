import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { User } from '@/model/types/user'
import { apiClient } from '@/utils/apiClient'

const [provideUsers, useUsers] = createInjectionState(() => {
  const isLoadingUsers = ref(false)
  const users = ref<User[]>([])

  async function getUsers() {
    isLoadingUsers.value = true

    const response = await apiClient.get<User[]>('/users').finally(() => {
      isLoadingUsers.value = false
    })
    users.value = response.data

    return response
  }

  return {
    getUsers,
    isLoadingUsers,
    users,
  }
})

export { provideUsers }

export function useUsersOrThrow() {
  const users = useUsers()
  if (!users) {
    throw new Error('Please call `provideUsers` on the appropriate parent component')
  }
  return users
}
