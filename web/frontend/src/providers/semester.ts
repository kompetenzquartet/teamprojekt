import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { Semester } from '@/model/types/semester'
import { apiClient } from '@/utils/apiClient'

const [provideSemester, useSemester] = createInjectionState(() => {
  const isLoadingSemester = ref(false)
  const isCreatingSemester = ref(false)
  const isUpdatingSemester = ref(false)
  const isDeletingSemester = ref(false)
  const semester = ref<Semester>()

  async function getSemester(id: Semester['_id']) {
    isLoadingSemester.value = true

    const response = await apiClient.get<Semester>(`/semesters/${id}`).finally(() => {
      isLoadingSemester.value = false
    })
    semester.value = response.data

    return response.data
  }

  async function createSemester(data: Omit<Semester, '_id'>) {
    isCreatingSemester.value = true

    return apiClient.post<Semester>('/semesters', data).finally(() => {
      isCreatingSemester.value = false
    })
  }

  async function updateSemester(id: Semester['_id'], data: Omit<Semester, '_id'>) {
    isUpdatingSemester.value = true

    return apiClient.patch<Semester>(`/semesters/${id}`, data).finally(() => {
      isUpdatingSemester.value = false
    })
  }

  async function deleteSemester(id: Semester['_id']) {
    isDeletingSemester.value = true

    return apiClient.delete(`/semesters/${id}`).finally(() => {
      isDeletingSemester.value = false
    })
  }

  return {
    isLoadingSemester,
    isCreatingSemester,
    isUpdatingSemester,
    isDeletingSemester,
    semester,
    getSemester,
    createSemester,
    updateSemester,
    deleteSemester,
  }
})

export { provideSemester }

export function useSemesterOrThrow() {
  const semester = useSemester()
  if (!semester) {
    throw new Error('Please call `provideSemester` on the appropriate parent component')
  }
  return semester
}
