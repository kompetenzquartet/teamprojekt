import { ref } from 'vue'
import { VSnackbar } from 'vuetify/components'
import { createInjectionState } from '@vueuse/core'

type Snackbar = Pick<VSnackbar['$props'], 'text' | 'color'>

const [providePageSnackbar, usePageSnackbar] = createInjectionState(() => {
  const snackbar = ref<Snackbar>()
  const showSnackbar = ref(false)

  function resetSnackbar() {
    snackbar.value = undefined
  }

  function show() {
    showSnackbar.value = true
  }

  return {
    snackbar,
    resetSnackbar,
    showSnackbar,
    show,
  }
})

export { providePageSnackbar, usePageSnackbar }

export function usePageSnackbarOrThrow() {
  const pageSnackbar = usePageSnackbar()
  if (!pageSnackbar) {
    throw new Error('Please call `providePageSnackbar` on the appropriate parent component')
  }
  return pageSnackbar
}
