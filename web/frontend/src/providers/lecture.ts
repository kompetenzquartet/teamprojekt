import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { Lecture, LectureRequest, SubscribedLecture } from '@/model/types/lecture'
import { apiClient } from '@/utils/apiClient'
import type { Semester } from '@/model/types/semester'
import { useAuthOrThrow } from './auth'

const [provideLecture, useLecture] = createInjectionState(() => {
  const isLoadingLecture = ref(false)
  const isCreatingLecture = ref(false)
  const isUpdatingLecture = ref(false)
  const isDeletingLecture = ref(false)
  const isLoadingSubscribedLecture = ref(false)
  const lecture = ref<Lecture>()
  const subscribedLecture = ref<SubscribedLecture>()
  const { user } = useAuthOrThrow()

  async function getLecture(id: Lecture['_id']) {
    isLoadingLecture.value = true

    const response = await apiClient.get<Lecture>(`/lectures/${id}`).finally(() => {
      isLoadingLecture.value = false
    })
    lecture.value = response.data

    return response.data
  }

  async function createLecture(data: LectureRequest) {
    isCreatingLecture.value = true
    await apiClient.post<Lecture>('/lectures', data).finally(() => {
      isCreatingLecture.value = false
    })
  }

  async function updateLecture(id: Lecture['_id'], data: LectureRequest) {
    isUpdatingLecture.value = true
    await apiClient.patch<Lecture>(`/lectures/${id}`, data).finally(() => {
      isUpdatingLecture.value = false
    })
  }

  async function deleteLecture(id: Lecture['_id']) {
    isDeletingLecture.value = true
    await apiClient.delete(`/lectures/${id}`).finally(() => {
      isDeletingLecture.value = false
    })
  }

  async function getSubscribedLecture(semesterId: Semester['_id'], lectureId: SubscribedLecture['_id']) {
    isLoadingSubscribedLecture.value = true

    const response = await apiClient.get<SubscribedLecture>(`users/${user.value?._id}/semesters/${semesterId}/lectures/${lectureId}/courses`).finally(() => {
      isLoadingSubscribedLecture.value = false
    })
    subscribedLecture.value = response.data

    return response.data
  }

  return {
    isLoadingLecture,
    isCreatingLecture,
    isUpdatingLecture,
    isDeletingLecture,
    lecture,
    getLecture,
    createLecture,
    updateLecture,
    deleteLecture,
    isLoadingSubscribedLecture,
    subscribedLecture,
    getSubscribedLecture,
  }
})

export { provideLecture }

export function useLectureOrThrow() {
  const Lecture = useLecture()
  if (!Lecture) {
    throw new Error('Please call `provideLecture` on the appropriate parent component')
  }
  return Lecture
}
