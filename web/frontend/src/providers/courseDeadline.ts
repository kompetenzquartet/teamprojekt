import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import { apiClient } from '@/utils/apiClient'
import type { CourseDeadline } from '@/model/types/courseDeadline'

const [provideCourseDeadline, useCourseDeadline] = createInjectionState(() => {
  const isCreatingCourseDeadline = ref(false)
  const isUpdatingCourseDeadline = ref(false)
  const isDeletingCourseDeadline = ref(false)

  async function createCourseDeadline(data: Omit<CourseDeadline, '_id'>) {
    isCreatingCourseDeadline.value = true
    await apiClient.post<CourseDeadline>(`/course-deadline`, data).finally(() => {
      isCreatingCourseDeadline.value = false
    })
  }

  async function updateCourseDeadline(id: CourseDeadline['_id'], data: Omit<CourseDeadline, '_id'>) {
    isUpdatingCourseDeadline.value = true
    await apiClient.put<CourseDeadline>(`/course-deadline/${id}`, data).finally(() => {
      isUpdatingCourseDeadline.value = false
    })
  }

  async function deleteCourseDeadline(id: CourseDeadline['_id']) {
    isDeletingCourseDeadline.value = true
    await apiClient.delete<CourseDeadline>(`/course-deadline/${id}`).finally(() => {
      isDeletingCourseDeadline.value = false
    })
  }

  return {
    isCreatingCourseDeadline,
    isUpdatingCourseDeadline,
    isDeletingCourseDeadline,
    createCourseDeadline,
    updateCourseDeadline,
    deleteCourseDeadline,
  }
})

export { provideCourseDeadline }

export function useCourseDeadlineOrThrow() {
  const courseDeadline = useCourseDeadline()
  if (!courseDeadline) {
    throw new Error('Please call `provideCourseDeadline` on the appropriate parent component')
  }
  return courseDeadline
}
