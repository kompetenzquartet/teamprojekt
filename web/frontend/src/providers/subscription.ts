import { createInjectionState } from '@vueuse/core'
import { ref } from 'vue'
import type { Subscription } from '@/model/types/subscription'
import { apiClient } from '@/utils/apiClient'

const [provideSubscription, useSubscription] = createInjectionState(() => {
  const isCreatingSubscription = ref(false)
  const isDeletingSubscription = ref(false)

  async function createSubscription(subscription: Omit<Subscription, '_id'>) {
    isCreatingSubscription.value = true
    return apiClient.post<Subscription>('/subscriptions', subscription).finally(() => {
      isCreatingSubscription.value = false
    })
  }

  async function deleteSubscription(subscriptionId: Subscription['_id']) {
    isDeletingSubscription.value = true
    return apiClient.delete(`/subscriptions/${subscriptionId}`).finally(() => {
      isDeletingSubscription.value = false
    })
  }

  return {
    isCreatingSubscription,
    isDeletingSubscription,
    createSubscription,
    deleteSubscription,
  }
})

export { provideSubscription }

export function useSubscriptionOrThrow() {
  const subscription = useSubscription()
  if (!subscription) {
    throw new Error('Please call `provideSubscription` on the appropriate parent component')
  }
  return subscription
}
