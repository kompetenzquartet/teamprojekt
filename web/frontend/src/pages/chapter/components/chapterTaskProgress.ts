import type { Task } from '@/model/types/task'
import type { TaskProgressState } from '@/model/types/taskProgress'
import { useTaskProgressOrThrow } from '@/providers/taskProgress'
import { useTaskProgressesOrThrow } from '@/providers/taskProgresses'
import { createInjectionState, watchImmediate } from '@vueuse/core'
import { computed, type MaybeRefOrGetter, toValue } from 'vue'
import { useChapterTaskFormOrThrow } from './chapterTaskForm'

const [provideChapterTaskProgress, useChapterTaskProgress] = createInjectionState((taskId: MaybeRefOrGetter<Task['_id']>) => {
  const { userChapterTaskProgresses, isLoadingUserChapterTaskProgresses } = useTaskProgressesOrThrow()

  const taskProgress = computed(() => userChapterTaskProgresses.value.find(taskProgress => taskProgress.taskId === toValue(taskId)))
  const { task, meta, resetField } = useChapterTaskFormOrThrow()

  watchImmediate(taskProgress, value => {
    if (!value) {
      return
    }

    resetField('_id', { value: value._id })

    if (task.value?.tag === 'single-choice-task') {
      resetField('solutions', { value: { [value.taskId]: value.solutions[0].solution } })
      resetField('cheatedSolutions', { value: { [value.taskId]: value.cheatedSolutions[0] } })
    } else if (task.value?.tag === 'multiple-choice-task') {
      resetField('solutions', {
        value: task.value?.data.solutions.reduce((prev, cur, index) => (
          { ...prev, [`${value.taskId}-${task.value?.data.solutions.find(sol => sol.solution === cur.solution)?._id}`]: value.solutions[index].solution }
        ), {}),
      })

      resetField('cheatedSolutions', {
        value: task.value?.data.solutions.reduce((prev, cur, index) => (
          { ...prev, [`${value.taskId}-${task.value?.data.solutions.find(sol => sol.solution === cur.solution)?._id}`]: value.cheatedSolutions[index] }
        ), {}),
      })
    } else {
      resetField('solutions', {
        value: task.value?.data.solutions.reduce((prev, cur, index) => (
          { ...prev, [`${value.taskId}-${index}`]: value.solutions[index].solution }
        ), {}),
      })

      resetField('cheatedSolutions', {
        value: task.value?.data.solutions.reduce((prev, _, index) => (
          { ...prev, [`${value.taskId}-${index}`]: value.cheatedSolutions[index] }
        ), {}),
      })
    }
  })

  const { createTaskProgress, updateTaskProgress } = useTaskProgressOrThrow()

  async function upsert(values: TaskProgressState) {
    const isReflectionOrVideoTask = ['reflection-task', 'video-task'].includes(task.value?.tag ?? '')

    if ((!meta.value.dirty && !isReflectionOrVideoTask) || (isReflectionOrVideoTask && Boolean(taskProgress.value))) {
      return
    }

    const data = {
      ...values,
      solutions: isReflectionOrVideoTask ? [] : Object.values(values.solutions).map(solution => solution ?? '')
        .filter(solution => task.value?.tag === 'multiple-choice-task' ? solution !== '' : true),
      cheatedSolutions: Object.values(values.cheatedSolutions ?? []),
    }

    const { _id, ...createData } = data
    await (taskProgress.value ? updateTaskProgress(_id, data) : createTaskProgress(createData))
  }

  return {
    taskProgress,
    upsert,
    userChapterTaskProgresses,
    isLoadingUserChapterTaskProgresses,
  }
})

export { provideChapterTaskProgress }

export function useChapterTaskProgressOrThrow() {
  const chapterTaskProgress = useChapterTaskProgress()
  if (!chapterTaskProgress) {
    throw new Error('Please call `provideChapterTaskProgress` on the appropriate parent component')
  }
  return chapterTaskProgress
}
