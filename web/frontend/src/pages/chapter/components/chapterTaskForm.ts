import type { Chapter } from '@/model/types/chapter'
import type { Course } from '@/model/types/course'
import type { Lecture } from '@/model/types/lecture'
import type { Semester } from '@/model/types/semester'
import type { Task } from '@/model/types/task'
import type { TaskProgressState } from '@/model/types/taskProgress'
import { useAuthOrThrow } from '@/providers/auth'
import { useCourseChapterOrThrow } from '@/providers/chapter'
import { createInjectionState, watchImmediate } from '@vueuse/core'
import { useForm } from 'vee-validate'
import { computed, watch } from 'vue'

const [provideChapterTaskForm, useChapterTaskForm] = createInjectionState((props: {
  chapterId: Chapter['_id'];
  courseId: Course['_id'];
  lectureId: Lecture['_id'];
  semesterId: Semester['_id'];
  taskId: Task['_id'];
}) => {
  const { courseChapter } = useCourseChapterOrThrow()
  const task = computed(() => courseChapter.value?.chapter.tasks.find(task => task._id === props.taskId))
  const { user } = useAuthOrThrow()

  const form = useForm<TaskProgressState>({
    initialValues: {
      userId: user.value!._id,
      semesterId: props.semesterId,
      lectureId: props.lectureId,
      courseId: props.courseId,
      chapterId: props.chapterId,
      taskId: props.taskId,
      solutions: {},
      cheatedSolutions: {},
    },
  })

  watch(props, value => {
    form.resetField('taskId', { value: value.taskId })
  })

  watchImmediate(task, value => {
    if (!value) {
      return
    }

    if (task.value?.tag === 'single-choice-task') {
      form.resetField('solutions', { value: { [props.taskId]: '' } })
      form.resetField('cheatedSolutions', { value: { [props.taskId]: false } })
    } else if (task.value?.tag === 'multiple-choice-task') {
      form.resetField('solutions', {
        value: task.value?.data.solutions.reduce((prev, cur) => (
          { ...prev, [`${props.taskId}-${cur._id}`]: '' }
        ), {}),
      })

      form.resetField('cheatedSolutions', {
        value: task.value?.data.solutions.reduce((prev, cur) => (
          { ...prev, [`${props.taskId}-${cur._id}`]: false }
        ), {}),
      })
    } else {
      form.resetField('solutions', {
        value: task.value?.data.solutions.reduce((prev, _, index) => (
          { ...prev, [`${props.taskId}-${index}`]: '' }
        ), {}),
      })

      form.resetField('cheatedSolutions', {
        value: task.value?.data.solutions.reduce((prev, _, index) => (
          { ...prev, [`${props.taskId}-${index}`]: false }
        ), {}),
      })
    }
  })

  const formId = Symbol('chapterForm').toString()

  return {
    ...form,
    formId,
    task,
  }
})

export { provideChapterTaskForm }

export function useChapterTaskFormOrThrow() {
  const chapterTaskForm = useChapterTaskForm()
  if (!chapterTaskForm) {
    throw new Error('Please call `provideChapterTaskForm` on the appropriate parent component')
  }
  return chapterTaskForm
}
