import {
  Decoration, DecorationSet, EditorView, MatchDecorator,
  ViewPlugin, ViewUpdate, WidgetType,
} from '@codemirror/view'

class KoanInputWidget extends WidgetType {
  constructor(readonly element: HTMLElement) { super() }

  eq(other: KoanInputWidget) { return other.element.dataset.id === this.element.dataset.id }

  toDOM() {
    return this.element
  }

  ignoreEvent() { return false }
}

const KoanInputMatcher = (koanInputs: HTMLElement[]) => new MatchDecorator({
  regexp: /__/g,
  decoration: () => {
    return Decoration.replace({
      widget: new KoanInputWidget(koanInputs.shift()!),
    })
  },
})

export const KoanInputPlugin = ViewPlugin.fromClass(class {
  koanInputs: DecorationSet
  koanInputMatcher: MatchDecorator

  constructor(view: EditorView) {
    this.koanInputMatcher = KoanInputMatcher(Array.from(document.querySelectorAll<HTMLElement>('.koan-task-input')))
    this.koanInputs = this.koanInputMatcher.createDeco(view)
  }

  update(update: ViewUpdate) {
    this.koanInputMatcher = KoanInputMatcher(Array.from(document.querySelectorAll<HTMLElement>('.koan-task-input')))
    this.koanInputs = this.koanInputMatcher.updateDeco(update, this.koanInputs)
  }
}, {
  decorations: instance => instance.koanInputs,
  provide: plugin => EditorView.atomicRanges.of(view => {
    return view.plugin(plugin)?.koanInputs || Decoration.none
  }),
})
