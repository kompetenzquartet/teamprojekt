import {
  Decoration, DecorationSet, EditorView, MatchDecorator,
  ViewPlugin, ViewUpdate, WidgetType,
} from '@codemirror/view'

class CodeInputWidget extends WidgetType {
  constructor(readonly element: HTMLElement) { super() }

  eq(other: CodeInputWidget) { return other.element.dataset.id === this.element.dataset.id }

  toDOM() {
    return this.element
  }

  ignoreEvent() { return false }
}

const CodeInputMatcher = (codeInputs: HTMLElement[]) => new MatchDecorator({
  regexp: /_#_/g,
  decoration: () => {
    return Decoration.replace({
      widget: new CodeInputWidget(codeInputs.shift()!),
    })
  },
})

export const CodeInputPlugin = ViewPlugin.fromClass(class {
  codeInputs: DecorationSet
  codeInputMatcher: MatchDecorator

  constructor(view: EditorView) {
    this.codeInputMatcher = CodeInputMatcher(Array.from(document.querySelectorAll<HTMLElement>('.code-task-input')))
    this.codeInputs = this.codeInputMatcher.createDeco(view)
  }

  update(update: ViewUpdate) {
    this.codeInputMatcher = CodeInputMatcher(Array.from(document.querySelectorAll<HTMLElement>('.code-task-input')))
    this.codeInputs = this.codeInputMatcher.updateDeco(update, this.codeInputs)
  }
}, {
  decorations: instance => instance.codeInputs,
  provide: plugin => EditorView.atomicRanges.of(view => {
    return view.plugin(plugin)?.codeInputs || Decoration.none
  }),
})
