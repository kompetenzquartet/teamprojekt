import axios from 'axios'
import { useStorage } from '@vueuse/core'

export const store = useStorage('store', { accessToken: '' })

export const apiClient = axios.create({
  baseURL: `${location.protocol}//${!import.meta.env.PROD ? '' : 'api.'}${
    location.hostname
  }`,
})

apiClient.interceptors.request.use(
  config => {
    if (store.value.accessToken) {
      config.headers.Authorization = `Bearer ${store.value.accessToken}`
    }
    return config
  },
  error => {
    Promise.reject(error)
  }
)
