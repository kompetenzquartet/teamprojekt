export function highlightText(text: string, search: string) {
  if (!search) {
    return text
  }
  return text.replace(new RegExp(search, 'gi'), match => {
    return '<span class="bg-secondary">' + match + '</span>'
  })
}
