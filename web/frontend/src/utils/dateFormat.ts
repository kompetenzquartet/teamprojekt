import { format } from 'date-fns'

export function getFormattedDate(date: Date) {
  return format(date, 'dd.MM.yyyy')
}
