import com.google.inject.AbstractModule
import integration.checkapi.{CheckApi, CheckApiESI}
import play.engineio.EngineIOController
import provider.MySocketIOEngineProvider
import service.exam.converter.{TaskToExamTaskConverter, TaskToExamTaskConverterImpl}
import service.exam.grade.{ExamMarkCalculator, MarkCalculator}
import service.random.{RandomCodeGenerator, RandomCodeGeneratorImpl}

class Module extends AbstractModule {
  override def configure() = {
    bind(classOf[EngineIOController]).toProvider(classOf[MySocketIOEngineProvider])

    bind(classOf[TaskToExamTaskConverter]).toInstance(new TaskToExamTaskConverterImpl())
    bind(classOf[RandomCodeGenerator]).toInstance(new RandomCodeGeneratorImpl())
    bind(classOf[MarkCalculator]).toInstance(new ExamMarkCalculator())
    bind(classOf[CheckApi]).to(classOf[CheckApiESI])
  }
}
