package util

import play.api.mvc.AnyContent
import play.api.libs.json.Reads
import play.api.libs.json.JsSuccess
import play.api.libs.json.JsValue
import scala.util.Success
import play.api.libs.json.JsError
import scala.util.Failure
import scala.util.Try

object JsonValidator {
  def getModelFromRequestBody[T: Reads](body: AnyContent): Try[T] = {
    if (body.asJson.nonEmpty) {
      val json: JsValue = body.asJson.get

      json.validate[T] match {
        case JsSuccess(user, _) =>
          Success(user)
        case e: JsError =>
          Failure(
            new Error(s"JSON doesn't match user object: ${JsError.toJson(e)}")
          )
      }
    } else {
      Failure(new Error("Body doesn't contain JSON."))
    }
  }
}
