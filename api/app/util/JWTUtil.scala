package util

import java.time.Clock

import com.typesafe.config.ConfigFactory
import pdi.jwt.{JwtAlgorithm, JwtClaim, JwtJson}

import scala.util.{Failure, Success, Try}
import play.api.libs.json.{JsObject, Json}
import play.api.libs.json.OWrites
import play.api.libs.json.Writes

object JWTUtil {
  val config = ConfigFactory.load

  val jwtAlgo = JwtAlgorithm.HS256
  val jwtKey: String = config.getString("auth.jwtSecretKey")
  val secondsInADay = 86400
  val accessExpirationTime: Int =
    secondsInADay * config.getInt("auth.jwtAccessTimeoutInDays")
  val refreshExpirationTime: Int =
    secondsInADay * config.getInt("auth.jwtRefreshTimeoutInDays")
  implicit val clock: Clock = Clock.systemUTC

  def createAccessToken(userId: String, userEmail: String) = {
    val claim = JwtClaim(
      Json.obj(("userId", userId), ("userEmail", userEmail)).toString
    ).issuedNow.expiresIn(accessExpirationTime)

    JwtJson.encode(claim, jwtKey, jwtAlgo)
  }

  def attachAccessToken[T: OWrites](obj: T, userId: String, userEmail: String) =
    Json.toJsObject(obj) + ("accessToken" -> Json.toJson(
      JWTUtil.createAccessToken(
        userId,
        userEmail
      )
    ))

  def createRefreshToken() = {
    val claim = JwtClaim(
      Json.toJson(java.util.UUID.randomUUID.toString).toString
    ).issuedNow
      .expiresIn(refreshExpirationTime)

    JwtJson.encode(claim, jwtKey, jwtAlgo)
  }

  def isValidToken(token: String): Boolean = Try(
    JwtJson.validate(token, jwtKey, Seq(jwtAlgo))
  ).map(_ => true).getOrElse(false)

  def decodePayload(token: String): Option[(String, String)] = {
    JwtJson.decodeJson(token, jwtKey, Seq(JwtAlgorithm.HS256)) match {
      case Success(aaa) =>
        Some(
          (aaa \ "userId").get.toString().replaceAll("\"", ""),
          (aaa \ "userEmail").get.toString().replaceAll("\"", "")
        )
      case Failure(_) => None
    }
  }
}
