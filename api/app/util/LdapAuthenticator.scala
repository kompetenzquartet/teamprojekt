package util

import java.util.Properties

import javax.naming.Context
import javax.naming.directory.InitialDirContext

import scala.util.Try

object LdapAuthenticator {
  def authenticateAtHtwg(username: String, password: String): Try[InitialDirContext] = {
    val props = new Properties
    props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory")
    props.put(Context.PROVIDER_URL, "ldaps://ldap.htwg-konstanz.de")
    props.put(Context.SECURITY_PRINCIPAL, "uid=" + username + ",ou=users,dc=fh-konstanz,dc=de")
    props.put(Context.SECURITY_CREDENTIALS, password)

    Try(new InitialDirContext(props))
  }
}
