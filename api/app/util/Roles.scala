package util

import dao.mongoDB.UserDBController
import model.user.User
import play.api.mvc.{Request, RequestHeader}

import scala.util.{Failure, Success, Try}
import org.bson.types.ObjectId
import play.api.http.HeaderNames

object Roles {
  val AdminRole = "admin"
  val StudentRole = "student"
  val UserRole = "casual"

  def getUserFromTokenParams(
      request: RequestHeader,
      database: UserDBController
  ): Try[User] = request
    .getQueryString("access_token")
    .toRight(Failure(new Error("Token is invalid.")))
    .map(token =>
      JWTUtil
        .decodePayload(token)
        .map(value => Success(value._1))
        .getOrElse(Failure(new Error("Token is invalid.")))
    )
    .map(userId =>
      userId match {
        case Failure(exception) => Failure(exception)
        case Success(value) =>
          database
            .get(new ObjectId(value))
            .map(user => Success(user))
            .getOrElse(Failure(new Error("Token is invalid.")))
      }
    )
    .getOrElse(Failure(new Error("Token is invalid.")))

  def getUserFromToken(
      request: RequestHeader,
      database: UserDBController
  ): Try[User] = request.headers
    .get(HeaderNames.AUTHORIZATION)
    .toRight(Failure(new Error("Token is invalid.")))
    .map(decodeAuthorizationHeader)
    .map(userId =>
      userId match {
        case Failure(exception) => Failure(exception)
        case Success(value) =>
          database
            .get(new ObjectId(value))
            .map(user => Success(user))
            .getOrElse(Failure(new Error("Token is invalid.")))
      }
    )
    .getOrElse(Failure(new Error("Token is invalid.")))

  private def decodeAuthorizationHeader(header: String): Try[String] = {
    val tokenType = "Bearer"
    val headerParts = header.split(" ")

    Option(headerParts)
      .filter(_.length == 2)
      .filter(tokenType equalsIgnoreCase _.head)
      .map(_.drop(1).head)
      .map(token =>
        JWTUtil
          .decodePayload(token)
          .map(value => Success(value._1))
          .getOrElse(Failure(new Error("Token is invalid.")))
      )
      .getOrElse(Failure(new Error("Token is invalid.")))
  }
}
