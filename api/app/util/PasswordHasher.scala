package util

import org.mindrot.jbcrypt.BCrypt

object PasswordHasher {
  def getHash(password: String): String = {
    BCrypt.hashpw(password, BCrypt.gensalt())
  }

  def checkHash(password: String, passwordHashed: String): Boolean = {
    BCrypt.checkpw(password, passwordHashed)
  }
}
