package dao.mongoDB

import dao.DAOInterface
import dao.DatabaseCollections.{examSolutionCollection, userCollection}
import model.exam.{Exam, ExamSolution, UserExamSolution}
import org.bson.types.ObjectId
import org.mongodb.scala.model.Filters.{equal, in}
import org.mongodb.scala.model.Updates
import service.exam.grade.MarkCalculator

import javax.inject.Inject
import scala.concurrent.Await
import scala.util.{Failure, Success, Try}

class UserExamSolutionDBController @Inject()(
                                              markCalculator: MarkCalculator
                                            ) extends DAOInterface[ObjectId, ExamSolution] {

  override def list(): Option[List[ExamSolution]] = {
    Try(Await.result(examSolutionCollection.find().toFuture(), duration).toList).toOption
  }

  override def get(id: ObjectId): Option[ExamSolution] = {
    Try(Await.result(examSolutionCollection.find(equal("_id", id)).first().toFuture(), duration)).toOption
  }

  override def create(examSolution: ExamSolution): Option[ExamSolution] = Try(
    Await.result(examSolutionCollection.insertOne(examSolution).toFuture(), duration)
  ) match {
    case Success(_) => Some(examSolution)
    case Failure(e) => logThrowableAndReturnNone[ExamSolution](e)
  }

  override def update(id: ObjectId, examSolution: ExamSolution): Option[ExamSolution] = Try(
    Await.result(
      examSolutionCollection.updateOne(equal("_id", id.toString), Updates.combine(
        Updates.set("solvedWith", examSolution.solvedWith),
        Updates.set("points", examSolution.points)
      )).toFuture(), duration
    )
  ) match {
    case Success(_) => Some(examSolution)
    case Failure(e) => logThrowableAndReturnNone[ExamSolution](e)
  }

  def updateMany(examSolutions: List[ExamSolution]): List[Option[ExamSolution]] = {
    examSolutions.map(examSolution => update(new ObjectId(examSolution._id), examSolution))
  }

    override def delete(id: ObjectId): Option[Long] = {
      Try(Await.result(
        examSolutionCollection.deleteOne(equal("_id", id)).toFuture(), duration
      )) match {
        case Success(deleteResult) => Some(deleteResult.getDeletedCount)
        case Failure(_) => None
      }
    }

    def getUserExamSolutionsByExamId(examId: String): Option[List[UserExamSolution]] = {
      val examDBController = new ExamDBController()
      val exam: Option[Exam] = examDBController.get(new ObjectId(examId))

      val userExamSolutions = for {
        examSolutions <- Try {
          val result = Await.result(examSolutionCollection.find(equal("examId", examId)).toFuture(), duration)
          result
        }.toOption
        userIds = examSolutions.map(solution => new ObjectId(solution.userId)).distinct
        users <- Try {
          val result = Await.result(userCollection.find(in("_id", userIds: _*)).toFuture(), duration)
          result
        }.toOption
      } yield {
        users.map { user =>
          val userExamSolutions = examSolutions.filter(_.userId == user._id.toString).toList
          UserExamSolution(
            userId = user._id.toString,
            userName = user.username,
            studentNumber = user.studentNumber,
            examSolutions = userExamSolutions,
            mark = exam.map(exam => markCalculator.calculateMark(exam, userExamSolutions))
              .getOrElse("-1.0")
          )
        }
      }
      userExamSolutions.map(_.toList).filter(_.nonEmpty).orElse(Some(List.empty))
    }
  }
