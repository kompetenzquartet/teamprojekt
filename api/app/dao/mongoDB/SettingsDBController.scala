package dao.mongoDB

import model.BaseModel
import org.mongodb.scala.model.Filters._

import scala.concurrent.Await
import scala.util.{Failure, Success, Try}
import org.bson.types.ObjectId
import dao.DAOInterface
import dao.DatabaseCollections.settingsCollection
import model.settings.Settings

class SettingsDBController extends DAOInterface[ObjectId, Settings] {
  override def list(): Option[List[Settings]] = {
    Try(Await.result(settingsCollection.find().toFuture(), duration)) match {
      case Success(globalSettings) =>
        Some(globalSettings.map(globalSetting => globalSetting).toList)
      case Failure(e) => logThrowableAndReturnNone[List[Settings]](e)
    }
  }

  override def get(id: ObjectId): Option[Settings] = {
    Try(
      Await.result(
        settingsCollection.find(equal("_id", id)).first().toFuture(),
        duration
      )
    ) match {
      case Success(globalSetting) => Some(globalSetting)
      case Failure(e)             => logThrowableAndReturnNone[Settings](e)
    }
  }

  override def create(settings: Settings): Option[Settings] = Try(
    Await.result(
      settingsCollection.insertOne(settings).toFuture(),
      duration
    )
  ) match {
    case Success(_) => Some(settings)
    case Failure(e) => logThrowableAndReturnNone[Settings](e)
  }

  override def update(
      id: ObjectId,
      settings: Settings
  ): Option[Settings] = Try(
    Await.result(
      settingsCollection
        .replaceOne(equal("_id", id), settings)
        .toFuture(),
      duration
    )
  ) match {
    case Success(_) => Some(settings)
    case Failure(e) => logThrowableAndReturnNone[Settings](e)
  }

  override def delete(id: ObjectId): Option[Long] = {
    Try(
      Await.result(
        settingsCollection.deleteOne(equal("_id", id)).toFuture(),
        duration
      )
    ) match {
      case Success(deleteResult) => Some(deleteResult.getDeletedCount)
      case Failure(_)            => None
    }
  }

  def koanTaskTime = list() match {
    case Some(globalSettings) =>
      globalSettings.headOption
        .orElse(Option(Settings(_id = null)))
        .get
        .koanTime
    case None => 15
  }

  def codeTaskTime = list() match {
    case Some(globalSettings) =>
      globalSettings.headOption
        .orElse(Option(Settings(_id = null)))
        .get
        .codetaskTime
    case None => 30
  }
}
