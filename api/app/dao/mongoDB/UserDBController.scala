package dao.mongoDB

import dao.DAOInterface
import model.subscription.Subscription
import model.user.User
import org.mongodb.scala.model.Filters.equal

import scala.concurrent.Await
import scala.util.{Failure, Success, Try}
import org.bson.types.ObjectId
import com.mongodb.client.model.Updates
import model.user.UserSettings
import model.user.UserProfile
import model.user.UserSettingsRequest
import util.JWTUtil
import dao.DatabaseCollections.userCollection
import dao.DatabaseCollections.subscriptionCollection
import model.user.UserProfileRequest

class UserDBController extends DAOInterface[ObjectId, User] {
  override def list(): Option[List[User]] = {
    Try(Await.result(userCollection.find().toFuture(), duration)) match {
      case Success(users) =>
        Some(users.map(user => user).toList)
      case Failure(e) => logThrowableAndReturnNone[List[User]](e)
    }
  }

  override def get(id: ObjectId): Option[User] = {
    Try(
      Await.result(
        userCollection.find(equal("_id", id)).first().toFutureOption(),
        duration
      )
    ) match {
      case Success(userOption) => userOption
      case Failure(e)          => logThrowableAndReturnNone[User](e)
    }
  }

  override def create(user: User): Option[User] = Try(
    Await.result(userCollection.insertOne(user).toFuture(), duration)
  ) match {
    case Success(_) => Some(user)
    case Failure(e) => logThrowableAndReturnNone[User](e)
  }

  def updateSettings(
      id: ObjectId,
      settingsRequest: UserSettings
  ): Option[User] = {
    Try(
      Await.result(
        userCollection
          .updateOne(
            equal("_id", id),
            Updates.set("settings", settingsRequest)
          )
          .toFuture(),
        duration
      )
    ) match {
      case Success(_) => get(id)
      case Failure(e) => logThrowableAndReturnNone[User](e)
    }
  }

  override def update(id: ObjectId, userRequest: User): Option[User] = ???

  def updateProfile(id: ObjectId, profile: UserProfileRequest): Option[User] =
    Try(
      Await.result(
        userCollection
          .updateOne(
            equal("_id", id),
            Updates.set("username", profile.username)
          )
          .toFuture(),
        duration
      )
    ) match {
      case Success(_) => get(id)
      case Failure(e) => logThrowableAndReturnNone[User](e)
    }

  def updateRefreshToken(id: ObjectId): Option[User] = {
    Try(
      Await.result(
        userCollection
          .updateOne(
            equal("_id", id),
            Updates.set("refreshToken", JWTUtil.createRefreshToken)
          )
          .toFuture(),
        duration
      )
    ) match {
      case Success(_) => get(id)
      case Failure(e) => logThrowableAndReturnNone[User](e)
    }
  }

  def getByEMail(email: String): Option[User] = {
    Try(
      Await.result(
        userCollection.find(equal("email", email)).first().toFutureOption(),
        duration
      )
    ) match {
      case Success(userOption) => userOption
      case Failure(e)          => logThrowableAndReturnNone[User](e)
    }
  }

  override def delete(id: ObjectId): Option[Long] = ???
}
