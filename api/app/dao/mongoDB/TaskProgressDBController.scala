package dao.mongoDB

import dao.DAOInterface
import org.bson.types.ObjectId
import model.taskProgress.TaskProgressRequest
import scala.util.Try
import scala.concurrent.Await
import scala.util.Success
import scala.util.Failure
import org.mongodb.scala.model.Filters.{equal, and}
import dao.DatabaseCollections.taskProgressCollection
import dao.DatabaseCollections.courseCollection
import _root_.model.course.subs.Solution
import _root_.model.taskProgress.TaskProgressSolution
import _root_.model.taskProgress.TaskProgress
import dao.DatabaseClient.user
import model.course.subs.Task
import _root_.dao.DatabaseCollections.lectureCollection

class TaskProgressDBController
    extends DAOInterface[ObjectId, TaskProgressRequest] {

  override def list(): Option[List[TaskProgress]] = {
    Try(Await.result(taskProgressCollection.find().toFuture(), duration)) match {
      case Success(userTasks) => Some(userTasks.toList.map(getTaskProgress))
      case Failure(e)         => logThrowableAndReturnNone[List[TaskProgress]](e)
    }
  }

  def getTask(taskProgress: TaskProgressRequest) = Try(
    Await
      .result(
        courseCollection
          .find(equal("_id", taskProgress.courseId))
          .first()
          .toFuture(),
        duration
      )
  ) match {
    case Failure(e) => logThrowableAndReturnNone[Task](e)
    case Success(course) =>
      course.chapters
        .find(chapter => chapter._id == taskProgress.chapterId)
        .map(chapter =>
          chapter.tasks
            .find(task => task._id == taskProgress.taskId)
        )
        .getOrElse(None)
  }

  def getTaskProgress(request: TaskProgressRequest) = {
    val task = getTask(request)
    val taskSolutions = task.map(task => task.data.solutions)

    if (task.get.tag == "multiple-choice-task") {
      request.toTaskProgress(
        task.get.tag,
        task.get.data.code,
        request.solutions.map { userSolution =>
          TaskProgressSolution(
            taskSolutions.exists(_.exists(solution => solution.solution == userSolution && solution.correctness)),
            userSolution
          )
        }
      )
    } else if (task.get.tag == "single-choice-task") {
      request.toTaskProgress(
        task.get.tag,
        task.get.data.code,
        request.solutions.headOption.map { userSolution =>
          TaskProgressSolution(
            taskSolutions.exists(_.exists(solution => solution.solution == userSolution && solution.correctness)),
            userSolution
          )
        }.toList
      )
    }else if (task.get.tag == "koan-task") {
      val tag = task.get.tag
      val code = task.get.data.code
      val solutions = request.solutions

      val taskSolutionsList = taskSolutions.getOrElse(List.empty[Solution])
      val isTaskSolutionsEmpty = taskSolutionsList.isEmpty

      val taskProgressSolutions = solutions.zip(taskSolutionsList).map { case (taskProgressSolution, solution) =>
        val trimmedTaskProgressSolution = taskProgressSolution.trim
        val trimmedSolution = solution.solution.trim
        val isCorrect = isTaskSolutionsEmpty || trimmedTaskProgressSolution.equalsIgnoreCase(trimmedSolution)
        TaskProgressSolution(isCorrect, taskProgressSolution)
      }

      request.toTaskProgress(tag, code, taskProgressSolutions)
    }

    else {
      request.toTaskProgress(
        task.get.tag,
        task.get.data.code,
        request.solutions
          .map(taskProgressSolution =>
            TaskProgressSolution(
              taskSolutions.get.isEmpty || taskSolutions.get
                .find(solution =>
                  taskProgressSolution == solution.solution
                )
                .nonEmpty,
              taskProgressSolution
            )
          )
      )
    }
  }

  def listUserTaskProgresses(userId: ObjectId) = Try(
    Await
      .result(
        taskProgressCollection.find(equal("userId", userId)).toFuture(),
        duration
      )
  ) match {
    case Success(userTasks) => Some(userTasks.toList.map(getTaskProgress))
    case Failure(e)         => logThrowableAndReturnNone[List[TaskProgress]](e)
  }

  def listUserChapterTaskProgresses(
      userId: ObjectId,
      semesterId: ObjectId,
      lectureId: ObjectId,
      courseId: String,
      chapterId: String
  ) = Try(
    Await.result(
      taskProgressCollection
        .find(
          and(
            equal("userId", userId),
            equal("lectureId", lectureId),
            equal("semesterId", semesterId),
            equal("courseId", courseId),
            equal("chapterId", chapterId)
          )
        )
        .toFuture(),
      duration
    )
  ) match {
    case Success(userTasks) => Some(userTasks.toList.map(getTaskProgress))
    case Failure(e)         => logThrowableAndReturnNone[List[TaskProgress]](e)
  }

  override def get(id: ObjectId): Option[TaskProgress] = Try(
    Await.result(
      taskProgressCollection.find(equal("_id", id)).first().toFuture(),
      duration
    )
  ) match {
    case Success(userTask) => Some(getTaskProgress(userTask))
    case Failure(e)        => logThrowableAndReturnNone[TaskProgress](e)
  }

  override def create(modelDAO: TaskProgressRequest): Option[TaskProgress] =
    Try(
      Await
        .result(taskProgressCollection.insertOne(modelDAO).toFuture(), duration)
    ) match {
      case Success(_) => get(modelDAO._id)
      case Failure(e) => logThrowableAndReturnNone[TaskProgress](e)
    }

  override def update(
      id: ObjectId,
      modelDAO: TaskProgressRequest
  ): Option[TaskProgress] = Try(
    Await.result(
      taskProgressCollection
        .replaceOne(
          equal("_id", id),
          modelDAO
        )
        .toFuture(),
      duration
    )
  ) match {
    case Success(_) => Some(getTaskProgress(modelDAO))
    case Failure(e) => logThrowableAndReturnNone[TaskProgress](e)
  }

  override def delete(id: ObjectId): Option[Long] = ???
}
