package dao.mongoDB

import dao.DAOInterface
import model.course.Course
import org.mongodb.scala.model.Filters.equal
import org.mongodb.scala.model.ReplaceOptions

import scala.concurrent.Await
import scala.util.{Failure, Success, Try}
import org.bson.types.ObjectId
import model.course.CourseRequest
import org.mongodb.scala.model.Updates
import dao.DatabaseCollections.courseCollection
import model.course.subs.TaskIdentifier

class CourseDBController extends DAOInterface[String, Course] {
  override def list(): Option[List[Course]] = {
    Try(Await.result(courseCollection.find().toFuture(), duration)) match {
      case Success(courses) => Option(courses.toList)
      case Failure(e)       => logThrowableAndReturnNone[List[Course]](e)
    }
  }

  override def create(course: Course): Option[Course] = get(course._id) match {
    case Some(oldCourse) =>
      Try(
        Await.result(
          courseCollection
            .replaceOne(
              equal("_id", course._id),
              course.copy(lectures = oldCourse.lectures),
              new ReplaceOptions().upsert(true)
            )
            .toFuture(),
          duration
        )
      ) match {
        case Success(_) => Some(course)
        case Failure(e) => logThrowableAndReturnNone[Course](e)
      }
    case None =>
      Try(
        Await.result(
          courseCollection
            .replaceOne(
              equal("_id", course._id),
              course,
              new ReplaceOptions().upsert(true)
            )
            .toFuture(),
          duration
        )
      ) match {
        case Success(_) => Some(course)
        case Failure(e) => logThrowableAndReturnNone[Course](e)
      }
  }

  override def get(id: String): Option[Course] = {
    Try(
      Await.result(
        courseCollection.find(equal("_id", id)).first().toFuture(),
        duration
      )
    ) match {
      case Success(course) => Option(course)
      case Failure(e)      => logThrowableAndReturnNone[Course](e)
    }
  }

  override def update(id: String, course: Course): Option[Course] = ???

  def updateLectures(id: String, course: CourseRequest): Option[Course] = Try(
    Await.result(
      courseCollection
        .updateOne(
          equal("_id", id),
          Updates.set("lectures", course.lectures)
        )
        .toFuture(),
      duration
    )
  ) match {
    case Success(_) => get(id)
    case Failure(e) => logThrowableAndReturnNone[Course](e)
  }

  override def delete(id: String): Option[Long] = ???

  def getTaskFromTaskIdentifier(taskIdentifier: TaskIdentifier) =
    get(taskIdentifier.courseId)
      .map(course =>
        course.chapters
          .filter(chapter => chapter._id == taskIdentifier.chapterId)
          .head
      )
      .map(chapter =>
        chapter.tasks.filter(task => task._id == taskIdentifier.taskId).head
      )
}
