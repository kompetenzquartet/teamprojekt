package dao.mongoDB

import model.BaseModel
import model.quiz.QuizTaskSet
import org.mongodb.scala.model.Filters.equal

import scala.concurrent.Await
import scala.util.{Failure, Success, Try}
import dao.DAOInterface
import dao.DatabaseCollections.quizTaskSetCollection
import org.bson.types.ObjectId

class QuizTaskSetDBController extends DAOInterface[ObjectId, QuizTaskSet] {

  override def list(): Option[List[QuizTaskSet]] = {
    Try(Await.result(quizTaskSetCollection.find().toFuture(), duration)) match {
      case Success(taskSets) =>
        Some(taskSets.map(taskSet => taskSet).toList)
      case Failure(e) => logThrowableAndReturnNone[List[QuizTaskSet]](e)
    }
  }

  override def get(id: ObjectId): Option[QuizTaskSet] = {
    Try(
      Await.result(
        quizTaskSetCollection.find(equal("_id", id)).first().toFuture(),
        duration
      )
    ) match {
      case Success(taskSet) => Some(taskSet)
      case Failure(e)       => logThrowableAndReturnNone[QuizTaskSet](e)
    }
  }

  override def create(taskSet: QuizTaskSet): Option[QuizTaskSet] = Try(
    Await.result(
      quizTaskSetCollection.insertOne(taskSet).toFuture(),
      duration
    )
  ) match {
    case Success(_) => Some(taskSet)
    case Failure(e) => logThrowableAndReturnNone[QuizTaskSet](e)
  }

  override def update(id: ObjectId, taskSet: QuizTaskSet): Option[QuizTaskSet] =
    Try(
      Await.result(
        quizTaskSetCollection
          .replaceOne(equal("_id", id), taskSet)
          .toFuture(),
        duration
      )
    ) match {
      case Success(_) => Some(taskSet)
      case Failure(e) => logThrowableAndReturnNone[QuizTaskSet](e)
    }

  override def delete(id: ObjectId): Option[Long] = {
    Try(
      Await.result(
        quizTaskSetCollection.deleteOne(equal("_id", id)).toFuture(),
        duration
      )
    ) match {
      case Success(deleteResult) => Some(deleteResult.getDeletedCount)
      case Failure(_)            => None
    }
  }
}
