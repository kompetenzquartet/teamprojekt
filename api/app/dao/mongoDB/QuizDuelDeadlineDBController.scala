package dao.mongoDB

import dao.DAOInterface
import dao.DatabaseCollections.quizDuelDeadlineCollection
import model.quiz.QuizDuelDeadline
import org.bson.types.ObjectId
import org.mongodb.scala.model.Filters.equal

import scala.concurrent.Await
import scala.util.{Failure, Success, Try}

class QuizDuelDeadlineDBController extends DAOInterface[ObjectId, QuizDuelDeadline] {

  override def list(): Option[List[QuizDuelDeadline]] = {
    Try(Await.result(quizDuelDeadlineCollection.find().toFuture(), duration)) match {
      case Success(quizDuelDeadlines) => Some(quizDuelDeadlines.map(quizDuelDeadline => quizDuelDeadline).toList)
      case Failure(e) => logThrowableAndReturnNone[List[QuizDuelDeadline]](e)
    }
  }

  override def get(id: ObjectId): Option[QuizDuelDeadline] = {
    Try(Await.result(quizDuelDeadlineCollection.find(equal("_id", id)).first().toFuture(), duration)) match {
      case Success(quizDuelDeadline: QuizDuelDeadline) => Some(quizDuelDeadline)
      case Failure(e) => logThrowableAndReturnNone[QuizDuelDeadline](e)
    }
  }

  override def create(quizDuelDeadline: QuizDuelDeadline): Option[QuizDuelDeadline] = Try(
    Await.result(quizDuelDeadlineCollection.insertOne(quizDuelDeadline).toFuture(), duration)
  ) match {
    case Success(_) => Some(quizDuelDeadline)
    case Failure(e) => logThrowableAndReturnNone[QuizDuelDeadline](e)
  }

  override def update(id: ObjectId, quizDuelDeadline: QuizDuelDeadline): Option[QuizDuelDeadline] = Try(
    Await.result(quizDuelDeadlineCollection.replaceOne(equal("_id", id), quizDuelDeadline).toFuture(), duration)
  ) match {
    case Success(_) => Some(quizDuelDeadline)
    case Failure(e) => logThrowableAndReturnNone[QuizDuelDeadline](e)
  }

  override def delete(id: ObjectId): Option[Long] = {
    Try(Await.result(quizDuelDeadlineCollection.deleteOne(equal("_id", id)).toFuture(), duration)) match {
      case Success(deleteResult) => Some(deleteResult.getDeletedCount)
      case Failure(_) => None
    }
  }
}
