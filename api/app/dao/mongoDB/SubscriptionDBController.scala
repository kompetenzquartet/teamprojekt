package dao.mongoDB

import dao.DAOInterface
import model.subscription.Subscription
import org.mongodb.scala.model.Filters._

import scala.concurrent.Await
import scala.util.{Failure, Success, Try}
import org.bson.types.ObjectId
import dao.DatabaseCollections.subscriptionCollection

class SubscriptionDBController extends DAOInterface[ObjectId, Subscription] {
  override def list(): Option[List[Subscription]] = {
    Try(
      Await.result(subscriptionCollection.find().toFuture(), duration)
    ) match {
      case Success(subscriptions) =>
        Some(subscriptions.map(subscription => subscription).toList)
      case Failure(e) => logThrowableAndReturnNone[List[Subscription]](e)
    }
  }

  def listUserSubscriptions(userId: ObjectId): Option[List[Subscription]] = {
    Try(
      Await.result(
        subscriptionCollection.find(equal("user", userId)).toFuture(),
        duration
      )
    ) match {
      case Success(subscriptions) => Some(subscriptions.toList)
      case Failure(e) => logThrowableAndReturnNone[List[Subscription]](e)
    }
  }

  override def get(id: ObjectId): Option[Subscription] = {
    Try(
      Await.result(
        subscriptionCollection.find(equal("_id", id)).first().toFuture(),
        duration
      )
    ) match {
      case Success(subscription) => Some(subscription)
      case Failure(e)            => logThrowableAndReturnNone[Subscription](e)
    }
  }

  override def create(subscription: Subscription): Option[Subscription] = Try(
    Await.result(
      subscriptionCollection.insertOne(subscription).toFuture(),
      duration
    )
  ) match {
    case Success(_) => Some(subscription)
    case Failure(e) => logThrowableAndReturnNone[Subscription](e)
  }

  override def update(id: ObjectId, modelDAO: Subscription): Option[Subscription] =
    ???

  override def delete(id: ObjectId): Option[Long] = {
    Try(
      Await.result(
        subscriptionCollection.deleteOne(equal("_id", id)).toFuture(),
        duration
      )
    ) match {
      case Success(deleteResult) => Some(deleteResult.getDeletedCount)
      case Failure(_)            => None
    }
  }
}
