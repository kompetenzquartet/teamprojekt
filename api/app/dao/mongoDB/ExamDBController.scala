package dao.mongoDB

import dao.DAOInterface
import dao.DatabaseCollections.examCollection
import model.exam.{Exam, ExamTask}
import org.bson.types.ObjectId
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Updates

import scala.concurrent.Await
import scala.util.{Failure, Success, Try}

class ExamDBController extends DAOInterface[ObjectId, Exam] {

  override def list(): Option[List[Exam]] = {
    Try(Await.result(examCollection.find().toFuture(), duration).toList) match {
      case Success(exams) => Some(exams)
      case Failure(e) => logThrowableAndReturnNone[List[Exam]](e)
    }
  }

  override def get(id: ObjectId): Option[Exam] = {
    Try(Await.result(examCollection.find(equal("_id", id)).first().toFuture(), duration)).toOption
  }

  override def create(exam: Exam): Option[Exam] = Try(
    Await.result(examCollection.insertOne(exam).toFuture(), duration)
  ) match {
    case Success(_) => Some(exam)
    case Failure(e) => logThrowableAndReturnNone[Exam](e)
  }

  override def update(id: ObjectId, exam: Exam): Option[Exam] = Try(
    Await.result(examCollection.replaceOne(equal("_id", id), exam)
      .toFuture(), duration))
  match {
    case Success(_) => Some(exam)
    case Failure(e) => logThrowableAndReturnNone[Exam](e)
  }

  def assignTasks(id: ObjectId, examTasks: List[ExamTask]): Option[Exam] = {
    Try(Await.result(
      examCollection.updateOne(
          equal("_id", id),
          Updates.set("tasks", examTasks))
        .toFuture(), duration)) match {
      case Success(_) => get(id)
      case Failure(e) => logThrowableAndReturnNone[Exam](e)
    }
  }

  override def delete(id: ObjectId): Option[Long] = {
    Try(Await.result(
      examCollection.deleteOne(equal("_id", id)).toFuture(), duration
    )) match {
      case Success(deleteResult) => Some(deleteResult.getDeletedCount)
      case Failure(e) => logThrowableAndReturnNone[Long](e)
    }
  }
}
