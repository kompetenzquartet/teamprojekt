package dao.mongoDB

import org.mongodb.scala.model.Filters.equal

import scala.concurrent.Await
import scala.util.{Failure, Success, Try}
import dao.DAOInterface
import org.bson.types.ObjectId
import dao.DatabaseCollections.quizduelCollection
import model.quiz.QuizDuel

class QuizDuelDBController extends DAOInterface[ObjectId, QuizDuel] {

  override def list(): Option[List[QuizDuel]] = {
    Try(Await.result(quizduelCollection.find().toFuture(), duration)) match {
      case Success(quizduels) => Some(quizduels.toList)
      case Failure(e)         => logThrowableAndReturnNone[List[QuizDuel]](e)
    }
  }

  override def get(id: ObjectId): Option[QuizDuel] = {
    Try(
      Await.result(
        quizduelCollection.find(equal("_id", id)).first().toFuture(),
        duration
      )
    ) match {
      case Success(quizduel) => Some(quizduel)
      case Failure(e)        => logThrowableAndReturnNone[QuizDuel](e)
    }
  }

  override def create(quizduel: QuizDuel): Option[QuizDuel] = Try(
    Await.result(
      quizduelCollection.insertOne(quizduel).toFuture(),
      duration
    )
  ) match {
    case Success(_) => Some(quizduel)
    case Failure(e) => logThrowableAndReturnNone[QuizDuel](e)
  }

  override def update(id: ObjectId, quizduel: QuizDuel): Option[QuizDuel] =
    Try(
      Await.result(
        quizduelCollection
          .replaceOne(equal("_id", id), quizduel)
          .toFuture(),
        duration
      )
    ) match {
      case Success(_) => Some(quizduel)
      case Failure(e) => logThrowableAndReturnNone[QuizDuel](e)
    }

  override def delete(id: ObjectId): Option[Long] = {
    Try(
      Await.result(
        quizduelCollection.deleteOne(equal("_id", id)).toFuture(),
        duration
      )
    ) match {
      case Success(deleteResult) => Some(deleteResult.getDeletedCount)
      case Failure(_)            => None
    }
  }
}
