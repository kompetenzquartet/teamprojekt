package dao.mongoDB

import dao.DAOInterface
import model.course.Course
import model.lecture.Lecture
import model.semester.Semester
import org.mongodb.scala.model.Filters._

import scala.concurrent.Await
import scala.util.{Failure, Success, Try}
import org.bson.types.ObjectId
import model.lecture.SubscribedLecture
import dao.DatabaseCollections.{
  lectureCollection,
  courseCollection,
  semesterCollection
}

class LectureDBController extends DAOInterface[ObjectId, Lecture] {
  override def list(): Option[List[Lecture]] = {
    Try(Await.result(lectureCollection.find().toFuture(), duration)) match {
      case Success(lectures) => Some(lectures.toList)
      case Failure(e)        => logThrowableAndReturnNone[List[Lecture]](e)
    }
  }

  override def get(id: ObjectId): Option[Lecture] = {
    Try(
      Await.result(
        lectureCollection.find(equal("_id", id)).first().toFuture(),
        duration
      )
    ) match {
      case Success(lecture) => Some(lecture)
      case Failure(e)       => logThrowableAndReturnNone[Lecture](e)
    }
  }

  override def create(lecture: Lecture): Option[Lecture] = Try(
    Await
      .result(lectureCollection.insertOne(lecture).toFuture(), duration)
  ) match {
    case Success(_) => Some(lecture)
    case Failure(e) => logThrowableAndReturnNone[Lecture](e)
  }
  override def update(id: ObjectId, lecture: Lecture): Option[Lecture] = Try(
    Await.result(
      lectureCollection.replaceOne(equal("_id", id), lecture).toFuture(),
      duration
    )
  ) match {
    case Success(_) => Some(lecture)
    case Failure(e) => logThrowableAndReturnNone[Lecture](e)
  }

  override def delete(id: ObjectId): Option[Long] = {
    Try(
      Await.result(
        lectureCollection.deleteOne(equal("_id", id)).toFuture(),
        duration
      )
    ) match {
      case Success(deleteResult) => Some(deleteResult.getDeletedCount)
      case Failure(_)            => None
    }
  }

  def getRelatedSemesters(lecture: Lecture): List[ObjectId] = {
    Await
      .result(
        semesterCollection
          .find(in("lectures", lecture._id))
          .map(semester => semester._id)
          .toFuture(),
        duration
      )
      .toList
  }

  def getRelatedCourses(lectureId: ObjectId): List[Course] = {
    Await
      .result(
        courseCollection.find(in("lectures", lectureId)).toFuture(),
        duration
      )
      .toList
  }
}

