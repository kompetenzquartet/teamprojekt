package dao.mongoDB

import dao.DAOInterface
import model.semester.Semester
import org.mongodb.scala.model.Filters._
import org.mongodb.scala.model.Sorts._

import scala.concurrent.Await
import scala.util.{Failure, Success, Try}
import org.bson.types.ObjectId
import model.lecture.Lecture
import dao.DatabaseCollections.semesterCollection
import dao.DatabaseCollections.lectureCollection

class SemesterDBController extends DAOInterface[ObjectId, Semester] {
  override def list(): Option[List[Semester]] = {
    Try(Await.result(semesterCollection.find().toFuture(), duration)) match {
      case Success(semesters) =>
        Some(semesters.map(semester => semester).toList)
      case Failure(e) => logThrowableAndReturnNone[List[Semester]](e)
    }
  }

  def listRecentFour(): Option[List[Semester]] = {
    Try(
      Await.result(
        semesterCollection
          .find()
          .sort(descending("closesAt"))
          .limit(4)
          .toFuture(),
        duration
      )
    ) match {
      case Success(semesters) =>
        Some(semesters.map(semester => semester).toList)
      case Failure(e) => logThrowableAndReturnNone[List[Semester]](e)
    }
  }

  override def get(id: ObjectId): Option[Semester] = {
    Try(
      Await.result(
        semesterCollection.find(equal("_id", id)).first().toFuture(),
        duration
      )
    ) match {
      case Success(semester) => Some(semester)
      case Failure(e)        => logThrowableAndReturnNone[Semester](e)
    }
  }

  override def create(semester: Semester): Option[Semester] = Try(
    Await
      .result(semesterCollection.insertOne(semester).toFuture(), duration)
  ) match {
    case Success(_) => Some(semester)
    case Failure(e) => logThrowableAndReturnNone[Semester](e)
  }

  override def update(id: ObjectId, semester: Semester): Option[Semester] = Try(
    Await.result(
      semesterCollection
        .replaceOne(equal("_id", id), semester)
        .toFuture(),
      duration
    )
  ) match {
    case Success(_) => Some(semester)
    case Failure(e) => logThrowableAndReturnNone[Semester](e)
  }

  override def delete(id: ObjectId): Option[Long] = {
    Try(
      Await.result(
        semesterCollection.deleteOne(equal("_id", id)).toFuture(),
        duration
      )
    ) match {
      case Success(deleteResult) => Some(deleteResult.getDeletedCount)
      case Failure(_)            => None
    }
  }

  def getRelatedLectures(lectureIds: List[ObjectId]): Option[List[Lecture]] = {
    Try(
      Await
        .result(
          lectureCollection.find(in("_id", lectureIds: _*)).toFuture(),
          duration
        )
    ) match {
      case Success(lectures) => Some(lectures.toList)
      case Failure(e)        => logThrowableAndReturnNone[List[Lecture]](e)
    }
  }
}
