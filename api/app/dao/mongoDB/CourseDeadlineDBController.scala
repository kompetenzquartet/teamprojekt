package dao.mongoDB

import dao.DAOInterface
import dao.DatabaseCollections.courseDeadlineCollection
import model.course.CourseDeadline
import org.bson.types.ObjectId
import org.mongodb.scala.model.Filters.equal

import scala.concurrent.Await
import scala.util.{Failure, Success, Try}

class CourseDeadlineDBController extends DAOInterface[ObjectId, CourseDeadline] {

  override def list(): Option[List[CourseDeadline]] = {
    Try(Await.result(courseDeadlineCollection.find().toFuture(), duration)) match {
      case Success(courseDeadline) => Some(courseDeadline.map(courseDeadline => courseDeadline).toList)
      case Failure(e) => logThrowableAndReturnNone[List[CourseDeadline]](e)
    }
  }

  override def get(id: ObjectId): Option[CourseDeadline] = {
    Try(Await.result(courseDeadlineCollection.find(equal("_id", id)).first().toFuture(), duration)) match {
      case Success(courseDeadline: CourseDeadline) => Some(courseDeadline)
      case Failure(e) => logThrowableAndReturnNone[CourseDeadline](e)
    }
  }

  override def create(courseDeadline: CourseDeadline): Option[CourseDeadline] = Try(
    Await.result(courseDeadlineCollection.insertOne(courseDeadline).toFuture(), duration)
  ) match {
    case Success(_) => Some(courseDeadline)
    case Failure(e) => logThrowableAndReturnNone[CourseDeadline](e)
  }

  override def update(id: ObjectId, courseDeadline: CourseDeadline): Option[CourseDeadline] = Try(
    Await.result(
      courseDeadlineCollection.replaceOne(equal("_id", id), courseDeadline).toFuture(),
      duration
    )
  ) match {
    case Success(_) => Some(courseDeadline)
    case Failure(e) => logThrowableAndReturnNone[CourseDeadline](e)
  }

  override def delete(id: ObjectId): Option[Long] = {
    Try(
      Await.result(
        courseDeadlineCollection.deleteOne(equal("_id", id)).toFuture(),
        duration
      )
    ) match {
      case Success(deleteResult) => Some(deleteResult.getDeletedCount)
      case Failure(_) => None
    }
  }
}
