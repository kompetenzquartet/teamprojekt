package dao

import model.BaseModel
import scala.reflect.ClassTag
import org.bson.types.ObjectId
import play.api.Logger
import com.google.common.base.Throwables
import com.typesafe.config.{Config, ConfigFactory}
import scala.concurrent.duration.{Duration, FiniteDuration, MILLISECONDS}

trait DAOInterface[Id, Model] {
  def list(): Option[List[BaseModel]]

  def get(id: Id): Option[BaseModel]

  def create(modelDAO: Model): Option[BaseModel]

  def update(id: Id, modelDAO: Model): Option[BaseModel]

  def delete(id: Id): Option[Long]

  val logger: Logger = Logger(this.getClass)

  protected def logThrowableAndReturnNone[A](e: Throwable): Option[A] = {
    logger.warn(
      s"Database operation failed with \n error message: ${e.getMessage}\n stack trace: ${Throwables
          .getStackTraceAsString(e)}"
    )
    None
  }

  val config: Config = ConfigFactory.load
  val timeoutDurationInMs: Int = config.getInt("database.timeoutInMs")
  val duration: FiniteDuration = Duration(timeoutDurationInMs, MILLISECONDS)
}
