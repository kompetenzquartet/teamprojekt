package dao

import model.course.subs._
import model.course.{Course, CourseDeadline}
import model.exam._
import model.lecture.Lecture
import model.quiz._
import model.semester.Semester
import model.settings.Settings
import model.subscription.Subscription
import model.taskProgress.{TaskProgressRequest, TaskProgressSolution}
import model.user.{User, UserSettings}
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.bson.codecs.configuration.CodecRegistry
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._

object DatabaseRegistry {
  val codecRegistry: CodecRegistry = fromRegistries(
    fromProviders(
      classOf[Subscription],
      classOf[UserSettings],
      classOf[Lecture],
      classOf[Semester],
      classOf[User],
      classOf[TaskProgressRequest],
      classOf[Course],
      classOf[Chapter],
      classOf[Task],
      classOf[TaskData],
      classOf[Solution],
      classOf[QuizTaskSet],
      classOf[TaskIdentifier],
      classOf[QuizDuel],
      classOf[QuizDuelUser],
      classOf[QuizDuelUserScore],
      classOf[Settings],
      classOf[TaskProgressSolution],
      classOf[CourseDeadline],
      classOf[QuizDuelDeadline],
      classOf[Exam],
      classOf[ExamTask],
      classOf[ExamTaskData],
      classOf[ExamSolution],
      classOf[GradeThresholds]
    ),
    DEFAULT_CODEC_REGISTRY
  )
}
