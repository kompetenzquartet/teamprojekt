package dao

import model.course.{Course, CourseDeadline}
import model.lecture.Lecture
import model.quiz.{QuizDuel, QuizTaskSet, QuizDuelDeadline}
import model.semester.Semester
import model.settings.Settings
import model.subscription.Subscription
import model.user.User
import org.mongodb.scala.MongoCollection
import DatabaseClient.database
import model.exam.{Exam, ExamSolution}
import model.taskProgress.TaskProgressRequest

object DatabaseCollections {
  val userCollection: MongoCollection[User] = database.getCollection("users")
  val subscriptionCollection: MongoCollection[Subscription] =
    database.getCollection("subscriptions")
  val lectureCollection: MongoCollection[Lecture] =
    database.getCollection("lectures")
  val semesterCollection: MongoCollection[Semester] =
    database.getCollection("semesters")
  val courseCollection: MongoCollection[Course] =
    database.getCollection("courses")
  val quizTaskSetCollection: MongoCollection[QuizTaskSet] =
    database.getCollection("quizTaskSets")
  val quizduelCollection: MongoCollection[QuizDuel] =
    database.getCollection("quizDuels")
  val quizDuelDeadlineCollection: MongoCollection[QuizDuelDeadline] =
    database.getCollection("quizdueldeadlines")
  val courseDeadlineCollection: MongoCollection[CourseDeadline] =
    database.getCollection("coursedeadlines")
  val settingsCollection: MongoCollection[Settings] =
    database.getCollection("settings")
  val taskProgressCollection: MongoCollection[TaskProgressRequest] =
    database.getCollection("taskProgresses")
  val examCollection: MongoCollection[Exam] =
    database.getCollection("exams")
  val examSolutionCollection: MongoCollection[ExamSolution] =
    database.getCollection("examsolutions")
}
