package dao

import com.typesafe.config.{Config, ConfigFactory}
import org.mongodb.scala.MongoClient
import org.mongodb.scala.MongoDatabase
import DatabaseRegistry.codecRegistry

object DatabaseClient {
  val config: Config = ConfigFactory.load
  val mongoDBConfig: Config = config.getConfig("mongoDB")
  val user: String = mongoDBConfig.getString("mongoDBUser")
  val password: String = mongoDBConfig.getString("mongoDBPassword")
  val dbName: String = mongoDBConfig.getString("databaseName")
  val mongoClient: MongoClient = MongoClient(
    s"mongodb://$user:$password@db:27017/$dbName?authSource=admin"
  )

  val database: MongoDatabase =
    mongoClient
      .getDatabase(dbName)
      .withCodecRegistry(codecRegistry)
}
