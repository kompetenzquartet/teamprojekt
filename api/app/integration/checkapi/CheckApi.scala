package integration.checkapi

trait CheckApi {
  /**
   * Check the code against the solution
   *
   * @param code     the code to check
   * @param solution the solution to check against
   * @return true if the solution is correct, false otherwise
   */
  def checkCodeTask(code: String, solution: String): Option[Boolean]
}
