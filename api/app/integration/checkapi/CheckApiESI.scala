package integration.checkapi

import com.typesafe.config.{Config, ConfigFactory}
import play.api.libs.json.Json
import play.api.libs.ws.WSClient

import javax.inject.Inject
import scala.concurrent.Await
import scala.concurrent.duration.{Duration, FiniteDuration, MILLISECONDS}
import scala.util.Try


class CheckApiESI @Inject()(
                             ws: WSClient
                           ) extends CheckApi {
  private val checkApiConfig: Config = ConfigFactory.load.getConfig("checkApi")
  private val checkApiUrl: String = checkApiConfig.getString("url")
  private val duration: FiniteDuration = Duration(checkApiConfig.getInt("timeoutInMs"), MILLISECONDS)

  override def checkCodeTask(code: String, solution: String): Option[Boolean] = {
    Try(Await.result(ws.url(checkApiUrl + "/check-codetask")
      .post(Json.obj("code" -> code.replace("_#_", solution))), duration))
      .map(response => response.status == 200)
      .toOption
  }
}
