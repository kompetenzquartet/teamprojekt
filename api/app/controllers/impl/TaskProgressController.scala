package controllers.impl

import dao.mongoDB.TaskProgressDBController
import play.api.Environment
import play.api.mvc.ControllerComponents
import controllers.impl.middleware.{AdminMiddelware, AuthMiddleware}
import play.api.mvc.AbstractController
import controllers.ControllerInterface
import com.google.inject.Inject
import play.api.mvc.Action
import play.api.mvc.AnyContent
import org.bson.types.ObjectId
import play.api.mvc.Request
import play.api.libs.json.Json
import model.ErrorResponse
import model.taskProgress.TaskProgress
import model.taskProgress.TaskProgressRequest
import util.JsonValidator.getModelFromRequestBody
import scala.util.Success
import scala.util.Failure
import play.api.libs.ws.WSClient
import scala.util.Try
import scala.concurrent.Await
import model.taskProgress.TaskProgressSolution
import model.course.subs.Task

class TaskProgressController @Inject() (
    cc: ControllerComponents,
    database: TaskProgressDBController,
    env: Environment,
    authAction: AuthMiddleware,
    adminAction: AdminMiddelware,
    ws: WSClient
) extends AbstractController(cc)
    with ControllerInterface {

  override def list: Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      database.list() match {
        case Some(taskProgresses) => Ok(Json.toJson(taskProgresses))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not list task progresses."))
          )
      }
  }

  def getCheckedTaskProgresses(taskProgresses: List[TaskProgress]) =
    taskProgresses.map(taskProgress => {
      if (taskProgress.taskTag == "code-task" && taskProgress.solutions.nonEmpty) {
        checkCodeTask(
          taskProgress.taskCode,
          taskProgress.solutions.head.solution
        ).map(result =>
          result.status match {
            case 200 =>
              taskProgress.copy(solutions =
                List(taskProgress.solutions.head.copy(correctness = true))
              )
            case _: Int =>
              taskProgress.copy(solutions =
                List(
                  taskProgress.solutions.head.copy(correctness = false)
                )
              )
          }
        ).get
      } else {
        taskProgress
      }
    })

  def listUserTaskProgresses(userId: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      database.listUserTaskProgresses(new ObjectId(userId)) match {
        case Some(taskProgresses) =>
          Ok(Json.toJson(getCheckedTaskProgresses(taskProgresses)))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not list user task progresses."))
          )
      }
  }

  def getSubscribedLectureTaskProgresses(
      userId: ObjectId,
      semesterId: ObjectId,
      lectureId: ObjectId
  ) = database.listUserTaskProgresses(userId) match {
    case Some(taskProgresses) =>
      getCheckedTaskProgresses(
        taskProgresses.filter(taskProgress =>
          taskProgress.semesterId == semesterId && taskProgress.lectureId == lectureId
        )
      )
    case None => List()
  }

  def listUserChapterTaskProgresses(
      userId: String,
      semesterId: String,
      lectureId: String,
      courseId: String,
      chapterId: String
  ): Action[AnyContent] = authAction { implicit request: Request[AnyContent] =>
    database.listUserChapterTaskProgresses(
      new ObjectId(userId),
      new ObjectId(semesterId),
      new ObjectId(lectureId),
      courseId,
      chapterId
    ) match {
      case Some(taskProgresses) =>
        Ok(Json.toJson(getCheckedTaskProgresses(taskProgresses)))
      case None =>
        InternalServerError(
          Json.toJson(
            ErrorResponse("Could not get user chapter task progresses")
          )
        )
    }
  }

  override def get(id: String): Action[AnyContent] = ???

  def checkCodeTask(code: String, solution: String) =
    Try(
      Await.result(
        ws
          .url("http://check-api:9000/check-codetask")
          //.url("http://localhost:9001/check-codetask")
          .post(Json.obj("code" -> code.replace("_#_", solution))),
        duration
      )
    )

  def createTaskProgress(
      taskProgress: TaskProgressRequest,
      checked: Boolean = false
  ) = database
    .create(taskProgress)
    .map(taskProgress =>
      Ok(
        if (checked)
          Json.toJson(
            taskProgress
              .copy(solutions =
                List(
                  taskProgress.solutions.head
                    .copy(correctness = true)
                )
              )
          )
        else Json.toJson(taskProgress)
      )
    )
    .getOrElse(
      InternalServerError(
        Json.toJson(
          ErrorResponse("Could not create task progress.")
        )
      )
    )

  override def create: Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[TaskProgressRequest](request.body) match {
        case Success(taskProgress) =>
          database
            .getTask(taskProgress)
            .map(task =>
              if (task.tag == "code-task") {
                val response = createTaskProgress(taskProgress, true)
                checkCodeTask(task.data.code, taskProgress.solutions.head)
                  .map(result =>
                    result.status match {
                      case 200 => response
                      case _: Int =>
                        BadRequest(Json.toJson(ErrorResponse(result.body)))
                    }
                  )
                  .get
              } else createTaskProgress(taskProgress)
            )
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not create task progress"))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  def updateTaskProgress(
      id: String,
      taskProgress: TaskProgressRequest,
      checked: Boolean = false
  ) = database
    .update(new ObjectId(id), taskProgress)
    .map(taskProgress =>
      Ok(
        if (checked)
          Json.toJson(
            taskProgress
              .copy(solutions =
                List(
                  taskProgress.solutions.head
                    .copy(correctness = true)
                )
              )
          )
        else Json.toJson(taskProgress)
      )
    )
    .getOrElse(
      InternalServerError(
        Json.toJson(
          ErrorResponse("Could not update task progress.")
        )
      )
    )

  override def update(id: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[TaskProgressRequest](request.body) match {
        case Success(taskProgress) =>
          database
            .getTask(taskProgress)
            .map(task =>
              if (task.tag == "code-task") {
                val response = updateTaskProgress(id, taskProgress, true)
                checkCodeTask(task.data.code, taskProgress.solutions.head)
                  .map(result =>
                    result.status match {
                      case 200 => response
                      case _: Int =>
                        BadRequest(Json.toJson(ErrorResponse(result.body)))
                    }
                  )
                  .get
              } else updateTaskProgress(id, taskProgress)
            )
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not update task progress."))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  override def delete(id: String): Action[AnyContent] = ???
}
