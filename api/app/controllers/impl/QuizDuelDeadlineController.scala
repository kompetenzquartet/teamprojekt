package controllers.impl

import com.google.inject.Inject
import controllers.ControllerInterface
import controllers.impl.middleware.{AdminMiddelware, AuthMiddleware}
import dao.mongoDB.{QuizDuelDeadlineDBController, SubscriptionDBController}
import model.ErrorResponse
import model.quiz.QuizDuelDeadline
import org.bson.types.ObjectId
import play.api.libs.json.Json
import play.api.mvc._
import util.JsonValidator.getModelFromRequestBody

import scala.util.{Failure, Success}

class QuizDuelDeadlineController @Inject()(cc: ControllerComponents, quizDuelDeadlineDatabase: QuizDuelDeadlineDBController,
                                           subscriptionDatabase: SubscriptionDBController,
                                           authAction: AuthMiddleware, adminAction: AdminMiddelware)
  extends AbstractController(cc) with ControllerInterface {
  override def create: Action[AnyContent] = adminAction { implicit request: Request[AnyContent] =>
    getModelFromRequestBody[QuizDuelDeadline](request.body) match {
      case Success(quizDuelDeadline) =>
        quizDuelDeadlineDatabase.create(quizDuelDeadline)
          .map(quizDuelDeadline => Ok(Json.toJson(quizDuelDeadline)))
          .getOrElse(InternalServerError(Json.toJson(ErrorResponse("Could not create quizDuelDeadline."))))
      case Failure(e) => BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
    }
  }

  def listSubscribedQuizDuelDeadlines: Action[AnyContent] = authAction { implicit request: Request[AnyContent] =>
    authAction.isAuthorized(request) match {
      case Success(user) =>
        quizDuelDeadlineDatabase.list() match {
          case Some(quizDuelDeadlines) =>
            subscriptionDatabase.list() match {
              case Some(subscriptions) =>
                val filteredQuizDuels = for {
                  quizDuelDeadline <- quizDuelDeadlines
                  subscription <- subscriptions
                  if quizDuelDeadline.lecture == subscription.lecture.toString && user._id == subscription.user
                } yield quizDuelDeadline
                Ok(Json.toJson(filteredQuizDuels.distinct))
              case None => InternalServerError(Json.toJson(ErrorResponse("Could not list subscriptions.")))
            }
          case None => InternalServerError(Json.toJson(ErrorResponse("Could not list quizDuelDeadlines.")))
        }
      case Failure(exception) => Unauthorized(exception.getMessage)
    }
  }

  override def list: Action[AnyContent] = authAction { implicit request: Request[AnyContent] =>
    quizDuelDeadlineDatabase.list() match {
      case Some(quizDuelDeadlines) => Ok(Json.toJson(quizDuelDeadlines))
      case None => InternalServerError(Json.toJson(ErrorResponse("Could not list quizDuelDeadlines.")))
    }
  }

  override def get(id: String): Action[AnyContent] = authAction { implicit request: Request[AnyContent] =>
    quizDuelDeadlineDatabase.get(new ObjectId(id)) match {
      case Some(quizDuelDeadline) => Ok(Json.toJson(quizDuelDeadline))
      case None => InternalServerError(Json.toJson(ErrorResponse("Could not get quizDuelDeadline.")))
    }
  }

  override def update(id: String): Action[AnyContent] = adminAction { implicit request: Request[AnyContent] =>
    getModelFromRequestBody[QuizDuelDeadline](request.body) match {
      case Success(quizDuelDeadline) =>
        quizDuelDeadlineDatabase
          .update(new ObjectId(id), quizDuelDeadline)
          .map(quizDuelDeadline => Ok(Json.toJson(quizDuelDeadline)))
          .getOrElse(InternalServerError(Json.toJson(ErrorResponse("Could not update quizDuelDeadline."))))
      case Failure(e) => BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
    }
  }

  override def delete(id: String): Action[AnyContent] = adminAction { implicit request: Request[AnyContent] =>
    quizDuelDeadlineDatabase.delete(new ObjectId(id)) match {
      case Some(value) => Ok(Json.toJson(s"Deleted $value document(s)."))
      case None => InternalServerError(Json.toJson(ErrorResponse("Could not delete quizDuelDeadline.")))
    }
  }
}
