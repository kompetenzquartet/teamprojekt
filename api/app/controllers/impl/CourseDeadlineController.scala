package controllers.impl

import com.google.inject.Inject
import controllers.ControllerInterface
import controllers.impl.middleware.{AdminMiddelware, AuthMiddleware}
import dao.mongoDB.{CourseDeadlineDBController, SubscriptionDBController}
import model.ErrorResponse
import model.course.CourseDeadline
import org.bson.types.ObjectId
import play.api.libs.json.Json
import play.api.mvc._
import util.JsonValidator.getModelFromRequestBody

import scala.util.{Failure, Success}

class CourseDeadlineController @Inject()(cc: ControllerComponents, courseDeadlineDatabase: CourseDeadlineDBController,
                                         subscriptionDatabase: SubscriptionDBController,
                                         authAction: AuthMiddleware, adminAction: AdminMiddelware)
  extends AbstractController(cc) with ControllerInterface {
  override def create: Action[AnyContent] = adminAction { implicit request: Request[AnyContent] =>
    getModelFromRequestBody[CourseDeadline](request.body) match {
      case Success(courseDeadline) =>
        courseDeadlineDatabase.create(courseDeadline)
          .map(courseDeadline => Ok(Json.toJson(courseDeadline)))
          .getOrElse(InternalServerError(Json.toJson(ErrorResponse("Could not create courseDeadline."))))
      case Failure(e) => BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
    }
  }

  def listSubscribedCourseDeadlines: Action[AnyContent] = authAction { implicit request: Request[AnyContent] =>
    authAction.isAuthorized(request) match {
      case Success(user) =>
        courseDeadlineDatabase.list() match {
          case Some(courseDeadlines) =>
            subscriptionDatabase.list() match {
              case Some(subscriptions) =>
                val filteredCourses = for {
                  courseDeadline <- courseDeadlines
                  subscription <- subscriptions
                  if courseDeadline.lecture == subscription.lecture.toString && user._id == subscription.user
                } yield courseDeadline
                Ok(Json.toJson(filteredCourses.distinct))
              case None => InternalServerError(Json.toJson(ErrorResponse("Could not list subscriptions.")))
            }
          case None => InternalServerError(Json.toJson(ErrorResponse("Could not list courseDeadlines.")))
        }
      case Failure(exception) => Unauthorized(exception.getMessage)
    }
  }

  override def list: Action[AnyContent] = authAction { implicit request: Request[AnyContent] =>
    courseDeadlineDatabase.list() match {
      case Some(courseDeadlines) => Ok(Json.toJson(courseDeadlines))
      case None => InternalServerError(Json.toJson(ErrorResponse("Could not list courseDeadlines.")))
    }
  }

  override def get(id: String): Action[AnyContent] = authAction { implicit request: Request[AnyContent] =>
    courseDeadlineDatabase.get(new ObjectId(id)) match {
      case Some(courseDeadline) => Ok(Json.toJson(courseDeadline))
      case None => InternalServerError(Json.toJson(ErrorResponse("Could not get deadline.")))
    }
  }

  override def update(id: String): Action[AnyContent] = adminAction { implicit request: Request[AnyContent] =>
    getModelFromRequestBody[CourseDeadline](request.body) match {
      case Success(courseDeadline) =>
        courseDeadlineDatabase
          .update(new ObjectId(id), courseDeadline)
          .map(courseDeadline => Ok(Json.toJson(courseDeadline)))
          .getOrElse(InternalServerError(Json.toJson(ErrorResponse("Could not update deadline."))))
      case Failure(e) => BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
    }
  }

  override def delete(id: String): Action[AnyContent] = adminAction { implicit request: Request[AnyContent] =>
    courseDeadlineDatabase.delete(new ObjectId(id)) match {
      case Some(value) => Ok(Json.toJson(s"Deleted $value document(s)."))
      case None => InternalServerError(Json.toJson(ErrorResponse("Could not delete courseDeadline.")))
    }
  }

}
