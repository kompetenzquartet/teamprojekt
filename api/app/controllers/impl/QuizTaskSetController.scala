package controllers.impl

import controllers.ControllerInterface
import controllers.impl.middleware.{AdminMiddelware, AuthMiddleware}
import dao.mongoDB.QuizTaskSetDBController
import javax.inject.{Inject, Singleton}
import model.ErrorResponse
import model.quiz.QuizTaskSet
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._

import scala.util.{Failure, Success, Try}
import org.bson.types.ObjectId
import util.JsonValidator.getModelFromRequestBody

@Singleton
class QuizTaskSetController @Inject() (
    cc: ControllerComponents,
    database: QuizTaskSetDBController,
    authAction: AuthMiddleware,
    adminAction: AdminMiddelware
) extends AbstractController(cc)
    with ControllerInterface {

  override def list: Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      database.list() match {
        case Some(quizTaskSet) => Ok(Json.toJson(quizTaskSet))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not list TaskSet."))
          )
      }
  }

  override def get(id: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      database.get(new ObjectId(id)) match {
        case Some(quizTaskSet) => Ok(Json.toJson(quizTaskSet))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not get TaskSet."))
          )
      }
  }

  override def create: Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[QuizTaskSet](request.body) match {
        case Success(quizTaskSet) =>
          database
            .create(quizTaskSet)
            .map(quizTaskSet => Ok(Json.toJson(quizTaskSet)))
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not create TaskSet."))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  override def update(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[QuizTaskSet](request.body) match {
        case Success(quizTaskSet) =>
          database
            .update(new ObjectId(id), quizTaskSet)
            .map(quizTaskSet => Ok(Json.toJson(quizTaskSet)))
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not update lecture."))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  override def delete(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      database.delete(new ObjectId(id)) match {
        case Some(value) => Ok(s"Deleted $value quiz task set(s).")
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not delete quiz task set"))
          )
      }
  }
}
