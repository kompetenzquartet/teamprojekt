package controllers.impl

import controllers.ControllerInterface
import controllers.impl.middleware.{AdminMiddelware, AuthMiddleware}
import dao.mongoDB.SemesterDBController
import javax.inject.Inject
import model.ErrorResponse
import model.semester.Semester
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._
import util.JsonValidator.getModelFromRequestBody

import scala.util.{Failure, Success, Try}
import org.bson.types.ObjectId
import model.semester.SemesterView

class SemesterController @Inject() (
    cc: ControllerComponents,
    database: SemesterDBController,
    authAction: AuthMiddleware,
    adminAction: AdminMiddelware
) extends AbstractController(cc)
    with ControllerInterface {
  override def list: Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      database.list() match {
        case Some(semesters) => Ok(Json.toJson(semesters))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not list semesters."))
          )
      }
  }

  def listView: Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      database.listRecentFour() match {
        case Some(semesters) =>
          Ok(
            Json.toJson[List[SemesterView]](
              semesters.map(semester => semester.toSemesterView)
            )
          )
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not list semesters."))
          )
      }
  }

  override def get(id: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      database.get(new ObjectId(id)) match {
        case Some(semester) => Ok(Json.toJson(semester))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not get semester."))
          )
      }
  }

  override def create: Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[Semester](request.body) match {
        case Success(semester) =>
          database
            .create(semester)
            .map(semester => Ok(Json.toJson(semester)))
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not create semester."))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  override def update(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[Semester](request.body) match {
        case Success(semester) =>
          database
            .update(new ObjectId(id), semester)
            .map(semester => Ok(Json.toJson(semester)))
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not update semester."))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  override def delete(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      database.delete(new ObjectId(id)) match {
        case Some(value) => Ok(s"Deleted $value document(s).")
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not delete semester."))
          )
      }
  }
}
