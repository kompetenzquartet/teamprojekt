package controllers.impl

import controllers.impl.middleware.AdminMiddelware
import dao.mongoDB.CourseDBController
import javax.inject.Inject
import model.course.Course
import parser.shared.State
import play.api.libs.json.Json
import play.api.mvc._
import service.{GitRetrieveService, ParserService}

import scala.collection.mutable.ListBuffer
import scala.util.{Failure, Success, Try}

class ParserController @Inject() (
    cc: ControllerComponents,
    gitService: GitRetrieveService,
    parserService: ParserService,
    database: CourseDBController,
    adminAction: AdminMiddelware
) extends AbstractController(cc) {

  private val secretGitlabToken = "a6f446bc7bc9444ea87c4ae9293fbd5376d09ab3"

  def webhookParse(): Action[AnyContent] = Action {
    implicit request: Request[AnyContent] =>
      request.headers.get("X-Gitlab-Token") match {
        case Some(value) =>
          value match {
            case `secretGitlabToken` => fetchAndParse
            case _                   => BadRequest("Secret token invalid.")
          }
        case None => BadRequest("Secret token not provided.")
      }
  }

  def parseKoansRepository(): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      fetchAndParse
  }

  def fetchAndParse: Result = {
    try {
      State.running()
      val parsed: Option[List[Try[Course]]] = for {
        filesContent <- Some(gitService.retrieveKoanFiles())
        parsedFiles <- Some(parserService.parse(filesContent))
      } yield parsedFiles

      var foundCourses = List[Try[Course]]()
      var exceptions = ListBuffer[String]()

      parsed match {
        case Some(courses) =>
          courses.foreach {
            case Success(course)    => database.create(course)
            case Failure(exception) => exceptions += exception.getMessage
          }
          foundCourses = courses
        case None =>
          throw new NoSuchElementException("Request body could not be parsed!")
      }

      if (exceptions.nonEmpty) {
        BadRequest(Json.toJson(exceptions))
      } else {
        State.succeed()
        Ok(
          Json.toJson(
            foundCourses
              .filter(course => course.isSuccess)
              .map(course => course.get)
          )
        )
      }
    } catch {
      case err: Exception =>
        State.failed(
          new IllegalAccessException(
            s"${err.getMessage}\n${err.getStackTrace.deep.mkString("\n")}"
          )
        )
        BadRequest(
          Json.toJson(
            s"${err.getMessage}\n${err.getStackTrace.deep.mkString("\n")}"
          )
        )
    }
  }
}
