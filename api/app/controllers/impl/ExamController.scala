package controllers.impl

import actors.ExamWebSocketActorFactory
import akka.actor.{ActorRef, ActorSystem}
import akka.stream.Materializer
import controllers.ControllerInterface
import controllers.impl.middleware.{AdminMiddelware, AuthMiddleware}
import dao.mongoDB.{CourseDBController, ExamDBController, SubscriptionDBController, UserDBController, UserExamSolutionDBController}
import model.ErrorResponse
import model.course.subs.{Solution, Task}
import model.exam.request.AssignTaskRequest
import model.exam.{Exam, ExamSolution, ExamStartEvent, UserExamSolution}
import model.subscription.Subscription
import model.user.User
import org.bson.types.ObjectId
import play.api.libs.json.{JsValue, Json}
import play.api.libs.streams.ActorFlow
import play.api.mvc._
import service.exam.converter.TaskToExamTaskConverter
import service.random.RandomCodeGenerator
import util.JsonValidator.getModelFromRequestBody
import util.Roles

import javax.inject.Inject
import scala.concurrent.Future
import scala.util.{Failure, Success}

class ExamController @Inject()(
                                cc: ControllerComponents,
                                userDatabase: UserDBController,
                                adminAction: AdminMiddelware,
                                authAction: AuthMiddleware,
                                examDBController: ExamDBController,
                                courseDBController: CourseDBController,
                                taskToExamTaskConverter: TaskToExamTaskConverter,
                                randomCodeGenerator: RandomCodeGenerator,
                                subscriptionDBController: SubscriptionDBController,
                                userExamSolutionDBController: UserExamSolutionDBController,
                              )(implicit system: ActorSystem, mat: Materializer) extends AbstractController(cc) {

  private var subscribers: Map[User, ActorRef] = Map.empty

  def addSubscriber(user: User, out: ActorRef): Unit = {
    subscribers += (user -> out)
  }

  def removeSubscriber(user: User): Unit = {
    subscribers -= user
  }

  def notifySubscribers(examStartEvent: ExamStartEvent): Unit = {
    subscribers.values.foreach(_ ! Json.toJson(examStartEvent))
  }

  def connect: WebSocket = WebSocket.acceptOrResult[JsValue, JsValue] { request =>
    Future.successful {
      Roles.getUserFromTokenParams(request, userDatabase) match {
        case Success(user) =>
          Right(ActorFlow.actorRef { out =>
            addSubscriber(user, out)
            ExamWebSocketActorFactory.create(this, out, user)
          })
        case Failure(exception) => Left(Unauthorized(exception.getMessage))
      }
    }
  }

  def list: Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      examDBController.list()
        .map(exams => Ok(Json.toJson(exams)))
        .getOrElse(InternalServerError("Could not list exams."))
  }

  def get(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      examDBController.get(new ObjectId(id))
        .map(exam => Ok(Json.toJson(exam)))
        .getOrElse(InternalServerError("Could not get exam."))
  }

  def create: Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[Exam](request.body) match {
        case Success(exam) => examDBController
          .create(exam)
          .map(exam => Ok(Json.toJson(exam)))
          .getOrElse(
            InternalServerError(
              Json.toJson(ErrorResponse("Could not create course."))
            )
          )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  def assignTasks(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      val errorMsg = "Could not assign tasks to exam."
      getModelFromRequestBody[List[AssignTaskRequest]](request.body) match {
        case Success(assignTaskRequests) => examDBController.assignTasks(new ObjectId(id),
            taskToExamTaskConverter.convertList(getTasksFromCourses(assignTaskRequests)))
          .map(exam => Ok(Json.toJson(exam)))
          .getOrElse(InternalServerError(Json.toJson(errorMsg)))
        case Failure(_) => BadRequest(Json.toJson(errorMsg))
      }
  }


  def update(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[Exam](request.body) match {
        case Success(exam) =>
          examDBController
            .update(new ObjectId(id), exam)
            .map(exam => Ok(Json.toJson(exam)))
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not update course."))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  def delete(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      examDBController.delete(new ObjectId(id))
        .filter(_ > 0)
        .map(deletedCount => Ok(Json.toJson(deletedCount)))
        .getOrElse(InternalServerError("Could not delete exam with id " + id))
  }

  def startExam(examId: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[Exam](request.body) match {
        case Success(exam) =>
          examDBController
            .update(new ObjectId(examId), exam.copy(
              isOpen = true,
              startTime = System.currentTimeMillis(),
              endTime = System.currentTimeMillis() + exam.duration * 60000,
              password = randomCodeGenerator.generateCode(5)
            ))
            .map { exam =>
              val examStartEvent = ExamStartEvent(examId)
              notifySubscribers(examStartEvent)
              Ok(Json.toJson(exam))
            }
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not start Exam."))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  def listUserExams(userId: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      val subscribedLectures: Option[List[Subscription]] =
        subscriptionDBController
          .listUserSubscriptions(new ObjectId(userId))
      examDBController.list() match {
        case Some(exams) =>
          val filteredExams =
            exams
              .filter(exam =>
                subscribedLectures.exists(subs => subs.map(_.lecture.toString)
                  .contains(exam.lecture)))
              .map(exam => exam.copy(tasks = List.empty, password = ""))
          Ok(Json.toJson(filteredExams))
        case None => InternalServerError("Could not list exams.")
      }
  }

  def getUserExam(examId: String, userId: String): Action[AnyContent] = authAction {
    val maybeExam: Option[Exam] = examDBController.get(new ObjectId(examId))

    maybeExam match {
      case None => InternalServerError(Json.toJson("Exam not found."))

      case Some(exam) if exam.tasks.isEmpty =>
        InternalServerError(Json.toJson("Exam has no tasks."))

      case Some(exam) =>
        val subscribedLectures = subscriptionDBController
          .listUserSubscriptions(new ObjectId(userId))
          .filter(subs => subs.map(_.lecture.toString).contains(exam.lecture))

        val userExamSolution: Option[UserExamSolution] = userExamSolutionDBController
          .getUserExamSolutionsByExamId(examId)
          .flatMap(_.find(_.userId == userId))

        if (subscribedLectures.isEmpty) {
          BadRequest(Json.toJson("User is not subscribed to this lecture."))
        } else if (exam.examType == "old-exam") {
          if (userExamSolution.isEmpty) {
            exam.tasks.foreach { task =>
              userExamSolutionDBController.create(
                ExamSolution(
                  userId = userId,
                  examId = examId,
                  examTaskId = task._id.toString,
                  solvedWith = task.data.solutions.map(solution => Solution(
                    _id = solution._id,
                    solution = "",
                    correctness = false
                  )),
                  points = -1.0
                )
              )
            }
          }
          Ok(Json.toJson(
            exam.copy(
              password = "",
              tasks = exam.tasks.map(task => task
                .copy(data = task.data.copy(
                  solutions = if (task.tag == "multiple-choice-task" || task.tag == "single-choice-task") {
                    task.data.solutions.map(solution => solution.copy(correctness = false))
                  } else {
                    List.empty
                  }
                ))))))
        } else if (exam.examType == "test-exam" && exam.endTime <= System.currentTimeMillis()) {
          Ok(Json.toJson(
            exam.copy(
              password = "",
              tasks = exam.tasks.map(task => task
                .copy(data = task.data.copy(
                  solutions = if (task.tag == "multiple-choice-task" || task.tag == "single-choice-task") {
                    task.data.solutions.map(solution => solution.copy(correctness = false))
                  } else {
                    List.empty
                  }
                ))))))
        } else if (exam.isOpen
          && exam.startTime <= System.currentTimeMillis()
          && exam.endTime > System.currentTimeMillis()) {
          if (userExamSolution.isEmpty) {
            Unauthorized(Json.toJson(
              exam.copy(
                password = "",
                tasks = List.empty
              )
            ))
          } else {
            Ok(Json.toJson(
              exam.copy(
                password = "",
                tasks = exam.tasks.map(task => task.copy(data = task.data.copy(
                  solutions = if (task.tag == "multiple-choice-task" || task.tag == "single-choice-task") {
                    task.data.solutions.map(solution => solution.copy(correctness = false))
                  } else {
                    List.empty
                  }
                )))
              )
            ))
          }
        } else {
          Ok(Json.toJson(
            exam.copy(
              password = "",
              tasks = List.empty
            )
          ))
        }
    }
  }

  private def getTasksFromCourses(assignTaskRequests: List[AssignTaskRequest]): List[Task] = {
    courseDBController.list() match {
      case Some(courses) =>
        courses
          .filter(course =>
            assignTaskRequests.exists(req => course._id == req.courseId)
          )
          .flatMap(course =>
            course.chapters.filter(chapter =>
              assignTaskRequests.exists(req => chapter._id == req.chapterId)
            ).flatMap(_.tasks)
          )
      case None => List.empty[Task]
    }
  }
}
