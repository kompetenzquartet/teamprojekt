package controllers.impl

import com.google.inject.Inject
import controllers.ControllerInterface
import controllers.impl.middleware.{AdminMiddelware, AuthMiddleware}
import dao.mongoDB.SubscriptionDBController
import model.ErrorResponse
import model.subscription.Subscription
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._

import scala.util.{Failure, Success, Try}
import org.bson.types.ObjectId
import util.JsonValidator.getModelFromRequestBody
import scala.concurrent.Await

class SubscriptionController @Inject() (
    cc: ControllerComponents,
    database: SubscriptionDBController,
    authAction: AuthMiddleware,
    adminAction: AdminMiddelware
) extends AbstractController(cc)
    with ControllerInterface {
  override def list: Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      database.list() match {
        case Some(subscriptions) => Ok(Json.toJson(subscriptions))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not list subscriptions."))
          )
      }
  }

  override def get(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      database.get(new ObjectId(id)) match {
        case Some(subscription) => Ok(Json.toJson(subscription))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not get subscription."))
          )
      }
  }

  override def create: Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[Subscription](request.body) match {
        case Success(subscription) =>
          database
            .create(subscription)
            .map(subscription => Ok(Json.toJson(subscription)))
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not create subscription."))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  override def update(id: String): Action[AnyContent] = ???

  override def delete(id: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      database.delete(new ObjectId(id)) match {
        case Some(value) => Ok(s"Deleted $value subscription(s).")
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not delete subscription."))
          )
      }
  }

  def listUserSubscriptions(userId: String) = authAction {
    implicit request: Request[AnyContent] =>
      database.listUserSubscriptions(new ObjectId(userId)) match {
        case Some(subscriptions) => Ok(Json.toJson(subscriptions))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not get user subscriptions."))
          )
      }
  }
}
