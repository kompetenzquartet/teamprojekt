package controllers.impl

import com.google.inject.Inject
import controllers.ControllerInterface
import controllers.impl.middleware.{AdminMiddelware, AuthMiddleware}
import dao.mongoDB.SettingsDBController
import model.ErrorResponse
import model.settings.Settings
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._

import scala.util.{Failure, Success, Try}
import org.bson.types.ObjectId
import play.api.Logger
import util.JsonValidator.getModelFromRequestBody

class SettingsController @Inject() (
    cc: ControllerComponents,
    database: SettingsDBController,
    authAction: AuthMiddleware,
    adminAction: AdminMiddelware
) extends AbstractController(cc) {
  def list: Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      database.list() match {
        case Some(globalSettings) =>
          Ok(
            Json.toJson(
              globalSettings.headOption.orElse(
                Option(Settings(_id = null))
              )
            )
          )
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not list global settings."))
          )
      }
  }

  def get(): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      database.list() match {
        case Some(globalSetting) => Ok(Json.toJson(globalSetting.head))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not get global setting."))
          )
      }
  }

  def create: Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[Settings](request.body) match {
        case Success(globalSetting) =>
          database
            .create(globalSetting)
            .map(globalSetting => Ok(Json.toJson(globalSetting)))
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not create global setting."))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  def update(): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[Settings](request.body) match {
        case Success(globalSetting) =>
          database
            .update(globalSetting._id, globalSetting)
            .map(globalSetting => Ok(Json.toJson(globalSetting)))
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not update course."))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  def delete(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      database.delete(new ObjectId(id)) match {
        case Some(value) => Ok(s"Deleted $value global setting(s).")
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not delete global setting."))
          )
      }
  }
}
