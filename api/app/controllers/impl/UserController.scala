package controllers.impl

import controllers.ControllerInterface
import controllers.impl.middleware.{AdminMiddelware, AuthMiddleware}
import dao.mongoDB.UserDBController
import javax.inject.Inject
import model.ErrorResponse
import model.user.User
import play.api.Environment
import play.api.Mode.Prod
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._
import util.{JWTUtil, LdapAuthenticator, Roles}

import scala.util.{Failure, Success, Try}
import org.bson.types.ObjectId
import play.api.libs.json.JsValue
import model.user.UserSettings
import model.user.UserProfile
import model.user.UserSettingsRequest
import util.PasswordHasher
import model.user.SignInRequest
import play.api.mvc.Cookie.SameSite
import model.BaseModel
import play.api.libs.json.Reads
import util.JsonValidator.getModelFromRequestBody
import model.user.UserProfileRequest

class UserController @Inject() (
    cc: ControllerComponents,
    database: UserDBController,
    env: Environment,
    authAction: AuthMiddleware,
    adminAction: AdminMiddelware
) extends AbstractController(cc)
    with ControllerInterface {

  override def list: Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      database.list() match {
        case Some(users) => Ok(Json.toJson(users))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not list users."))
          )
      }
  }

  override def get(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      database.get(new ObjectId(id)) match {
        case Some(user) => Ok(Json.toJson(user.toUserProfile))
        case None =>
          InternalServerError(Json.toJson(ErrorResponse("Could not get user.")))
      }
  }

  def getSettings(id: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      database.get(new ObjectId(id)) match {
        case Some(user) => Ok(Json.toJson[UserSettings](user.settings))
        case None =>
          InternalServerError(Json.toJson(ErrorResponse("Could not get user.")))
      }
  }

  override def create: Action[AnyContent] = ???

  def update(id: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[UserProfileRequest](request.body) match {
        case Success(user) =>
          database.updateProfile(new ObjectId(id), user) match {
            case Some(user) => Ok(Json.toJson(user))
            case None =>
              InternalServerError(
                Json.toJson(ErrorResponse("Could not update user."))
              )
          }
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  def updateSettings(id: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[UserSettings](request.body) match {
        case Success(settings) =>
          database.updateSettings(new ObjectId(id), settings) match {
            case Some(user) => Ok(Json.toJson(user.settings))
            case None =>
              InternalServerError(
                Json.toJson(ErrorResponse("Could not update user."))
              )
          }
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  override def delete(id: String): Action[AnyContent] = ???

  def getUserFromToken(): Action[AnyContent] = Action {
    implicit request: Request[AnyContent] =>
      Roles.getUserFromToken(request, database) match {
        case Success(user)      => Ok(Json.toJson(user))
        case Failure(exception) => Unauthorized(exception.getMessage)
      }
  }
  def isAuthenticated: Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] => Ok(Json.obj())
  }

  def signupCasualUser(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    getModelFromRequestBody[User](request.body) match {
      case Failure(exception) =>
        BadRequest(Json.toJson(ErrorResponse("Could not signup user.")))
      case Success(userModel) =>
        database.getByEMail(userModel.email) match {
          case Some(_) =>
            BadRequest(Json.toJson(ErrorResponse("Email already exists.")))
          case None =>
            database.create(
              User(
                email = userModel.email,
                username = userModel.username,
                password = userModel.password
              ).encryptPassword
            ) match {
              case None =>
                InternalServerError(Json.toJson(ErrorResponse("Could not signup user.")))
              case Some(user) =>
                Ok(JWTUtil.attachAccessToken(user.toUserProfile, user._id.toString, userModel.email))
            }
        }
    }
  }

  def signinHtwg(): Action[AnyContent] =
    Action { implicit request: Request[AnyContent] =>
      getModelFromRequestBody[SignInRequest](request.body) match {
        case Failure(exception) =>
          Unauthorized(
            Json.toJson(ErrorResponse("Could not authenticate HTWG user."))
          )
        case Success(userModel) => {
          val loginName: String = userModel.email.split('@')(0)

          LdapAuthenticator.authenticateAtHtwg(
            loginName,
            userModel.password
          ) match {
            case Success(ldapResponse) =>
              database.getByEMail(userModel.email) match {
                case Some(user) =>
                  Ok(
                    JWTUtil.attachAccessToken(
                      user.toUserProfile,
                      user._id.toString,
                      userModel.email
                    )
                  )
                case None =>
                  val username: String = ldapResponse
                    .getAttributes(
                      "uid=" + loginName + ",ou=users,dc=fh-konstanz,dc=de"
                    )
                    .get("cn")
                    .get()
                    .toString
                  database.create(
                    User(
                      email = userModel.email,
                      username = username,
                      permissions = Roles.StudentRole
                    )
                  ) match {
                    case Some(user) =>
                      Ok(
                        JWTUtil.attachAccessToken(
                          user.toUserProfile,
                          user._id.toString,
                          userModel.email
                        )
                      )
                    case None =>
                      InternalServerError(
                        Json.toJson(
                          ErrorResponse("Could not authenticate HTWG user.")
                        )
                      )
                  }
              }
            case Failure(_) =>
              Unauthorized(
                Json.toJson(ErrorResponse("Could not authenticate HTWG user."))
              )
          }
        }
      }
    }

  def signinCasualUser() = Action { implicit request: Request[AnyContent] =>
    getModelFromRequestBody[SignInRequest](request.body) match {
      case Failure(exception) =>
        Unauthorized(
          Json.toJson(ErrorResponse("Could not authenticate user."))
        )
      case Success(model) =>
        database.getByEMail(model.email) match {
          case None =>
            Unauthorized(
              Json.toJson(ErrorResponse("Could not authenticate user."))
            )
          case Some(user) =>
            if (user != null && PasswordHasher.checkHash(model.password, user.password))
              database.updateRefreshToken(user._id) match {
                case None =>
                  Unauthorized(
                    Json.toJson(ErrorResponse("Could not authenticate user."))
                  )
                case Some(user) =>
                  Ok(
                    JWTUtil.attachAccessToken(
                      user.toUserProfile,
                      user._id.toString,
                      model.email
                    )
                  )
              }
            else
              Unauthorized(
                Json.toJson(ErrorResponse("Could not authenticate user."))
              )
        }
    }

  }

  def refreshAccessToken(id: String) = Action {
    implicit request: Request[AnyContent] =>
      request.cookies.get("refresh_token") match {
        case Some(refreshToken) =>
          if (JWTUtil.isValidToken(refreshToken.value))
            database.get(new ObjectId(id)) match {
              case None =>
                Unauthorized(
                  Json.toJson(ErrorResponse("Could not authenticate user."))
                )
              case Some(user) =>
                if (user.refreshToken == refreshToken.value)
                  Ok(
                    JWTUtil.attachAccessToken(
                      user.toUserProfile,
                      user._id.toString,
                      user.email
                    )
                  )
                else
                  Unauthorized(
                    Json.toJson(ErrorResponse("Could not authenticate user."))
                  )
            }
          else
            Unauthorized(
              Json.toJson(ErrorResponse("Could not authenticate user."))
            )
        case None =>
          Unauthorized(
            Json.toJson(ErrorResponse("Could not authenticate user."))
          )
      }
  }
}
