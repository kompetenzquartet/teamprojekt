package controllers.impl

import controllers.ControllerInterface
import controllers.impl.middleware.{AdminMiddelware, AuthMiddleware}
import dao.mongoDB.{ExamDBController, UserExamSolutionDBController}
import model.ErrorResponse
import model.course.subs.Solution
import model.exam.ExamSolution
import org.bson.types.ObjectId
import play.api.libs.json.Json
import play.api.mvc._
import service.exam.grade.ExamSolutionPointsCalculationService
import util.JsonValidator.getModelFromRequestBody

import javax.inject.Inject
import scala.util.{Failure, Success}

class UserExamSolutionController @Inject()(
                                            cc: ControllerComponents,
                                            authAction: AuthMiddleware,
                                            adminAction: AdminMiddelware,
                                            userExamSolutionDBController: UserExamSolutionDBController,
                                            examDBController: ExamDBController,
                                            pointsCalculationService: ExamSolutionPointsCalculationService
                                          ) extends AbstractController(cc) with ControllerInterface {


  override def list: Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      userExamSolutionDBController.list()
        .map(exams => Ok(Json.toJson(exams)))
        .getOrElse(InternalServerError("Could not list Exam Solutions."))
  }

  override def get(id: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      userExamSolutionDBController.get(new ObjectId(id))
        .map(exam => Ok(Json.toJson(exam)))
        .getOrElse(InternalServerError("Could not get Exam Solution."))
  }

  override def create: Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[ExamSolution](request.body) match {
        case Success(examSolution) =>
          userExamSolutionDBController
            .create(examSolution)
            .map(solution => Ok(Json.toJson(solution)))
            .getOrElse(
              InternalServerError(
                Json.toJson("Could not create Exam Solution.")
              )
            )
        case scala.util.Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  def authorizeUser(examId: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      val authCode = request.body.asJson.get("authCode").as[String]
      examDBController.get(new ObjectId(examId)) match {
        case Some(exam) =>
          if (authCode.equals(exam.password) && exam.password != "") {
            authAction.isAuthorized(request).toOption match {
              case Some(user) =>
                exam.tasks.foreach { task =>
                  userExamSolutionDBController.create(
                    ExamSolution(
                      userId = user._id.toString,
                      examId = examId,
                      examTaskId = task._id,
                      solvedWith = task.data.solutions.map(solution => Solution(
                        _id = solution._id,
                        solution = "",
                        correctness = false
                      )),
                      points = -1.0
                    )
                  )
                }
                Ok("User authorized")
              case None => Unauthorized("Authorization failed")
            }
          } else {
            Unauthorized("Das eingegebene Passwort ist falsch")
          }
        case None => NotFound("Exam not found")
      }
  }

  override def update(id: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[ExamSolution](request.body) match {
        case Success(examSolution) =>
          examDBController.get(new ObjectId(examSolution.examId)) match {
            case Some(exam) =>
              if ((exam.examType == "exam" || exam.examType == "test-exam") && exam.endTime < System.currentTimeMillis()) {
                Forbidden(Json.toJson("Exam has ended."))
              } else {
                exam.tasks.find(_._id == examSolution.examTaskId) match {
                  case Some(examTask) =>
                    val points = pointsCalculationService.calculatePoints(examTask, examSolution)
                    userExamSolutionDBController.update(new ObjectId(id), examSolution.copy(points = points)) match {
                      case Some(updatedExam) => Ok(Json.toJson(updatedExam))
                      case None =>
                        InternalServerError(Json.toJson("Could not update Exam Solution."))
                    }
                }
              }
            case None =>
              NotFound(Json.toJson("Could not find Exam Task."))
          }

        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }


  def updateMany(): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[List[ExamSolution]](request.body) match {
        case Success(examSolutions) =>
          val updatedExamSolutions = userExamSolutionDBController.updateMany(examSolutions)
          Ok(Json.toJson(updatedExamSolutions))
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  override def delete(id: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      InternalServerError(Json.toJson("Exam Solution deletion is not allowed."))
  }


  def listGroupedByUser(examId: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      userExamSolutionDBController.getUserExamSolutionsByExamId(examId)
        .map(exams => Ok(Json.toJson(exams)))
        .getOrElse(InternalServerError("Could not list Exam Solutions."))
  }

  def listUserExamSolutions(examId: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      authAction.isAuthorized(request).map(user =>
        userExamSolutionDBController.list()
          .map(_.filter(solution => solution.examId == examId && solution.userId == user._id.toString))
          .map(solutions => Ok(Json.toJson(solutions.map(_.copy(points = -1.0)))))
          .getOrElse(InternalServerError("Could not list Exam Solutions."))
      ).getOrElse(Unauthorized("Could not authorize user."))
  }
}
