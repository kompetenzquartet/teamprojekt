package controllers.impl

import controllers.ControllerInterface
import controllers.impl.middleware.{AdminMiddelware, AuthMiddleware}
import dao.mongoDB.{CourseDBController, SubscriptionDBController}
import model.ErrorResponse
import model.course.{Course, CourseChapter, CourseRequest}
import play.api.libs.json.Json
import play.api.libs.ws.WSClient
import play.api.mvc._
import util.JsonValidator.getModelFromRequestBody

import javax.inject.Inject
import scala.concurrent.Await
import scala.concurrent.duration.{Duration, FiniteDuration, MILLISECONDS}
import scala.util.{Failure, Success}

class CourseController @Inject() (
    cc: ControllerComponents,
    courseDBController: CourseDBController,
    subscriptionDBController: SubscriptionDBController,
    authAction: AuthMiddleware,
    adminAction: AdminMiddelware,
    ws: WSClient
) extends AbstractController(cc)
    with ControllerInterface {
  override def list: Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      courseDBController.list() match {
        case Some(courses) => Ok(Json.toJson(courses))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not list courses."))
          )
      }
  }

  def listSubscribedCoursesWithoutSolutions: Action[AnyContent] = authAction { implicit request: Request[AnyContent] =>
    authAction.isAuthorized(request) match {
      case Success(user) =>
        subscriptionDBController.list() match {
          case Some(subscriptions) =>
            courseDBController.list() match {
              case Some(courses) =>
                val subscribedCourses = for {
                  subscription <- subscriptions
                  course <- courses
                  if subscription.user == user._id && course.lectures.contains(subscription.lecture)
                } yield course
                Ok(Json.toJson(subscribedCourses.distinct.map(course => course.copy(chapters = List.empty))))
              case None => InternalServerError(Json.toJson(ErrorResponse("Could not list courses."))
              )
            }
          case None => InternalServerError(Json.toJson(ErrorResponse("Could not list subscriptions."))
          )
        }
      case Failure(exception) => Unauthorized(exception.getMessage)
    }
  }

  def getChapter(id: String, chapterId: String): Action[AnyContent] =
    authAction { implicit request: Request[AnyContent] =>
      courseDBController.get(id) match {
        case Some(course) =>
          Ok(
            Json.toJson(
              CourseChapter(
                course._id,
                course.title,
                course.chapters.find(_._id == chapterId)
              )
            )
          )
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not get chapter."))
          )
      }
    }

  override def get(id: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      courseDBController.get(id) match {
        case Some(course) =>
          Ok(Json.toJson(course))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not get course."))
          )
      }
  }

  override def create: Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[Course](request.body) match {
        case Success(course) =>
          courseDBController
            .create(course)
            .map(course => Ok(Json.toJson(course)))
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not create course."))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  override def update(id: String): Action[AnyContent] = ???

  def updateLectures(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[CourseRequest](request.body) match {
        case Success(course) =>
          courseDBController
            .updateLectures(id, course)
            .map(course => Ok(Json.toJson(course)))
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not update course."))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  def getCodeSolution(
      courseId: String,
      chapterId: String,
      taskId: String
  ): Action[AnyContent] = authAction { implicit request: Request[AnyContent] =>
    courseDBController.get(courseId) match {
      case Some(course) =>
        course.chapters.find(chapter => chapter._id == chapterId) match {
          case Some(chapter) =>
            chapter.tasks.find(task => task._id == taskId) match {
              case Some(task) => Ok(Json.toJson(task.data.codeSolution))
              case None => NotFound(s"Could not find task with id ${taskId}")
            }
          case None => NotFound(s"Could not find chapter with id ${chapterId}")
        }
      case None =>
        InternalServerError(Json.toJson(ErrorResponse("Could not get course.")))
    }
  }

  def checkCodeTask: Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      request.body.asJson match {
        case Some(bodyContent) =>
          val duration: FiniteDuration = Duration(5000, MILLISECONDS)
          val code = (bodyContent \ "code").get.as[String]
          val response = ws
            .url("http://check-api:9000/check-codetask")
            .post(Json.obj("code" -> code))
          val result = Await.result(response, duration)
          result.status match {
            case 200 => Ok(Json.toJson(result.body))
            case _   => BadRequest(Json.toJson(new ErrorResponse(result.body)))
          }
        case None =>
          BadRequest(Json.toJson(new ErrorResponse("Missing code to check.")))
      }
  }

  override def delete(id: String): Action[AnyContent] = ???
}
