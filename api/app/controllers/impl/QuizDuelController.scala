package controllers.impl

import java.util.concurrent.TimeUnit

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.google.inject.Inject
import controllers.impl.middleware.{AdminMiddelware, AuthMiddleware}
import dao.mongoDB.{
  CourseDBController,
  SettingsDBController,
  QuizTaskSetDBController,
  QuizDuelDBController,
  UserDBController
}
import model.ErrorResponse
import model.quiz._
import model.user.User
import play.api.libs.json.{JsValue, Json}
import play.api.libs.streams.ActorFlow
import play.api.mvc._
import service.QuizDuelService
import util.Roles

import scala.concurrent.Future
import scala.language.postfixOps
import scala.swing.Publisher
import scala.swing.event.Event
import scala.util.{Failure, Success}
import org.bson.types.ObjectId
import util.JsonValidator.getModelFromRequestBody
import actors.QuizDuelWebSocketActorFactory
import dao.mongoDB.LectureDBController
import dao.mongoDB.SemesterDBController
import model.course.subs.TaskIdentifier
import model.course.subs.TaskWithIdentifier
import controllers.ControllerInterface
import scala.concurrent.duration.Duration
import scala.concurrent.ExecutionContext
import java.util.concurrent.Executors

class QuizDuelController @Inject() (
    cc: ControllerComponents,
    userDatabase: UserDBController,
    quizTaskSetDatabase: QuizTaskSetDBController,
    database: QuizDuelDBController,
    lectureDatabase: LectureDBController,
    semesterDatabase: SemesterDBController,
    courseDatabase: CourseDBController,
    settingsDatabase: SettingsDBController,
    taskProgressController: TaskProgressController,
    authAction: AuthMiddleware,
    adminAction: AdminMiddelware
) extends AbstractController(cc)
    with Publisher
    with ControllerInterface {
  implicit val sys: ActorSystem = ActorSystem("quiz-duel")
  implicit val mat: ActorMaterializer = ActorMaterializer()

  var quizDuelService: Option[QuizDuelService] = Option.empty

  def getQuizDuelView(
      quizDuel: QuizDuel,
      participants: List[QuizDuelUser],
      remainingTime: Long,
      isRunning: Boolean
  ) = if (isOpenedQuizDuel && quizDuelService.get.quizDuelId == quizDuel._id)
    QuizDuelView(
      _id = quizDuel._id,
      lecture = lectureDatabase
        .get(quizDuel.lectureId)
        .map(lecture => lecture.name)
        .get,
      semester = semesterDatabase
        .get(quizDuel.semesterId)
        .map(semester => semester.name)
        .get,
      quizTaskSet = quizTaskSetDatabase
        .get(quizDuel.quizTaskSetId)
        .map(quizTaskSet => quizTaskSet.name)
        .get,
      participants = quizDuelService.get.participants,
      remainingTime = quizDuelService.get.remainingTime,
      isRunning = true
    )
  else
    QuizDuelView(
      _id = quizDuel._id,
      lecture = lectureDatabase
        .get(quizDuel.lectureId)
        .map(lecture => lecture.name)
        .get,
      semester = semesterDatabase
        .get(quizDuel.semesterId)
        .map(semester => semester.name)
        .get,
      quizTaskSet = quizTaskSetDatabase
        .get(quizDuel.quizTaskSetId)
        .map(quizTaskSet => quizTaskSet.name)
        .get
    )

  override def list = adminAction { implicit request: Request[AnyContent] =>
    database.list() match {
      case Some(quizDuels) =>
        Ok(
          Json.toJson(
            quizDuels.map(quizDuel =>
              getQuizDuelView(
                quizDuel,
                if (
                  isOpenedQuizDuel && quizDuel._id == quizDuelService.get.quizDuelId
                )
                  quizDuelService.get.participants
                else List(),
                if (
                  isOpenedQuizDuel && quizDuel._id == quizDuelService.get.quizDuelId
                )
                  quizDuelService.get.remainingTime
                else 0,
                if (
                  isOpenedQuizDuel && quizDuel._id == quizDuelService.get.quizDuelId
                ) true
                else false
              )
            )
          )
        )
      case None =>
        InternalServerError(
          Json.toJson(ErrorResponse("Could not list quiz duels"))
        )
    }
  }

  def listOriginal: Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      database.list() match {
        case Some(quizDuels) => Ok(Json.toJson(quizDuels))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not list quizDuels."))
          )
      }
  }

  def connect = WebSocket.acceptOrResult[JsValue, JsValue] { request =>
    Future.successful(if (!isOpenedQuizDuel) {
      Left(Forbidden("Quiz duel is currently not available"))
    } else {
      Roles.getUserFromTokenParams(request, userDatabase) match {
        case Success(user) =>
          Right(ActorFlow.actorRef { out =>
            QuizDuelWebSocketActorFactory.create(this, out, user)
          })
        case Failure(exception) => Left(Unauthorized(exception.getMessage))
      }
    })
  }

  override def create() = adminAction { implicit request: Request[AnyContent] =>
    getModelFromRequestBody[QuizDuel](request.body) match {
      case Success(quizDuel) =>
        database
          .create(quizDuel)
          .map(quizDuel => Ok(Json.toJson(quizDuel)))
          .getOrElse(
            InternalServerError(
              Json.toJson(ErrorResponse("Could not create quiz duel"))
            )
          )
      case Failure(e) =>
        BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
    }
  }

  implicit val context =
    ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor())

  def start(id: String) = adminAction { implicit request: Request[AnyContent] =>
    database.get(new ObjectId(id)) match {
      case None =>
        BadRequest(Json.toJson(new ErrorResponse("Quiz duel not found")))
      case Some(quizDuel) => {
        if (quizDuelService.nonEmpty) {
          quizDuelService.get.finishExecution()
        }

        val tasks = quizTaskSetDatabase
          .get(quizDuel.quizTaskSetId)
          .map(quizTaskSet =>
            quizTaskSet.tasks.map(taskIdentifier => {
              val task =
                courseDatabase.getTaskFromTaskIdentifier(taskIdentifier).get
              TaskWithIdentifier(
                taskIdentifier.taskId,
                task.tag,
                task.data,
                taskIdentifier.courseId,
                taskIdentifier.chapterId
              )
            })
          )
          .get

        quizDuelService = Some(
          QuizDuelService(
            this,
            new ObjectId(id),
            tasks,
            settingsDatabase.koanTaskTime,
            settingsDatabase.codeTaskTime
          )
        )
        Future {
          quizDuelService.get.execute()
        }

        Ok("Quiz duel started")
      }
    }
  }

  def getActive = authAction { implicit request: Request[AnyContent] =>
    if (isOpenedQuizDuel)
      database.get(quizDuelService.get.quizDuelId) match {
        case None => InternalServerError("Quiz duel currently not available")
        case Some(quizDuel) =>
          Ok(
            Json.toJson(
              getQuizDuelView(
                quizDuel,
                quizDuelService.get.participants,
                quizDuelService.get.remainingTime,
                true
              )
            )
          )
      }
    else
      Ok
  }

  def isOpenedQuizDuel = quizDuelService.nonEmpty

  def stopQuizDuel(id: String) = {
    logger.warn(isOpenedQuizDuel.toString())
    logger.warn((quizDuelService.get.quizDuelId != new ObjectId(id)).toString())
    if (
      !isOpenedQuizDuel || quizDuelService.get.quizDuelId != new ObjectId(id)
    ) {
      BadRequest(Json.toJson(new ErrorResponse("Quiz duel not started")))
    }

    quizDuelService.get.stop

    val quizDuel = database.get(new ObjectId(id)).get

    val data = quizDuel.copy(participants =
      quizDuel.participants.map(participant =>
        participant.copy(scores =
          participant.scores ::: quizDuelService
            .map(service =>
              service.participants
                .find(other => other._id == participant._id)
                .map(participant => participant.scores)
                .getOrElse(List())
            )
            .getOrElse(List())
        )
      ) ::: quizDuelService.get.participants.filter(participant =>
        quizDuel.participants
          .find(other => other._id == participant._id)
          .isEmpty
      )
    )

    logger.warn(Json.toJson(data).toString())

    database
      .update(new ObjectId(id), data)

    quizDuelService = Option.empty

    Ok("Quiz duel stopped")
  }

  def stop(id: String) = adminAction { implicit request: Request[AnyContent] =>
    stopQuizDuel(id)
  }

  def skipTask() = adminAction { implicit request: Request[AnyContent] =>
    quizDuelService match {
      case Some(executor) => {
        executor.skipTask()

        Ok("quiz duel task skipped")
      }
      case None =>
        BadRequest(Json.toJson(new ErrorResponse("Quiz duel not started")))
    }
  }

  def submitSolution(user: User, msg: QuizDuelSubmitMessage) =
    quizDuelService match {
      case Some(executor) => {
        val score = executor.timeScore
        val task = courseDatabase.getTaskFromTaskIdentifier(msg.task).get

        if (task.tag == "code-task") {
          taskProgressController.checkCodeTask(
            task.data.code,
            msg.solutions.head
          ) match {
            case Failure(exception) =>
            case Success(value) =>
              executor.submitSolution(user, msg.task, score)
          }
        } else if (
          task.tag == "koan-task" && task.data.solutions
            .filter(solution => solution.correctness)
            .map(solution => solution.solution)
            .equals(msg.solutions)
        ) {
          executor.submitSolution(user, msg.task, score)
        }
      }
      case None =>
    }

  def addParticipant(user: User) =
    quizDuelService match {
      case Some(executor) =>
        executor.addParticipant(
          QuizDuelUser(userId = user._id, username = user.username)
        )
      case None =>
    }

  def removeParticipant(user: User) =
    quizDuelService match {
      case Some(executor) => executor.removeParticipant(user)
      case None           =>
    }

  override def publish(e: Event) = super.publish(e)

  override def get(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      database.get(new ObjectId(id)) match {
        case Some(quizDuel) =>
          Ok(Json.toJson(quizDuel))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not get quiz duel"))
          )
      }
  }

  override def delete(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      database.delete(new ObjectId(id)) match {
        case Some(value) => Ok(s"Deleted $value document(s).")
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not delete quiz duel"))
          )
      }
  }

  override def update(id: String) = ???
}
