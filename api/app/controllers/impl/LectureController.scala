package controllers.impl

import controllers.ControllerInterface
import controllers.impl.middleware.{AdminMiddelware, AuthMiddleware}
import dao.mongoDB.LectureDBController
import javax.inject.Inject
import model.ErrorResponse
import model.lecture.Lecture
import play.api.libs.json.{JsError, JsSuccess, Json}
import play.api.mvc._
import util.JsonValidator.getModelFromRequestBody

import scala.util.{Failure, Success, Try}
import org.bson.types.ObjectId
import model.lecture.SubscribedLecture
import dao.DatabaseClient.user
import dao.mongoDB.SubscriptionDBController

class LectureController @Inject() (
    cc: ControllerComponents,
    database: LectureDBController,
    authAction: AuthMiddleware,
    adminAction: AdminMiddelware,
    taskProgressController: TaskProgressController,
    subscriptionsDB: SubscriptionDBController
) extends AbstractController(cc)
    with ControllerInterface {

  override def list: Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      database.list() match {
        case Some(lectures) => Ok(Json.toJson(lectures))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not list lectures."))
          )
      }
  }

  override def get(id: String): Action[AnyContent] = authAction {
    implicit request: Request[AnyContent] =>
      database.get(new ObjectId(id)) match {
        case Some(lecture) => Ok(Json.toJson(lecture))
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not get lecture."))
          )
      }
  }

  override def create: Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[Lecture](request.body) match {
        case Success(lecture) =>
          database
            .create(lecture)
            .map(lecture => Ok(Json.toJson(lecture)))
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not create lecture."))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  override def update(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      getModelFromRequestBody[Lecture](request.body) match {
        case Success(lecture) =>
          database
            .update(new ObjectId(id), lecture)
            .map(lecture => Ok(Json.toJson(lecture)))
            .getOrElse(
              InternalServerError(
                Json.toJson(ErrorResponse("Could not update lecture."))
              )
            )
        case Failure(e) =>
          BadRequest(Json.toJson(new ErrorResponse(e.getMessage)))
      }
  }

  override def delete(id: String): Action[AnyContent] = adminAction {
    implicit request: Request[AnyContent] =>
      database.delete(new ObjectId(id)) match {
        case Some(value) => Ok(s"Deleted $value document(s).")
        case None =>
          InternalServerError(
            Json.toJson(ErrorResponse("Could not delete lecture."))
          )
      }
  }

  def getSubscribedLectureCourses(
      userId: String,
      semesterId: String,
      lectureId: String
  ): Action[AnyContent] = authAction { implicit request: Request[AnyContent] =>
    database.get(new ObjectId(lectureId)) match {
      case Some(lecture) =>
        Ok(
          Json.toJson(
            lecture.toSubscribedLecture(
              taskProgressController.getSubscribedLectureTaskProgresses(
                new ObjectId(userId),
                new ObjectId(semesterId),
                new ObjectId(lectureId)
              )
            )
          )
        )
      case None =>
        InternalServerError(
          Json.toJson(
            ErrorResponse("Could not get subscribed lecture courses.")
          )
        )
    }
  }

  def getSubscribedLectures(
      userId: String,
      semesterId: String
  ): Action[AnyContent] = authAction { implicit request: Request[AnyContent] =>
    subscriptionsDB.list() match {
      case None =>
        InternalServerError(
          Json.toJson(ErrorResponse("Could not get subscribed lectures."))
        )
      case Some(subscriptions) =>
        database.list() match {
          case None =>
            InternalServerError(
              Json.toJson(ErrorResponse("Could not get subscribed lectures."))
            )
          case Some(lectures) =>
            Ok(
              Json.toJson(
                lectures
                  .filter(lecture =>
                    subscriptions
                      .find(subscription =>
                        subscription.lecture == lecture._id && subscription.semester == new ObjectId(
                          semesterId
                        )
                      )
                      .nonEmpty
                  )
                  .map(lecture =>
                    lecture.toSubscribedLecture(
                      taskProgressController.getSubscribedLectureTaskProgresses(
                        new ObjectId(userId),
                        new ObjectId(semesterId),
                        lecture._id
                      )
                    )
                  )
              )
            )
        }
    }
  }
}
