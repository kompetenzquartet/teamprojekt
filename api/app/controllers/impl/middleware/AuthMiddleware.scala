package controllers.impl.middleware

import dao.mongoDB.UserDBController
import javax.inject.Inject
import model.user.User
import play.api.mvc.Results._
import play.api.mvc._
import util.Roles

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

class AuthMiddleware @Inject() (
    parser: BodyParsers.Default,
    database: UserDBController
)(implicit ec: ExecutionContext)
    extends ActionBuilderImpl(parser) {
  override def invokeBlock[A](
      request: Request[A],
      block: (Request[A]) => Future[Result]
  ): Future[Result] = isAuthorized(request) match {
    case Success(_) => block(request)
    case Failure(exception) =>
      Future.successful(Unauthorized(exception.getMessage))
  }

  def isAuthorized[A](request: Request[A]): Try[User] =
    Roles.getUserFromToken(request, database)
}
