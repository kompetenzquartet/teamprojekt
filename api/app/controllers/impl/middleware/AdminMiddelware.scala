package controllers.impl.middleware

import dao.mongoDB.UserDBController
import javax.inject.Inject
import play.api.mvc.Results._
import play.api.mvc._
import util.Roles

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

class AdminMiddelware @Inject()(parser: BodyParsers.Default, database: UserDBController)(implicit ec: ExecutionContext) extends ActionBuilderImpl(parser) {
  override def invokeBlock[A](request: Request[A], block: (Request[A]) => Future[Result]): Future[Result] = {
    Roles.getUserFromToken(request, database) match {
      case Success(user) =>
        if (user.permissions != Roles.AdminRole) {
          Future.successful(Forbidden("Not an admin"))
        } else {
          block(request)
        }
      case Failure(exception) => Future.successful(Unauthorized(exception.getMessage))
    }
  }

  def isAdmin[A](request: Request[A]): Try[Unit] = {
    Roles.getUserFromToken(request, database) match {
      case Success(user) =>
        if (user.permissions != Roles.AdminRole) {
          Failure(new Exception())
        } else {
          Success(())
        }
      case Failure(exception) => Failure(exception)
    }
  }
}
