package controllers

import model.BaseModel
import play.api.mvc.{Action, AnyContent}

import scala.util.Try
import org.bson.types.ObjectId
import play.api.libs.json.Reads
import play.api.Logger
import com.typesafe.config.{Config, ConfigFactory}
import scala.concurrent.duration.{Duration, FiniteDuration, MILLISECONDS}

trait ControllerInterface {
  def list: Action[AnyContent]

  def get(id: String): Action[AnyContent]

  def create: Action[AnyContent]

  def update(id: String): Action[AnyContent]

  def delete(id: String): Action[AnyContent]

  val logger: Logger = Logger(this.getClass)

  val config: Config = ConfigFactory.load
  val timeoutDurationInMs: Int = config.getInt("database.timeoutInMs")
  val duration: FiniteDuration = Duration(timeoutDurationInMs, MILLISECONDS)
}
