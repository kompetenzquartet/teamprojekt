package parser.utils

case class CodeTaskSuite(title: String, id: String, tasks: List[Task])
    extends JsonObject

case class Koan(
    data: KoanData,
    var id: String = "",
    tag: String = "koan-task"
) extends Task

case class Video(
    data: VideoData,
    var id: String = "",
    tag: String = "video-task"
) extends Task

case class CodeTask(
    data: CodeTaskData,
    var id: String = "",
    tag: String = "code-task"
) extends Task

case class Info(
    data: InfoData,
    var id: String = "",
    tag: String = "info-task"
) extends Task

case class Reflection(
    data: ReflectionData,
    var id: String = "",
    tag: String = "reflection-task"
) extends Task

case class MultipleChoice(
    data: MultipleChoiceData,
    var id: String = "",
    tag: String = "multiple-choice-task"
) extends Task

case class SingleChoice(
    data: SingleChoiceData,
    var id: String = "",
    tag: String = "single-choice-task"
) extends Task
