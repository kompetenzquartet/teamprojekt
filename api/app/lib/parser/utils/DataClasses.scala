package parser.utils

case class KoanData(
    description: String,
    code: String,
    solutions: List[String],
    mode: String = "scala",
    time: Int = 0
) extends JsonObject

case class VideoData(description: String, url: String) extends JsonObject

case class CodeTaskData(
    description: String,
    code: String,
    codeSolution: String,
    solutions: List[String],
    mode: String = "scala",
    time: Int = 0
) extends JsonObject

case class InfoData(description: String) extends JsonObject

case class ReflectionData(description: String) extends JsonObject

case class ChoiceSolution(solution: String, correctness: Boolean)

case class SingleChoiceData(
    description: String,
    solutions: List[ChoiceSolution]
) extends JsonObject

case class MultipleChoiceData(
    description: String,
    solutions: List[ChoiceSolution]
) extends JsonObject
