package parser.model

case class Meta(id: String, title: String, description: String) extends FileContent