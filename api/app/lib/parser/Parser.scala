package lib.parser

import parser.error.ParsingError
import parser.utils._

import scala.util.matching.Regex
import scala.util.parsing.combinator.RegexParsers
import scala.util.parsing.input.CharArrayReader

class Parser(val skipCodeTask: Boolean = false) extends RegexParsers {
  val solutionRegex: Regex = """(should\s+(be|equal)\s*\(("[^"]+"|[^\n]+)\))"""
    .r("ShouldBe", "assert", "Solution")
  val taskTimeRegex: Regex = """\s*,\s*(\d+)\s*""".r("time")

  private[parser] def trimLine(text: String): String = text
    .replaceAll("^\\s+", "")
    .replaceAll("\\s+$", "")
    .replaceAll("^(\\\\n)+", "")
    .replaceAll("(\\\\n)+$", "")
    .replaceAll("^(\\n)+", "")
    .replaceAll("(\\n)+$", "")
    .replaceAll("^\\s+", "")
    .replaceAll("\\s+$", "")

  override val skipWhitespace = false

  implicit def textToInput(text: String): Input =
    new CharArrayReader(text.toCharArray, 0).asInstanceOf[Input]

  private[parser] def _EOF: Regex = "\\z".r

  private[parser] def _LINE_BREAK = "(\\n|\\r\\n)".r

  private[parser] def _line: Parser[String] = this._line(_EOF)

  private[parser] def _lineUntilAppearance(until: Regex): Parser[String] =
    s"[^\\n\\r]+?(?=${until.regex})".r ^^ {
      _.replaceAll("^\\s+", " ")
    }

  private[parser] def _line(until: Regex): Parser[String] =
    not(this._EOF) ~> (not(until) ~> guard(
      s"[^\\n\\r]+${until.regex}".r
    ) ~> _lineUntilAppearance(until) |
      not(until) ~> "[^\\n\\r]*".r ~ opt("[\\n\\r]".r) ^^ {
        case l ~ Some(_) => l.trim + "\n"
        case l ~ None    => l.trim
      } |
      failure(s"Reached the expected line: ${until.regex}"))

  // region String parsing
  private[parser] def quotedString: Parser[String] = "\"\"\"" ~>
    commit(
      (("[\\s\\S]*?(?=\"\"\"([^\"]|\\z))".r withFailureMessage stringNotClosed) ^^ {
        v => s"""${"\""}""$v""${"\""}"""
      }) <~ "\"\"\""
    ) | "\"" ~>
    commit(
      (("[\\s\\S]*?((?<!\\\\)|(?<!\\\\)(\\\\\\\\)+)(?=\")".r withFailureMessage stringNotClosed) ^^ {
        v => s""""$v""""
      }) <~ "\""
    )

  private[parser] def stringNotClosed = "String is not closed"

  private[parser] def stringThreeQuotes: Parser[String] =
    "\"\"\"" ~> ("[\\s\\S]*?(?=\"\"\"([^\"]|\\z))".r withFailureMessage stringNotClosed) <~
      "\"\"\""

  private[parser] def stringOneQuote: Parser[String] = "\"" ~>
    ("[^\\r\\n]*?((?<!\\\\)|(?<!\\\\)(\\\\\\\\)+)(?=\")".r withFailureMessage stringNotClosed) <~ "\"" ^^ {
      _.replaceAll("\\\\\"", "\"")
    }

  private[parser] def string: Parser[String] = guard("\"\"\"") ~> commit(
    stringThreeQuotes
  ) | guard("\"") ~> commit(stringOneQuote)

  // endregion

  private[parser] def comment: Parser[String] = "[ \\t]*//[^\\r\\n]*".r

  private[parser] def taskTime: Parser[Int] = taskTimeRegex ^^ { m =>
    taskTimeRegex.findFirstMatchIn(m).get.group(1).toInt
  }

  // region Brackets ()/{}
  private[parser] def curBracketsContent: Parser[String] =
    rep(
      comment | _line("[{(\"\\}]".r) | curBrackets | brackets | quotedString
    ) ^^ (_.mkString)

  private[parser] def curBrackets: Parser[String] =
    "{" ~> curBracketsContent <~ "}" ^^ { v => s"{${v}}" }

  private[parser] def bracketsContent: Parser[String] =
    rep(
      comment | _line("[{(\")]".r) | curBrackets | brackets | quotedString
    ) ^^ (_.mkString)

  private[parser] def brackets: Parser[String] =
    "(" ~> bracketsContent <~ ")" ^^ { v => s"(${v})" }

  // endregion

  // description combining strings with a + sign
  private[parser] def description: Parser[(String, Int)] =
    "\\s*\\(\\s*".r ~> string ~ opt(
      rep1("\\s*\\+\\s*".r ~> commit(string))
    ) ~ opt(taskTime) <~ "\\s*\\)".r ^^ {
      case first ~ Some(text) ~ Some(time) => (first + text.mkString, time)
      case first ~ Some(text) ~ None       => (first + text.mkString, 0)
      case text ~ None ~ Some(time)        => (text, time)
      case text ~ None ~ None              => (text, 0)
    }

  case class AssertParsed(before: String, after: String)

  // region Koan
  private[parser] def assert: Parser[AssertParsed] = "(" ~>
    (assertBool | (
      rep1(
        _line("(\"|\\s*==|\\s*===|\\seq\\s|\\s*\\.\\s*eq)".r) | quotedString
      ) ~ (
        "\\s*===\\s*".r ~> compare(3) |
          "\\s*==\\s*".r ~> compare() |
          "\\seq\\s+".r ~> gapEqual |
          "\\s*\\.\\s*eq".r ~> equal
      ) ^^ { case before ~ after =>
        AssertParsed(s"${before.mkString}${after.before}", after.after)
      }
    ))

  private[parser] def assertBool: Parser[AssertParsed] =
    ("\\s*true\\s*".r | "\\s*false\\s*".r) ^^ { v =>
      AssertParsed("__", v.trim)
    }

  private[parser] def compare(signs: Int = 2): Parser[AssertParsed] =
    "\\s*".r ~> rep1(_line("([\"),])".r) | quotedString) ^^ { v =>
      AssertParsed(s" ${"=" * signs} __", v.mkString.trim)
    }

  private[parser] def gapEqual: Parser[AssertParsed] =
    rep1(_line("([\"),])".r) | quotedString) ^^ { v =>
      AssertParsed(" eq __", v.mkString.trim)
    }

  private[parser] def equal: Parser[AssertParsed] =
    "(" ~> bracketsContent <~ ")" ^^ { v =>
      AssertParsed(".eq(__)", v.mkString.trim)
    }

  private[parser] def assertString(
      value: AssertParsed ~ Option[String]
  ): AssertParsed = value match {
    case assertParsed ~ Some(text) =>
      AssertParsed(s"assert(${assertParsed.before}, $text)", assertParsed.after)
    case assertParsed ~ None =>
      AssertParsed(s"assert(${assertParsed.before})", assertParsed.after)
  }

  private[parser] def shouldBe: Parser[(String, String)] =
    "(" ~> bracketsContent <~ ")" ^^ { v => (s"should be(${v})", v) }

  private[parser] def shouldEqual: Parser[(String, String)] =
    "(" ~> bracketsContent <~ ")" ^^ { v => (s"should equal(${v})", v) }

  private[parser] def koanTasks: Parser[(String, List[String])] = rep1(
    "assert\\s*".r ~> commit(assert) ~ opt(
      "\\s*,\\s*".r ~> commit(quotedString)
    ) <~ ")" ^^ {
      assertString
    } |
      _line("(\\{|\\(|\"|\\}|should\\s+be|assert)".r) |
      "should\\s+be\\s*".r ~> commit(shouldBe) |
      "should\\s+equal\\s*".r ~> commit(shouldEqual) |
      curBrackets | brackets | quotedString
  ) ^^ { body =>
    {
      val stringBuilder = new StringBuilder()
      val answers = scala.collection.mutable.ArrayBuffer.empty[String]
      body.foreach {
        case (should: String, answer: String) =>
          stringBuilder.append(should)
          answers += answer
        case assertParsed: AssertParsed =>
          stringBuilder.append(assertParsed.before)
          answers += assertParsed.after
        case line: String => stringBuilder.append(line)
      }
      (stringBuilder.toString, answers.toList)
    }
  }

  private[parser] def koanBody: Parser[(String, List[String])] =
    "\\s*\\{".r ~> koanTasks <~ "}"

  private[parser] def koan: Parser[Koan] = description ~ koanBody ^^ {
    case desc ~ body =>
      val newBody: String = solutionRegex.replaceAllIn(
        body._1,
        m => m.group("ShouldBe").replace(m.group("Solution"), "__")
      )
      val solutionResults = solutionRegex.findAllMatchIn(body._1)
      val solutions = solutionResults.map(s => s.group("Solution")).toList
      if (body._2.nonEmpty && solutions.isEmpty) {
        Koan(KoanData(desc._1, this.trimLine(newBody), body._2, time = desc._2))
      } else {
        Koan(
          KoanData(desc._1, this.trimLine(newBody), solutions, time = desc._2)
        )
      }
  }

  // endregion


// region SingleChoice

  // eg: "true"
  private[parser] def boolean: Parser[Boolean] =
    ("true" | "false") ^^ (_.toBoolean)

  // eg: '"Option 1", false'
  private[parser] def  choiceSolution: Parser[ChoiceSolution] =
    string ~ "\\s*,\\s*".r ~ boolean ^^ {
      case text ~ _ ~ isCorrect  => ChoiceSolution(solution = text, correctness = isCorrect)
  }

  // eg: '("Option 1", false),'
  private[parser] def choiceSolutionWrapper: Parser[ChoiceSolution] =
    "\\s*\\(\\s*".r ~> choiceSolution <~ "\\s*\\)\\s*".r <~ opt("\\s*,\\s*".r)

  // eg: '(("Option 1", false), ("Option 1", true))' =
  private[parser] def choiceSolutionListWrapper: Parser[List[ChoiceSolution]] =
    "\\s*\\(\\s*".r ~> rep(choiceSolutionWrapper) <~ "\\s*\\)\\s*".r

  // eg: '{ val solutuions = List(("Option 1", false), ("Option 1", true))'
  private[parser] def choiceBody: Parser[List[ChoiceSolution]] =
    """\s*\{\s*val\s*solutions\s*=\s*List""".r ~> choiceSolutionListWrapper <~ "\\s*\\}\\s*".r ^^ { elem => elem }

  // eg: '("Question1 or Option2?") { val solutions = List(("Option 1", false), ("Option 2", true))}'
  private[parser] def singleChoice: Parser[SingleChoice] =
    description ~ choiceBody ^^ {
      case desc ~ solutions =>
        // val choices =>
        val descField = desc._1
        val bodyField = solutions
        SingleChoice(SingleChoiceData(descField, bodyField))
        // console.log("SingleChoiceData(descField, bodyField): ", SingleChoiceData(descField, bodyField))
    }

  // endregion

  // region MultipleChoice
  private[parser] def multipleChoiceDescription: Parser[String] =
    "\\s*\\(\\s*".r ~> string <~ "\\s*\\)".r


  private[parser] def multipleChoice: Parser[MultipleChoice] =
    multipleChoiceDescription ~ choiceBody ^^ { case desc ~ solutions =>
      MultipleChoice(MultipleChoiceData(desc, solutions))
  }
  // endregion

  // region Video
  private[parser] def videoBody: Parser[(String, String)] =
    (string <~ "\\s*,\\s*".r) ~ string ^^ { case desc ~ url => (desc, url) }

  private[parser] def video: Parser[Video] =
    "\\s*\\(\\s*".r ~> videoBody <~ "\\s*\\)".r ^^ { v =>
      Video(VideoData(v._1, v._2))
    }

  // endregion

  // region Infos
  private[parser] def info: Parser[Info] = {
    val descOnly: Parser[String] = string
    "\\s*\\(\\s*".r ~> descOnly <~ "\\s*\\)".r ^^ { desc =>
      Info(InfoData(desc))
    }
  }

  // endregion

  // region Reflection
  private[parser] def reflection: Parser[Reflection] = {
    val descOnly: Parser[String] = string
    "\\s*\\(\\s*".r ~> descOnly <~ "\\s*\\)".r ^^ { desc =>
      Reflection(ReflectionData(desc))
    }
  }

  // endregion

  // region Codetask
  private[parser] def codetaskSolve: Parser[String] =
    rep(_line("//\\s*endsolve".r)) <~ "//\\s*endsolve\\s*".r ^^ { m =>
      m.mkString.stripMargin
    }

  private[parser] def codetaskTest: Parser[String] =
    rep(_line("//\\s*endtest".r)) ^^ { m => m.mkString.stripMargin }

  private[parser] def codetaskBracketsContent: Parser[String] = rep(
    comment | _line(
      "[{(\")]".r
    ) | codetaskBody | codetaskBrackets | curBrackets | brackets | quotedString
  ) ^^ (_.mkString)

  private[parser] def codetaskBracketsSimpleContent: Parser[String] = rep(
    comment | _line("[{(\")]".r) | curBrackets | brackets | quotedString
  ) ^^ (_.mkString)

  private[parser] def codetaskBrackets: Parser[String] =
    "(" ~> codetaskBracketsContent <~ ")"

  private[parser] def codetaskCurBracketsContent
      : Parser[List[(String, String, String)]] = rep1(
    "\\s*//\\s*solve".r ~> commit(codetaskSolve) ^^ { m =>
      ("codeSolutions", "_#_\n", m)
    } |
      "\\s*//\\s*test".r ~> commit(codetaskTest) ^^ { m =>
        ("valueSolutions", m, m)
      } |
      _line("""//\s*endtest""".r) ^^ { m => ("boilerCode", m, m) }
  )

  private[parser] def codetaskBody: Parser[List[(String, String, String)]] =
    "\\s*\\{".r ~> codetaskCurBracketsContent <~ """//\s*endtest\s*\}""".r

  private[parser] def codetask: Parser[CodeTask] =
    description ~ codetaskBody ^^ { case desc ~ code =>
      val boilerCode = new StringBuilder()
      var codeSolutions = ""
      var valueSolutions = scala.collection.mutable.Buffer.empty[String]
      code.foreach(s => {
        boilerCode.append(s._2)
        s._1 match {
          case "codeSolutions" => codeSolutions = s._3.trim
          case "valueSolutions" =>
            val solutionResults = solutionRegex.findAllMatchIn(s._3)
            valueSolutions =
              solutionResults.map(s => s.group("Solution")).toBuffer
          case _ =>
        }
      })
      CodeTask(
        CodeTaskData(
          desc._1.trim,
          boilerCode.toString.trim,
          codeSolutions,
          valueSolutions.toList,
          time = desc._2
        )
      )
    }

  private[parser] def codetaskDecider: Parser[Any] = if (skipCodeTask) {
    codetask ^^^ {
      ""
    }
  } else {
    commit(codetask)
  }

  // endregion

  private[parser] def tasks: Parser[List[Task]] = "\\s*\\{".r ~> rep(
    "\\s*koan".r ~> commit(koan) |
      "\\s*video".r ~> commit(video) |
      "\\s*info".r ~> commit(info) |
      "\\s*singleChoice".r ~> commit(singleChoice) |
      "\\s*multipleChoice".r ~> commit(multipleChoice) |
      "\\s*reflection".r ~> commit(reflection) |
      "\\s*codetask".r ~> codetaskDecider |
      comment | _line("[{(\"\\}]".r) | curBrackets | brackets | quotedString
  ) <~ "\\s*\\}".r ^^ {
    _.filter(v => v.isInstanceOf[Task]).asInstanceOf[List[Task]]
  }

  private[parser] def className: Parser[Any] =
    _line("[ \\t(]".r) ~ opt(brackets) ~ "\\s*".r

  private[parser] def codetaskSuiteMeta: Parser[(String, String)] =
    "CodeTaskSuite" ~> "\\s*\\(\\s*".r ~>
      (string <~ "\\s*,\\s*".r) ~ ("\\d+".r <~ "\\s*\\)".r) ^^ {
        case name ~ id => (name, id)
      }

  private[parser] def inheritance: Parser[(String, String)] =
    rep(not("CodeTaskSuite") ~ className ~ "with\\s+".r) ~>
      codetaskSuiteMeta <~ rep("\\s*with\\s+".r ~> className)

  private[parser] def extend: Parser[(String, String)] =
    "extends\\s+".r ~> inheritance

  private[parser] def codetaskSuite: Parser[CodeTaskSuite] =
    className ~> extend ~ commit(tasks) ^^ { case meta ~ tasks =>
      val (name, id) = meta
      CodeTaskSuite(name, id, tasks)
    }

  private[parser] def unknownClass: Parser[Any] = className <~ opt(
    "extends\\s+".r ~ rep(className ~ "with\\s+".r) ~ className
  ) <~ opt(curBrackets)

  private[parser] def fileParser: Parser[Any] = rep(
    opt("\\s*case\\s+".r) ~> "\\s*class\\s+".r ~> commit(
      codetaskSuite | unknownClass
    ) |
      _line
  ) ^^ {
    _.filter(v => v.isInstanceOf[CodeTaskSuite])
  }

  /** This function is used to parse a CodeTask file.
    *
    * @param file
    *   Is the content of the file.
    * @throws ParsingError
    * @return
    *   List[CodeTaskSuite]
    */
  def parse(file: String): List[CodeTaskSuite] =
    parseAll(fileParser, file) match {
      case Success(msg, _) =>
        val codeTaskSuites = msg.asInstanceOf[List[CodeTaskSuite]]
        codeTaskSuites.foreach(codeTaskSuite => {
          codeTaskSuite.tasks.zipWithIndex.foreach { case (k: Task, i) =>
            k.id = (i + 1).toString
          }
        })
        codeTaskSuites
      case NoSuccess(err, next) => throw ParsingError(err, next.pos)
    }
}
