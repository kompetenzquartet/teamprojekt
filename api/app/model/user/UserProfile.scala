package model.user

import model.BaseModel
import org.bson.types.ObjectId
import util.Roles

import play.api.libs.json._
import org.mongodb.scala.bson.annotations.BsonProperty
import org.bson.codecs.pojo.annotations.BsonId
import play.api.libs.functional.syntax._

object UserProfile {

  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[UserProfile] = (user: UserProfile) =>
    Json.obj(
      "_id" -> user._id,
      "email" -> user.email,
      "username" -> user.username,
      "permissions" -> user.permissions,
      "studentNumber" -> user.studentNumber,
    )

  implicit val readsFormat: Reads[UserProfile] = (
    (JsPath \ "_id").readWithDefault(ObjectId.get()) and
      (JsPath \ "email").read[String] and
      (JsPath \ "username").read[String] and
      (JsPath \ "permissions").readWithDefault(Roles.UserRole) and
      (JsPath \ "studentNumber").readWithDefault("")
  )(UserProfile.apply _)
  implicit val formatDefault: OFormat[UserProfile] =
    OFormat(readsFormat, writesFormat)
}

case class UserProfile(
    _id: ObjectId = ObjectId.get(),
    email: String,
    username: String,
    permissions: String = Roles.UserRole,
    studentNumber: String = "",
) extends BaseModel {}
