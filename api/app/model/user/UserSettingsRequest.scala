package model.user

import model.BaseModel
import play.api.libs.json._
import play.api.libs.functional.syntax._

object UserSettingsRequest {
  implicit val writesFormat: OWrites[UserSettingsRequest] = (user: UserSettingsRequest) => Json.obj("username" -> user.username)
  implicit val readsFormat: Reads[UserSettingsRequest] = (__ \ "username").read[String].map(UserSettingsRequest.apply)
  implicit val formatDefault: OFormat[UserSettingsRequest] = OFormat(readsFormat, writesFormat)
}
case class UserSettingsRequest(username: String) extends BaseModel {}
