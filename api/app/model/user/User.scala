package model.user

import util.{JWTUtil, Roles, PasswordHasher}
import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._
import org.mongodb.scala.bson.annotations.BsonProperty
import org.bson.codecs.pojo.annotations.BsonId
import play.api.libs.functional.syntax._

object User {
  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[User] = (user: User) =>
    Json.obj(
      "_id" -> user._id,
      "email" -> user.email,
      "username" -> user.username,
      "permissions" -> user.permissions,
      "studentNumber" -> user.studentNumber,
      "settings" -> user.settings,
    )

  implicit val readsFormat: Reads[User] = (
    (JsPath \ "_id").readWithDefault(ObjectId.get()) and
      (JsPath \ "email").read[String] and
      (JsPath \ "username").read[String] and
      (JsPath \ "password").readWithDefault("") and
      (JsPath \ "permissions").readWithDefault(Roles.UserRole) and
      (JsPath \ "studentNumber").readWithDefault("") and
      (JsPath \ "settings").readWithDefault(UserSettings()) and
      (JsPath \ "refreshToken").readWithDefault(JWTUtil.createRefreshToken)
  )(User.apply _)
  implicit val formatDefault: OFormat[User] = OFormat(readsFormat, writesFormat)
}

case class User(
    _id: ObjectId = ObjectId.get(),
    email: String,
    username: String,
    password: String = "",
    permissions: String = Roles.UserRole,
    studentNumber: String = "",
    settings: UserSettings = UserSettings(),
    refreshToken: String = JWTUtil.createRefreshToken
) extends BaseModel {
  def encryptPassword: User = copy(password = PasswordHasher.getHash(password))
  def toUserProfile: UserProfile =
    UserProfile(_id, email, username, permissions, studentNumber)
}
