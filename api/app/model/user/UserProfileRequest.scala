package model.user

import model.BaseModel
import play.api.libs.json._
import play.api.libs.functional.syntax._

object UserProfileRequest {
  implicit val formatDefault: OFormat[UserProfileRequest] =
    new OFormat[UserProfileRequest] {
      override def reads(json: JsValue): JsResult[UserProfileRequest] =
        Json
          .using[Json.WithDefaultValues]
          .format[UserProfileRequest]
          .reads(json)

      override def writes(o: UserProfileRequest): JsObject =
        Json.using[Json.WithDefaultValues].format[UserProfileRequest].writes(o)
    }
}

case class UserProfileRequest(username: String, studentNumber: String = "")
    extends BaseModel {}
