package model.user

import model.BaseModel
import play.api.libs.json._
import play.api.libs.functional.syntax._

object SignInRequest {

  implicit val writesFormat: OWrites[SignInRequest] =
    (signInRequest: SignInRequest) =>
      Json.obj(
        "email" -> signInRequest.email
      )

  implicit val readsFormat: Reads[SignInRequest] = (
    (JsPath \ "email").read[String] and
      (JsPath \ "password").read[String]
  )(SignInRequest.apply _)
  implicit val formatDefault: OFormat[SignInRequest] =
    OFormat(readsFormat, writesFormat)
}

case class SignInRequest(email: String, password: String) extends BaseModel {}
