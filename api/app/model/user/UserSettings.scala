package model.user

import model.BaseModel
import play.api.libs.json._

object UserSettings {
  implicit val writesFormat: OWrites[UserSettings] = (settings: UserSettings) =>
    Json.obj(
      "autoPlay" -> settings.autoPlay,
      "autoNext" -> settings.autoNext,
      "infoText" -> settings.infoText
    )
  implicit val formatDefault: OFormat[UserSettings] = new OFormat[UserSettings] {
    override def reads(json: JsValue): JsResult[UserSettings] =
      Json.using[Json.WithDefaultValues].format[UserSettings].reads(json)

    override def writes(o: UserSettings): JsObject = writesFormat.writes(o)
  }
}

case class UserSettings(
    autoPlay: Boolean = true,
    autoNext: Boolean = true,
    infoText: Boolean = true
) extends BaseModel
