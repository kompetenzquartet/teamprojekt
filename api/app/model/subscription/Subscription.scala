package model.subscription

import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._

object Subscription {
  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[Subscription] =
    (subscription: Subscription) =>
      Json.obj(
        "_id" -> subscription._id,
        "user" -> subscription.user,
        "lecture" -> subscription.lecture,
        "semester" -> subscription.semester
      )
  implicit val formatDefault: OFormat[Subscription] =
    new OFormat[Subscription] {
      override def reads(json: JsValue): JsResult[Subscription] =
        Json.using[Json.WithDefaultValues].format[Subscription].reads(json)

      override def writes(o: Subscription): JsObject = writesFormat.writes(o)
    }
}

case class Subscription(
    _id: ObjectId = ObjectId.get(),
    user: ObjectId,
    lecture: ObjectId,
    semester: ObjectId
) extends BaseModel
