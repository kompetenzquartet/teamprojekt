package model.lecture

import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._
import org.mongodb.scala.bson.annotations.BsonProperty
import org.bson.codecs.pojo.annotations.BsonId

object LectureView {
  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[LectureView] =
    (lectureView: LectureView) =>
      Json.obj(
        "_id" -> lectureView._id,
        "name" -> lectureView.name,
        "description" -> lectureView.description
      )
  implicit val formatDefault: OFormat[LectureView] = new OFormat[LectureView] {
    override def reads(json: JsValue): JsResult[LectureView] =
      Json.using[Json.WithDefaultValues].format[LectureView].reads(json)

    override def writes(o: LectureView): JsObject = writesFormat.writes(o)
  }
}

case class LectureView(
    _id: ObjectId,
    name: String,
    description: String
) extends BaseModel
