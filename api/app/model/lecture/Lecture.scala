package model.lecture

import dao.mongoDB.LectureDBController
import model.BaseModel
import model.lecture.SubscribedLecture.lectureMongoDB
import org.bson.types.ObjectId
import play.api.libs.json._
import org.mongodb.scala.bson.annotations.BsonProperty
import org.bson.codecs.pojo.annotations.BsonId
import play.api.libs.functional.syntax._
import model.taskProgress.TaskProgress

object Lecture {
  implicit val writesFormat: OWrites[Lecture] = (lecture: Lecture) =>
    Json.obj(
      "_id" -> lecture._id,
      "name" -> lecture.name,
      "description" -> lecture.description,
      "courses" -> lectureMongoDB
        .getRelatedCourses(lecture._id)
        .map(course => course._id)
    )


  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val formatDefault: OFormat[Lecture] = new OFormat[Lecture] {
    override def reads(json: JsValue): JsResult[Lecture] =
      Json.using[Json.WithDefaultValues].format[Lecture].reads(json)

    override def writes(o: Lecture): JsObject = writesFormat.writes(o)
  }
}

case class Lecture(
    _id: ObjectId = ObjectId.get(),
    name: String,
    description: String = ""
) extends BaseModel {
  def toLectureView: LectureView = LectureView(_id, name, description)
  def toSubscribedLecture(taskProgresses: List[TaskProgress]): SubscribedLecture = SubscribedLecture(_id, name, taskProgresses)
}
