package model.lecture

import dao.mongoDB.LectureDBController
import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._
import org.mongodb.scala.bson.annotations.BsonProperty
import org.bson.codecs.pojo.annotations.BsonId
import model.taskProgress.TaskProgress

object SubscribedLecture {
  val lectureMongoDB = new LectureDBController()

  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[SubscribedLecture] =
    (lecture: SubscribedLecture) => {
      val courses = lectureMongoDB
        .getRelatedCourses(lecture._id)

      Json.obj(
        "_id" -> lecture._id,
        "name" -> lecture.name,
        "courses" -> courses.map(course =>
          course.toCourseView(lecture.taskProgresses)
        ),
      )
    }

  implicit val formatDefault: OFormat[SubscribedLecture] =
    new OFormat[SubscribedLecture] {
      override def reads(json: JsValue): JsResult[SubscribedLecture] =
        Json.using[Json.WithDefaultValues].format[SubscribedLecture].reads(json)

      override def writes(o: SubscribedLecture): JsObject =
        writesFormat.writes(o)
    }
}

case class SubscribedLecture(
    _id: ObjectId,
    name: String,
    taskProgresses: List[TaskProgress]
) extends BaseModel
