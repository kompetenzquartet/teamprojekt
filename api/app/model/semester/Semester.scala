package model.semester

import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._
import org.mongodb.scala.bson.annotations.BsonProperty
import org.bson.codecs.pojo.annotations.BsonId
import model.lecture.Lecture

object Semester {
  implicit val writesFormat: OWrites[Semester] = (semester: Semester) =>
    Json.obj(
      "_id" -> semester._id,
      "name" -> semester.name,
      "opensAt" -> semester.opensAt,
      "closesAt" -> semester.closesAt,
      "lectures" -> semester.lectures
    )

  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val formatDefault: OFormat[Semester] = new OFormat[Semester] {
    override def reads(json: JsValue): JsResult[Semester] =
      Json.using[Json.WithDefaultValues].format[Semester].reads(json)

    override def writes(o: Semester): JsObject = writesFormat.writes(o)
  }
}

case class Semester(
    _id: ObjectId = ObjectId.get(),
    name: String,
    opensAt: Long,
    closesAt: Long,
    lectures: List[ObjectId] = List()
) extends BaseModel {

  def toSemesterView: SemesterView =
    SemesterView(
      _id,
      name,
      opensAt,
      closesAt,
      lectures
    )
}
