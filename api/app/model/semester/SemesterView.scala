package model.semester

import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._
import org.mongodb.scala.bson.annotations.BsonProperty
import org.bson.codecs.pojo.annotations.BsonId
import model.lecture.LectureView
import dao.mongoDB.SemesterDBController

object SemesterView {
  val semesterMongoDB = new SemesterDBController()

  implicit val writesFormat: OWrites[SemesterView] =
    (semesterView: SemesterView) =>
      Json.obj(
        "_id" -> semesterView._id,
        "name" -> semesterView.name,
        "opensAt" -> semesterView.opensAt,
        "closesAt" -> semesterView.closesAt,
        "lectures" -> semesterMongoDB
          .getRelatedLectures(semesterView.lectures)
          .get
          .map(lecture => lecture.toLectureView)
      )

  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val formatDefault: OFormat[SemesterView] =
    new OFormat[SemesterView] {
      override def reads(json: JsValue): JsResult[SemesterView] =
        Json.using[Json.WithDefaultValues].format[SemesterView].reads(json)

      override def writes(o: SemesterView): JsObject = writesFormat.writes(o)
    }
}

case class SemesterView(
    _id: ObjectId,
    name: String,
    opensAt: Long,
    closesAt: Long,
    lectures: List[ObjectId]
) extends BaseModel
