package model.settings

import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._

object Settings {
  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[Settings] =
    (globalSettings: Settings) =>
      Json.obj(
        "_id" -> globalSettings._id,
        "koanTime" -> globalSettings.koanTime,
        "codetaskTime" -> globalSettings.codetaskTime
      )
  implicit val formatDefault: OFormat[Settings] =
    new OFormat[Settings] {
      override def reads(json: JsValue): JsResult[Settings] =
        Json.using[Json.WithDefaultValues].format[Settings].reads(json)

      override def writes(o: Settings): JsObject = writesFormat.writes(o)
    }
}

case class Settings(
    _id: ObjectId = ObjectId.get(),
    koanTime: Int = 15,
    codetaskTime: Int = 30
) extends BaseModel
