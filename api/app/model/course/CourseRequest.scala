package model.course

import model.BaseModel
import play.api.libs.json._
import play.api.libs.functional.syntax._
import org.bson.types.ObjectId

object CourseRequest {
  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue = if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _ => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[CourseRequest] = (course: CourseRequest) => Json.obj(
    "lectures" -> course.lectures
  )
  implicit val formatDefault: OFormat[CourseRequest] = new OFormat[CourseRequest] {
    override def reads(json: JsValue): JsResult[CourseRequest] = Json.using[Json.WithDefaultValues].format[CourseRequest].reads(json)

    override def writes(o: CourseRequest): JsObject = writesFormat.writes(o)
  }
}
case class CourseRequest(lectures: List[ObjectId] = List()) extends BaseModel
