package model.course

import model.BaseModel
import model.course.subs.Chapter
import play.api.libs.json._

object CourseChapter {
  implicit val writesFormat: OWrites[CourseChapter] = (course: CourseChapter) =>
    Json.obj(
      "_id" -> course._id,
      "title" -> course.title,
      "chapter" -> course.chapter,
    )
  implicit val formatDefault: OFormat[CourseChapter] = new OFormat[CourseChapter] {
    override def reads(json: JsValue): JsResult[CourseChapter] =
      Json.using[Json.WithDefaultValues].format[CourseChapter].reads(json)

    override def writes(o: CourseChapter): JsObject = writesFormat.writes(o)
  }
}

case class CourseChapter(
    _id: String,
    title: String,
    chapter: Option[Chapter],
) extends BaseModel {}
