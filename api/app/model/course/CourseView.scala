package model.course

import model.BaseModel
import play.api.libs.json._
import model.course.subs.ChapterView
import model.course.subs.Chapter
import model.taskProgress.TaskProgress

object CourseView {
  implicit val writesFormat: OWrites[CourseView] = (course: CourseView) =>
    Json.obj(
      "_id" -> course._id,
      "title" -> course.title,
      "description" -> course.description,
      "chapters" -> course.chapters.map(_.toChapterView(course._id, course.taskProgresses)),
      "solvedPercentage" -> course.taskProgresses.filter(taskProgress => taskProgress.courseId == course._id)
          .count(_.solutions.forall(_.correctness))
          .toDouble / course.chapters.flatMap(_.tasks).length * 100
    )
  implicit val formatDefault: OFormat[CourseView] = new OFormat[CourseView] {
    override def reads(json: JsValue): JsResult[CourseView] =
      Json.using[Json.WithDefaultValues].format[CourseView].reads(json)

    override def writes(o: CourseView): JsObject = writesFormat.writes(o)
  }
}

case class CourseView(
    _id: String,
    title: String,
    description: String,
    chapters: List[Chapter],
    taskProgresses: List[TaskProgress]
) extends BaseModel {}
