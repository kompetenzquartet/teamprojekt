package model.course.subs

import play.api.libs.json._
import model.BaseModel
import model.taskProgress.TaskProgress

object Chapter {
  implicit val writesFormat: OWrites[Chapter] = (chapter: Chapter) =>
    Json.obj(
      "_id" -> chapter._id,
      "title" -> chapter.title,
      "tasks" -> chapter.tasks
    )
  implicit val formatDefault: OFormat[Chapter] = new OFormat[Chapter] {
    override def reads(json: JsValue): JsResult[Chapter] =
      Json.using[Json.WithDefaultValues].format[Chapter].reads(json)

    override def writes(o: Chapter): JsObject = writesFormat.writes(o)
  }
}

case class Chapter(_id: String, title: String, tasks: List[Task])
    extends BaseModel {
  def toChapterView(courseId: String, taskProgresses: List[TaskProgress]): ChapterView =
    ChapterView(
      _id,
      title,
      courseId,
      tasks,
      taskProgresses,
    )
}
