package model.course.subs

import model.BaseModel
import play.api.libs.json._
import org.bson.types.ObjectId

case class TaskIdentifier(
    _id: ObjectId = ObjectId.get(),
    courseId: String,
    chapterId: String,
    taskId: String
) extends BaseModel

object TaskIdentifier {
  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[TaskIdentifier] =
    (taskIdentifier: TaskIdentifier) =>
      Json.obj(
        "courseId" -> taskIdentifier.courseId,
        "chapterId" -> taskIdentifier.chapterId,
        "taskId" -> taskIdentifier.taskId
      )

  implicit val formatDefault: OFormat[TaskIdentifier] =
    new OFormat[TaskIdentifier] {
      override def reads(json: JsValue): JsResult[TaskIdentifier] =
        Json.using[Json.WithDefaultValues].format[TaskIdentifier].reads(json)

      override def writes(o: TaskIdentifier): JsObject = writesFormat.writes(o)
    }
}
