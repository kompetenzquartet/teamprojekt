package model.course.subs

import play.api.libs.json._
import model.BaseModel
import model.taskProgress.TaskProgress

object ChapterView {
  implicit val writesFormat: OWrites[ChapterView] = (chapter: ChapterView) =>
    Json.obj(
      "_id" -> chapter._id,
      "title" -> chapter.title,
      "solvedPercentage" -> chapter.taskProgresses.filter(taskProgress => taskProgress.courseId == chapter.courseId && taskProgress.chapterId == chapter._id)
        .count(_.solutions.forall(_.correctness))
        .toDouble / chapter.tasks.length * 100
    )
  implicit val formatDefault: OFormat[ChapterView] = new OFormat[ChapterView] {
    override def reads(json: JsValue): JsResult[ChapterView] =
      Json.using[Json.WithDefaultValues].format[ChapterView].reads(json)

    override def writes(o: ChapterView): JsObject = writesFormat.writes(o)
  }
}

case class ChapterView(
    _id: String,
    title: String,
    courseId: String,
    tasks: List[Task],
    taskProgresses: List[TaskProgress]
) extends BaseModel
