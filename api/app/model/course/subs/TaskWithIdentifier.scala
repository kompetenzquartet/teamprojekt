package model.course.subs

import play.api.libs.json._

case class TaskWithIdentifier(_id: String, tag: String, data: TaskData, courseId: String, chapterId: String)

object TaskWithIdentifier {
  implicit val formatDefault: OFormat[TaskWithIdentifier] =
    new OFormat[TaskWithIdentifier] {
      override def reads(json: JsValue): JsResult[TaskWithIdentifier] =
        Json
          .using[Json.WithDefaultValues]
          .format[TaskWithIdentifier]
          .reads(json)

      override def writes(o: TaskWithIdentifier): JsObject =
        Json.using[Json.WithDefaultValues].format[TaskWithIdentifier].writes(o)
    }
}
