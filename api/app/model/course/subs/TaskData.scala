package model.course.subs

import play.api.libs.json._

case class TaskData(
    code: String = "",
    description: String = "",
    mode: String = "",
    codeSolution: String = "",
    solutions: List[Solution] = List(),
    url: String = "",
    taskTime: Int = 0
)

object TaskData {
  implicit val writesFormat: OWrites[TaskData] = (taskData: TaskData) =>
    Json.obj(
      "code" -> taskData.code,
      "description" -> taskData.description,
      "mode" -> taskData.mode,
      "solutions" -> taskData.solutions,
      "url" -> taskData.url,
      "taskTime" -> taskData.taskTime
    )
  implicit val formatDefault: OFormat[TaskData] = new OFormat[TaskData] {
    override def reads(json: JsValue): JsResult[TaskData] =
      Json.using[Json.WithDefaultValues].format[TaskData].reads(json)

    override def writes(o: TaskData): JsObject = writesFormat.writes(o)
  }
}
