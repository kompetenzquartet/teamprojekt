package model.course.subs

import play.api.libs.json._

case class Task(_id: String, tag: String, data: TaskData)

object Task {
  implicit val formatDefault: OFormat[Task] =
    new OFormat[Task] {
      override def reads(json: JsValue): JsResult[Task] =
        Json
          .using[Json.WithDefaultValues]
          .format[Task]
          .reads(json)

      override def writes(o: Task): JsObject =
        Json.using[Json.WithDefaultValues].format[Task].writes(o)
    }
}
