package model.course.subs

import play.api.libs.json._

case class Solution(
    _id: String,
    solution: String = "",
    correctness: Boolean = true
)

object Solution {
  implicit val writesFormat: OWrites[Solution] = (taskData: Solution) =>
    Json.obj(
      "_id" -> taskData._id,
      "solution" -> taskData.solution,
      "correctness" -> taskData.correctness
    )

  implicit val formatDefault: OFormat[Solution] = new OFormat[Solution] {
    override def reads(json: JsValue): JsResult[Solution] =
      Json.using[Json.WithDefaultValues].format[Solution].reads(json)

    override def writes(o: Solution): JsObject = writesFormat.writes(o)
  }
}
