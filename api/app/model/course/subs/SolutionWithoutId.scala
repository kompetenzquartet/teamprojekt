package model.course.subs

import play.api.libs.json._

case class SolutionWithoutId(
  solution: String = "",
  correctness: Boolean = true
)

object SolutionWithoutId {
  implicit val writesFormat: OWrites[SolutionWithoutId] = (taskData: SolutionWithoutId) =>
    Json.obj(
      "solution" -> taskData.solution,
      "correctness" -> taskData.correctness
    )

  implicit val formatDefault: OFormat[SolutionWithoutId] = new OFormat[SolutionWithoutId] {
    override def reads(json: JsValue): JsResult[SolutionWithoutId] =
      Json.using[Json.WithDefaultValues].format[SolutionWithoutId].reads(json)

    override def writes(o: SolutionWithoutId): JsObject = writesFormat.writes(o)
  }
}
