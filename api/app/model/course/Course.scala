package model.course

import model.BaseModel
import model.course.subs.Chapter
import org.bson.types.ObjectId
import play.api.libs.json._
import org.mongodb.scala.bson.annotations.BsonProperty
import org.bson.codecs.pojo.annotations.BsonId
import play.api.libs.functional.syntax._
import model.course.subs.ChapterView
import model.taskProgress.TaskProgress

object Course {
  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[Course] = (course: Course) =>
    Json.obj(
      "_id" -> course._id,
      "title" -> course.title,
      "description" -> course.description,
      "chapters" -> course.chapters,
      "lectures" -> course.lectures
    )
  implicit val formatDefault: OFormat[Course] = new OFormat[Course] {
    override def reads(json: JsValue): JsResult[Course] =
      Json.using[Json.WithDefaultValues].format[Course].reads(json)

    override def writes(o: Course): JsObject = writesFormat.writes(o)
  }
}

case class Course(
    _id: String,
    title: String,
    description: String,
    chapters: List[Chapter],
    lectures: List[ObjectId] = List()
) extends BaseModel {
  def toCourseView(taskProgresses: List[TaskProgress]): CourseView =
    CourseView(_id, title, description, chapters, taskProgresses)
}
