package model.course

import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._

case class CourseDeadline(_id: ObjectId = ObjectId.get(),
                          semester: String,
                          lecture: String,
                          course: String,
                          deadline: Long
                         ) extends BaseModel {}

object CourseDeadline {
  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[CourseDeadline] = (courseDeadline: CourseDeadline) => Json.obj(
    "_id" -> courseDeadline._id,
    "semester" -> courseDeadline.semester,
    "lecture" -> courseDeadline.lecture,
    "course" -> courseDeadline.course,
    "deadline" -> courseDeadline.deadline
  )

  implicit val formatDefault: OFormat[CourseDeadline] = new OFormat[CourseDeadline] {
    override def reads(json: JsValue): JsResult[CourseDeadline] = Json.using[Json.WithDefaultValues].format[CourseDeadline].reads(json)

    override def writes(o: CourseDeadline): JsObject = writesFormat.writes(o)
  }
}
