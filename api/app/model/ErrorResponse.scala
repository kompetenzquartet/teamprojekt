package model

import play.api.libs.json.Json

case class ErrorResponse(message: String) {}

object ErrorResponse {
  implicit val errorResponseJson = Json.format[ErrorResponse]
}
