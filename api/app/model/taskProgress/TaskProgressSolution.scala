package model.taskProgress

import model.BaseModel
import play.api.libs.json._

object TaskProgressSolution {
  implicit val formatDefault: OFormat[TaskProgressSolution] =
    new OFormat[TaskProgressSolution] {
      override def reads(json: JsValue): JsResult[TaskProgressSolution] =
        Json.using[Json.WithDefaultValues].format[TaskProgressSolution].reads(json)

      override def writes(o: TaskProgressSolution): JsObject =
        Json.using[Json.WithDefaultValues].format[TaskProgressSolution].writes(o)
    }
}

case class TaskProgressSolution(correctness: Boolean, solution: String) extends BaseModel
