package model.taskProgress

import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._

object TaskProgressRequest {
  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val formatDefault: OFormat[TaskProgressRequest] =
    new OFormat[TaskProgressRequest] {
      override def reads(json: JsValue): JsResult[TaskProgressRequest] =
        Json
          .using[Json.WithDefaultValues]
          .format[TaskProgressRequest]
          .reads(json)

      override def writes(o: TaskProgressRequest): JsObject =
        Json.using[Json.WithDefaultValues].format[TaskProgressRequest].writes(o)
    }
}

case class TaskProgressRequest(
    _id: ObjectId = ObjectId.get(),
    userId: ObjectId,
    semesterId: ObjectId,
    lectureId: ObjectId,
    courseId: String,
    chapterId: String,
    taskId: String,
    solutions: List[String],
    cheatedSolutions: List[Boolean]
) extends BaseModel {
  def toTaskProgress(
      taskTag: String,
      taskCode: String,
      solutions: List[TaskProgressSolution]
  ): TaskProgress =
    TaskProgress(
      _id,
      userId,
      semesterId,
      lectureId,
      courseId,
      chapterId,
      taskId,
      taskTag,
      taskCode,
      solutions,
      cheatedSolutions = cheatedSolutions
    )
}
