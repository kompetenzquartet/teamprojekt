package model.taskProgress

import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._

object TaskProgress {
  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val formatDefault: OFormat[TaskProgress] =
    new OFormat[TaskProgress] {
      override def reads(json: JsValue): JsResult[TaskProgress] =
        Json.using[Json.WithDefaultValues].format[TaskProgress].reads(json)

      override def writes(o: TaskProgress): JsObject =
        Json.using[Json.WithDefaultValues].format[TaskProgress].writes(o)
    }
}

case class TaskProgress(
    _id: ObjectId = ObjectId.get(),
    userId: ObjectId,
    semesterId: ObjectId,
    lectureId: ObjectId,
    courseId: String,
    chapterId: String,
    taskId: String,
    taskTag: String,
    taskCode: String,
    solutions: List[TaskProgressSolution],
    cheatedSolutions: List[Boolean]
) extends BaseModel
