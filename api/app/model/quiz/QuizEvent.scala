package model.quiz

import model.course.subs.Task
import play.api.libs.json.{Json, OWrites}

import scala.swing.event.Event
import model.course.subs.TaskWithIdentifier

trait QuizEvent extends Event {}

object QuizEvent {
  def getScores(participants: List[QuizDuelUser]) = participants
    .map(participant =>
      Json.obj(
        "username" -> participant.username,
        "score" -> participant.scores
          .map(score => score.score)
          .foldLeft(0D)(_ + _)
      )
    )
    .toList
}

case class FinishEvent(participants: List[QuizDuelUser]) extends QuizEvent {}

object FinishEvent {
  implicit val writesFormat: OWrites[FinishEvent] =
    (finishEvent: FinishEvent) =>
      Json.obj(
        "type" -> "finish",
        "msg" -> Json.obj(
          "scores" -> QuizEvent.getScores(finishEvent.participants)
        )
      )
}

case class TaskEvent(task: TaskWithIdentifier, duration: Long) extends Event {}

object TaskEvent {
  implicit val writesFormat: OWrites[TaskEvent] = (taskEvent: TaskEvent) =>
    Json.obj(
      "type" -> "task",
      "msg" -> Json.obj(
        "task" -> taskEvent.task,
        "duration" -> taskEvent.duration
      )
    )
}

case class ScoreboardEvent(participants: List[QuizDuelUser], task: Option[TaskWithIdentifier]) extends Event {}

object ScoreboardEvent {
  implicit val writesFormat: OWrites[ScoreboardEvent] =
    (scoreboardEvent: ScoreboardEvent) =>
      Json.obj(
        "type" -> "scoreboard",
        "msg" -> Json.obj(
          "scores" -> QuizEvent.getScores(scoreboardEvent.participants),
          "task" -> scoreboardEvent.task.getOrElse(null)
        )
      )
}
