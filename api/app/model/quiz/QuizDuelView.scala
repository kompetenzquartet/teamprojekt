package model.quiz

import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._

case class QuizDuelView(
    _id: ObjectId = ObjectId.get(),
    lecture: String,
    semester: String,
    quizTaskSet: String,
    participants: List[QuizDuelUser] = List(),
    remainingTime: Long = 0,
    isRunning: Boolean = false
) extends BaseModel

object QuizDuelView {
  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val formatDefault: OFormat[QuizDuelView] =
    new OFormat[QuizDuelView] {
      override def reads(json: JsValue): JsResult[QuizDuelView] =
        Json
          .using[Json.WithDefaultValues]
          .format[QuizDuelView]
          .reads(json)

      override def writes(o: QuizDuelView): JsObject =
        Json.using[Json.WithDefaultValues].format[QuizDuelView].writes(o)
    }
}
