package model.quiz

import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._
import org.mongodb.scala.bson.annotations.BsonProperty
import org.bson.codecs.pojo.annotations.BsonId
import play.api.libs.functional.syntax._

case class QuizDuel(
    _id: ObjectId = ObjectId.get(),
    lectureId: ObjectId,
    semesterId: ObjectId,
    quizTaskSetId: ObjectId,
    participants: List[QuizDuelUser] = List()
) extends BaseModel

object QuizDuel {

  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[QuizDuel] =
    (quizDuel: QuizDuel) =>
      Json.obj(
        "_id" -> quizDuel._id,
        "lectureId" -> quizDuel.lectureId,
        "semesterId" -> quizDuel.semesterId,
        "quizTaskSetId" -> quizDuel.quizTaskSetId,
        "participants" -> quizDuel.participants
      )

  implicit val formatDefault: OFormat[QuizDuel] =
    new OFormat[QuizDuel] {
      override def reads(json: JsValue): JsResult[QuizDuel] =
        Json
          .using[Json.WithDefaultValues]
          .format[QuizDuel]
          .reads(json)

      override def writes(o: QuizDuel): JsObject = writesFormat.writes(o)
    }
}
