package model.quiz

import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._

case class QuizDuelDeadline(_id: ObjectId = ObjectId.get(),
                            lecture: String,
                            semester: String,
                            taskSet: String,
                            deadline: Long
                            ) extends BaseModel {}

object QuizDuelDeadline {
  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[QuizDuelDeadline] = (quizDuelDeadline: QuizDuelDeadline) => Json.obj(
    "_id" -> quizDuelDeadline._id,
    "lecture" -> quizDuelDeadline.lecture,
    "semester"-> quizDuelDeadline.semester,
    "taskSet" -> quizDuelDeadline.taskSet,
    "deadline" -> quizDuelDeadline.deadline
  )

  implicit val formatDefault: OFormat[QuizDuelDeadline] = new OFormat[QuizDuelDeadline] {
    override def reads(json: JsValue): JsResult[QuizDuelDeadline] = Json.using[Json.WithDefaultValues].format[QuizDuelDeadline].reads(json)

    override def writes(o: QuizDuelDeadline): JsObject = writesFormat.writes(o)
  }
}