package model.quiz

import model.BaseModel
import play.api.libs.json._
import model.course.subs.TaskIdentifier

case class QuizDuelSubmitMessage(
    task: TaskIdentifier,
    solutions: List[String],
) extends BaseModel

object QuizDuelSubmitMessage {
  implicit val formatDefault: OFormat[QuizDuelSubmitMessage] =
    new OFormat[QuizDuelSubmitMessage] {
      override def reads(json: JsValue): JsResult[QuizDuelSubmitMessage] =
        Json
          .using[Json.WithDefaultValues]
          .format[QuizDuelSubmitMessage]
          .reads(json)

      override def writes(o: QuizDuelSubmitMessage): JsObject =
        Json.using[Json.WithDefaultValues].format[QuizDuelSubmitMessage].writes(o)
    }
}
