package model.quiz

import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._
import model.course.subs.TaskIdentifier

case class QuizDuelUserScore(
    task: TaskIdentifier,
    var score: Double = 0
) extends BaseModel

object QuizDuelUserScore {

  implicit val formatDefault: OFormat[QuizDuelUserScore] =
    new OFormat[QuizDuelUserScore] {
      override def reads(json: JsValue): JsResult[QuizDuelUserScore] =
        Json
          .using[Json.WithDefaultValues]
          .format[QuizDuelUserScore]
          .reads(json)

      override def writes(o: QuizDuelUserScore): JsObject =
        Json.using[Json.WithDefaultValues].format[QuizDuelUserScore].writes(o)
    }
}
