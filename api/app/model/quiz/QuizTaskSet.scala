package model.quiz

import model.BaseModel
import model.course.subs.TaskIdentifier
import org.bson.types.ObjectId
import play.api.libs.json._

case class QuizTaskSet(
    _id: ObjectId = ObjectId.get(),
    name: String,
    tasks: List[TaskIdentifier]
) extends BaseModel

object QuizTaskSet {

  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[QuizTaskSet] =
    (quizTaskSet: QuizTaskSet) =>
      Json.obj(
        "_id" -> quizTaskSet._id,
        "name" -> quizTaskSet.name,
        "tasks" -> quizTaskSet.tasks
      )

  implicit val formatDefault: OFormat[QuizTaskSet] = new OFormat[QuizTaskSet] {
    override def reads(json: JsValue): JsResult[QuizTaskSet] =
      Json.using[Json.WithDefaultValues].format[QuizTaskSet].reads(json)

    override def writes(o: QuizTaskSet): JsObject = writesFormat.writes(o)
  }
}
