package model.quiz

import model.BaseModel
import model.course.subs.Task
import org.bson.types.ObjectId
import play.api.libs.json.{JsObject, JsResult, JsValue, Json, OFormat, OWrites}

case class Quizduell(lecture: String,
                     semester: String,
                     quiztaskSet: String,
                     tasks: List[Task],
                     creationDate: Long,
                     var quizduellUsers: List[QuizDuelUser],
                     _id: String = ObjectId.get().toString
                    ) extends BaseModel

object Quizduell {

  implicit val writesFormat: OWrites[Quizduell] = (quizduell: Quizduell) => Json.obj(
    "_id" -> quizduell._id,
    "lecture" -> quizduell.lecture,
    "semester" -> quizduell.semester,
    "quiztaskSet" -> quizduell.quiztaskSet,
    "tasks" -> quizduell.tasks,
    "creationDate" -> quizduell.creationDate,
    "quizduellUsers" -> quizduell.quizduellUsers
  )

  implicit val formatDefault: OFormat[Quizduell] = new OFormat[Quizduell] {
    override def reads(json: JsValue): JsResult[Quizduell] = Json.using[Json.WithDefaultValues].format[Quizduell].reads(json)

    override def writes(o: Quizduell): JsObject = writesFormat.writes(o)
  }
}


