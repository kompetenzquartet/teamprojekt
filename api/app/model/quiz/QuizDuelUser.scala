package model.quiz

import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._

case class QuizDuelUser(
    _id: ObjectId = ObjectId.get(),
    userId: ObjectId,
    username: String,
    scores: List[QuizDuelUserScore] = List(),
) extends BaseModel

object QuizDuelUser {

  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  implicit val formatDefault: OFormat[QuizDuelUser] =
    new OFormat[QuizDuelUser] {
      override def reads(json: JsValue): JsResult[QuizDuelUser] =
        Json
          .using[Json.WithDefaultValues]
          .format[QuizDuelUser]
          .reads(json)

      override def writes(o: QuizDuelUser): JsObject =
        Json.using[Json.WithDefaultValues].format[QuizDuelUser].writes(o)
    }
}
