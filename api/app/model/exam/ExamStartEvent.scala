package model.exam

import play.api.libs.json.{Json, OFormat}

case class ExamStartEvent(examId: String)

object ExamStartEvent {
  implicit val format: OFormat[ExamStartEvent] = Json.format[ExamStartEvent]
}
