package model.exam

import model.BaseModel
import model.course.subs.Solution
import org.bson.types.ObjectId
import play.api.libs.json._


object ExamSolution {
  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull

    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _ => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[ExamSolution] = (userExamSolution: ExamSolution) =>
    Json.obj(
      "_id" -> userExamSolution._id,
      "userId" -> userExamSolution.userId,
      "examId" -> userExamSolution.examId,
      "examTaskId" -> userExamSolution.examTaskId,
      "solvedWith" -> userExamSolution.solvedWith,
      "points" -> userExamSolution.points
  )
  implicit val formatDefault: OFormat[ExamSolution] = new OFormat[ExamSolution] {
    override def reads(json: JsValue): JsResult[ExamSolution] =
      Json.using[Json.WithDefaultValues].format[ExamSolution].reads(json)

    override def writes(o: ExamSolution): JsObject = writesFormat.writes(o)
  }
}

case class ExamSolution(
                      _id: String = ObjectId.get().toString,
                      userId: String,
                      examId: String,
                      examTaskId: String,
                      solvedWith: List[Solution] = List(),
                      points: Double = 0.0
                      ) extends BaseModel
