package model.exam.request

import play.api.libs.json._

object AssignTaskRequest {
  implicit val writesFormat: OWrites[AssignTaskRequest] = (assignTaskRequest: AssignTaskRequest) =>
    Json.obj(
      "courseId" -> assignTaskRequest.courseId,
      "chapterId" -> assignTaskRequest.chapterId
    )
  implicit val formatDefault: OFormat[AssignTaskRequest] = new OFormat[AssignTaskRequest] {
    override def reads(json: JsValue): JsResult[AssignTaskRequest] =
      Json.using[Json.WithDefaultValues].format[AssignTaskRequest].reads(json)

    override def writes(o: AssignTaskRequest): JsObject = writesFormat.writes(o)
  }
}

case class AssignTaskRequest(courseId: String ,chapterId: String)

