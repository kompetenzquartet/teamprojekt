package model.exam

import play.api.libs.json._

object GradeThresholds {
  implicit val writesFormat: OWrites[GradeThresholds] = (gradeThresholds: GradeThresholds) =>
    Json.obj(
      "top" -> gradeThresholds.top,
      "bottom" -> gradeThresholds.bottom
    )

  implicit val formatDefault: OFormat[GradeThresholds] = new OFormat[GradeThresholds] {
    override def reads(json: JsValue): JsResult[GradeThresholds] =
      Json.using[Json.WithDefaultValues].format[GradeThresholds].reads(json)

    override def writes(o: GradeThresholds): JsObject = writesFormat.writes(o)
  }
}

/**
 * This class represents the grade thresholds for a specific exam.
 *
 * @param top    The top threshold represents the percentage of points
 *               a student needs to get 1.0 as grade.
 * @param bottom The bottom threshold represents the percentage of points
 *               a student needs to get 4.0 as grade.
 */
case class GradeThresholds(top: Double, bottom: Double)