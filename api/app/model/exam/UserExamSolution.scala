package model.exam

import play.api.libs.json._

object UserExamSolution {
  implicit val writesFormat: OWrites[UserExamSolution] = (userExamSolution: UserExamSolution) =>
    Json.obj(
      "userId" -> userExamSolution.userId,
      "userName" -> userExamSolution.userName,
      "studentNumber" -> userExamSolution.studentNumber,
      "examSolutions" -> userExamSolution.examSolutions,
      "mark" -> userExamSolution.mark
    )
  implicit val formatDefault: OFormat[UserExamSolution] = new OFormat[UserExamSolution] {
    override def reads(json: JsValue): JsResult[UserExamSolution] =
      Json.using[Json.WithDefaultValues].format[UserExamSolution].reads(json)

    override def writes(o: UserExamSolution): JsObject = writesFormat.writes(o)
  }
}


case class UserExamSolution(
                           userId: String,
                           userName: String,
                           studentNumber: String,
                           examSolutions: List[ExamSolution],
                           mark: String
                           )
