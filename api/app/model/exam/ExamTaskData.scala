package model.exam

import model.course.subs.Solution
import play.api.libs.json._

case class ExamTaskData(code: String = "",
                    description: String = "",
                    mode: String = "",
                    codeSolution: String = "",
                    solutions: List[Solution] = List(),
                    maxPoints: Double = 0.0
                   )

object ExamTaskData {
  implicit val writesFormat: OWrites[ExamTaskData] = (taskData: ExamTaskData) => Json.obj(
    "code" -> taskData.code,
    "description" -> taskData.description,
    "mode" -> taskData.mode,
    "solutions" -> taskData.solutions,
    "maxPoints" -> taskData.maxPoints
  )
  implicit val formatDefault: OFormat[ExamTaskData] = new OFormat[ExamTaskData] {
    override def reads(json: JsValue): JsResult[ExamTaskData] = Json.using[Json.WithDefaultValues].format[ExamTaskData].reads(json)

    override def writes(o: ExamTaskData): JsObject = writesFormat.writes(o)
  }
}


