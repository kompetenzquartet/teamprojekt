package model.exam

import play.api.libs.json._

case class ExamTask(_id: String,
                tag: String,
                data: ExamTaskData
               )

object ExamTask {
  implicit val writesFormat: OWrites[ExamTask] = (task: ExamTask) => Json.obj(
    "_id" -> task._id,
    "tag" -> task.tag,
    "data" -> task.data
  )
  implicit val formatDefault: OFormat[ExamTask] = new OFormat[ExamTask] {
    override def reads(json: JsValue): JsResult[ExamTask] =
      Json.using[Json.WithDefaultValues].format[ExamTask].reads(json)

    override def writes(o: ExamTask): JsObject = writesFormat.writes(o)
  }
}


