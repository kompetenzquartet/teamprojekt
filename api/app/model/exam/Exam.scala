package model.exam

import model.BaseModel
import org.bson.types.ObjectId
import play.api.libs.json._

object Exam {
  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull

    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _ => JsError("Invalid ObjectId")
    }
  }

  implicit val writesFormat: OWrites[Exam] = (exam: Exam) =>
    Json.obj(
      "_id" -> exam._id,
      "name" -> exam.name,
      "lecture" -> exam.lecture,
      "semester" -> exam.semester,
      "duration" -> exam.duration,
      "date" -> exam.date,
      "isOpen" -> exam.isOpen,
      "tasks" -> exam.tasks,
      "examType" -> exam.examType,
      "startTime" -> exam.startTime,
      "endTime" -> exam.endTime,
      "password" -> exam.password,
      "gradeThresholds" -> exam.gradeThresholds
    )
  implicit val formatDefault: OFormat[Exam] = new OFormat[Exam] {
    override def reads(json: JsValue): JsResult[Exam] =
      Json.using[Json.WithDefaultValues].format[Exam].reads(json)

    override def writes(o: Exam): JsObject = writesFormat.writes(o)
  }
}

case class Exam(
                 _id: ObjectId = ObjectId.get(),
                 name: String,
                 date: Long,
                 duration: Int,
                 lecture: String,
                 semester: String,
                 isOpen: Boolean,
                 tasks: List[ExamTask] = List(),
                 examType: String,
                 startTime: Long,
                 endTime: Long,
                 password: String,
                 gradeThresholds: GradeThresholds
               ) extends BaseModel