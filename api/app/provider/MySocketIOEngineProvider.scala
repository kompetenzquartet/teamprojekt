package provider

import akka.NotUsed
import akka.stream.Materializer
import akka.stream.scaladsl.{BroadcastHub, Flow, Keep, MergeHub}
import javax.inject.{Inject, Provider, Singleton}
import play.engineio.EngineIOController
import play.socketio.SocketIOSession
import play.socketio.scaladsl.SocketIO
import play.socketio.scaladsl.SocketIOEventCodec._

@Singleton
class MySocketIOEngineProvider @Inject()(socketIO: SocketIO)(implicit mat: Materializer)
  extends Provider[EngineIOController] {

  val decoder = decodeByName {
    case "send_chat" => decodeJson[String]
  }

  val encoder = encodeByType[Map[String, String]] {
    case _: Map[String, String] => "send_chat" -> encodeJson[Map[String, String]]
  }

  def chatFlow(session: SocketIOSession[String]): Flow[String, Map[String, String], NotUsed] = {
    val (sink, source) = MergeHub.source[String].toMat(BroadcastHub.sink)(Keep.both).run()
    Flow.fromSinkAndSourceCoupled(sink, source.map((message) => Map(
      "message" -> message,
      "sender" -> session.sid
    )))
  }

  override def get(): EngineIOController = socketIO.builder
    .onConnect { (request, session) =>
      // TODO: Validate token
      session
    }
    .defaultNamespace(decoder, encoder) {
      case (session) => chatFlow(session)
    }
    .createController()
}
