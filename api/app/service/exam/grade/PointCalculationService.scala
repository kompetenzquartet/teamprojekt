package service.exam.grade

import model.exam.{ExamSolution, ExamTask}

trait PointCalculationService {
  /**
   * Calculate the points for a given task and solution
   *
   * @param task     the task to calculate the points for
   * @param solution the solution to calculate the points for
   * @return the points for the given task and solution
   */
  def calculatePoints(task: ExamTask, solution: ExamSolution): Double
}
