package service.exam.grade

import model.exam.{Exam, ExamSolution}

class ExamMarkCalculator extends MarkCalculator {

  override def calculateMark(exam: Exam, solutions: List[ExamSolution]): String = {
    if (solutions.map(solutions => solutions.points).contains(-1.0)) {
      return "N/A"
    }

    val percentage = pointsToPercentage(exam, solutions)
    val top = exam.gradeThresholds.top
    val bottom = exam.gradeThresholds.bottom

    val step = (top - bottom) / 10

    percentage match {
      case Some(percentage) => calculateMarkHelper(percentage, top, bottom, step)
      case None => "5.0"
    }

  }

  private def pointsToPercentage(exam: Exam, solutions: List[ExamSolution]): Option[Double] = {
    val reachedPoints: Double = solutions.map(solution => solution.points).filter(_ > 0).sum

    val reachablePoints: Double = exam.tasks.map(task => task.data.maxPoints).sum

    if (reachablePoints == 0) {
      None
    } else
      Some(reachedPoints / reachablePoints)

  }

  private def calculateMarkHelper(percentage: Double, top: Double, bottom: Double, step: Double): String = {
    if (percentage >= top)
      "1.0"
    else if (percentage >= top - 1 * step)
      "1.3"
    else if (percentage >= top - 2 * step)
      "1.7"
    else if (percentage >= top - 3 * step)
      "2.0"
    else if (percentage >= top - 4 * step)
      "2.3"
    else if (percentage >= top - 5 * step)
      "2.7"
    else if (percentage >= top - 6 * step)
      "3.0"
    else if (percentage >= top - 7 * step)
      "3.3"
    else if (percentage >= top - 8 * step)
      "3.7"
    else if (percentage >= top - 9 * step)
      "4.0"
    else if (percentage < bottom)
      "5.0"
    else
      "5.0"
  }


}
