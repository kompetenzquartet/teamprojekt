package service.exam.grade

import com.google.inject.Inject
import integration.checkapi.CheckApi
import model.exam.{ExamSolution, ExamTask}

class ExamSolutionPointsCalculationService @Inject()(
                                                      checkApi: CheckApi
                                                    ) extends PointCalculationService {

  override def calculatePoints(task: ExamTask, solution: ExamSolution): Double = {
    roundToHalfStep(calculatePointsHelper(task, solution))
  }

  private def calculatePointsHelper(task: ExamTask, solution: ExamSolution): Double = {
    task.tag match {
      case "koan-task" => calculateKoanPoints(task, solution)
      case "multiple-choice-task" => calculateMultipleChoicePoints(task, solution)
      case "single-choice-task" => calculateSingleChoicePoints(task, solution)
      case "code-task" => calculateCodeTask(task, solution)
      case "info-task" => task.data.maxPoints
      case "reflection-task" => -1.0
      case _ => -1.0
    }
  }

  private def calculateSingleChoicePoints(task: ExamTask, solution: ExamSolution): Double = {
    solution.solvedWith.map { userSolution =>
      task.data.solutions.
        find(taskSolution => taskSolution._id == userSolution._id)
        .filter(taskSolution => taskSolution.correctness == userSolution.correctness && taskSolution.correctness)
        .map(_ => task.data.maxPoints)
        .getOrElse(0.0)
    }.sum.max(0.0)
  }

  private def calculateMultipleChoicePoints(task: ExamTask, solution: ExamSolution): Double = {
    val maxPoints = task.data.maxPoints
    val correctAnswersCount = task.data.solutions.count(_.correctness)
    val pointsPerCorrectAnswer = if (correctAnswersCount > 0) maxPoints / correctAnswersCount else 0.0

    solution.solvedWith.map { userSolution =>
      task.data.solutions
        .find(taskSolution => taskSolution._id == userSolution._id)
        .map { taskSolution =>
          if (taskSolution.correctness == userSolution.correctness && taskSolution.correctness) pointsPerCorrectAnswer
          else if (userSolution.correctness && !taskSolution.correctness) -pointsPerCorrectAnswer
          else 0.0
        }.getOrElse(0.0)
    }.sum.max(0.0)
  }

  private def calculateKoanPoints(task: ExamTask, solution: ExamSolution): Double = {
    val totalSolutions = task.data.solutions.length
    if (totalSolutions == 0) return 0.0

    val pointsPerCorrectAnswer = task.data.maxPoints / totalSolutions

    val correctAnswersCount = solution.solvedWith.distinct.count { userSolution =>
      task.data.solutions
        .find(taskSolution => taskSolution._id == userSolution._id)
        .exists(taskSolution =>
          taskSolution.solution.replaceAll(" ", "") == userSolution.solution.replaceAll(" ", "")
        )
    }

    (correctAnswersCount * pointsPerCorrectAnswer).min(task.data.maxPoints)
  }

  private def calculateCodeTask(task: ExamTask, solution: ExamSolution): Double = {
    checkApi.checkCodeTask(task.data.code, solution.solvedWith.head.solution)
      .map(isCorrect => if (isCorrect) task.data.maxPoints else 0.0)
      .getOrElse(0.0)
  }

  private def roundToHalfStep(value: Double): Double = {
    (Math.round(value * 2) / 2.0)
  }

}
