package service.exam.grade

import model.exam.{Exam, ExamSolution}

trait MarkCalculator {

  /**
   * Calculate mark for given task and solutions
   *
   * @param exam      exam to calculate mark for
   * @param solutions users solutions to calculate mark
   * @return mark for given task and solutions
   */
  def calculateMark(exam: Exam, solutions: List[ExamSolution]): String
}
