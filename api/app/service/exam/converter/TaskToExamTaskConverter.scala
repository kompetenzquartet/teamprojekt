package service.exam.converter

import model.course.subs.Task
import model.exam.ExamTask

trait TaskToExamTaskConverter {
  /**
   * Converts a list of tasks to a list of exam tasks
   *
   * @param taskList the list of tasks to convert
   * @return the list of exam tasks
   */
  def convertList(taskList: List[Task]): List[ExamTask]

  /**
   * Converts a task to an exam task
   *
   * @param task the task to convert
   * @return the exam task
   */
  def convertTask(task: Task): ExamTask
}
