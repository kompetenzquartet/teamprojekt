package service.exam.converter

import model.course.subs.Task
import model.exam.{ExamTask, ExamTaskData}

class TaskToExamTaskConverterImpl extends TaskToExamTaskConverter {


  override def convertList(taskList: List[Task]): List[ExamTask] = {
    taskList
      .filter(task => task.tag != "video-task")
      .map(convertTask)
      .zipWithIndex
      .map {
        case (task, index) => ExamTask((index + 1).toString, task.tag, task.data)
      }
  }

  override def convertTask(task: Task): ExamTask = {
    ExamTask(task._id, task.tag,
      ExamTaskData(
        task.data.code,
        task.data.description,
        task.data.mode,
        task.data.codeSolution,
        task.data.solutions,
        getPoints(task)
      ))
  }
  private def getPoints(task: Task): Double = {
    task.tag match {
      case "koan-task" => task.data.solutions.length
      case "info-task" => 0.0
      case _ => - 1.0
    }
  }
}
