package service

import java.util.concurrent.TimeUnit
import scala.util.control.Breaks._

import akka.actor.{ActorSystem, Scheduler}
import controllers.impl.QuizDuelController
import model.course.subs.TaskIdentifier
import model.quiz._
import model.settings.Settings
import model.user.User
import play.api.Logger

import scala.concurrent.ExecutionContextExecutor
import org.bson.types.ObjectId
import model.course.subs.TaskWithIdentifier
import play.api.libs.json.Json
import java.util.concurrent.atomic.AtomicBoolean

case class QuizDuelService(
    controller: QuizDuelController,
    quizDuelId: ObjectId,
    tasks: List[TaskWithIdentifier],
    koanTaskTime: Long,
    codeTaskTime: Long
) {
  val actorSystem: ActorSystem = ActorSystem()
  val scheduler: Scheduler = actorSystem.scheduler
  implicit val executor: ExecutionContextExecutor = actorSystem.dispatcher

  var quizduell: Option[QuizDuel] = Option.empty
  var participants: List[QuizDuelUser] = List()

  val LOCK = new AnyRef
  val shouldStop = new AtomicBoolean(false)

  var currentTask: Option[TaskWithIdentifier] = Option.empty

  var currentTaskStartTime: Long = System.currentTimeMillis
  var currentTaskDuration: Long = 5

  def timeScore = Math.round(Math.log(
    TimeUnit.SECONDS.toMillis(
      currentTaskDuration
    ) - (System.currentTimeMillis() - currentTaskStartTime)
  ))

  def remainingTime = currentTaskDuration -
    TimeUnit.MILLISECONDS.toSeconds(
      System.currentTimeMillis() - currentTaskStartTime
    )

  def execute() = {
    pushAllTasks
    finishExecution
  }

  def stop = {
    shouldStop.compareAndSet(false, true)
    skipTask
  }

  private def pushAllTasks() = LOCK synchronized {
    breakable {
      for (task <- tasks) {
        logger.warn("pushing task")
        logger.warn(task.data.description)
        setTaskDuration(task)
        currentTask = Some(task)
        pushTask(task)
        LOCK.wait(TimeUnit.SECONDS.toMillis(currentTaskDuration))
        if (shouldStop.get) break
      }
    }
  }

  private def setTaskDuration(task: TaskWithIdentifier) = {
    currentTaskDuration = if (task.data.taskTime == 0) {
      if (task.tag == "koan-task") {
        koanTaskTime
      } else if (task.tag == "code-task") {
        codeTaskTime
      } else {
        0
      }
    } else {
      task.data.taskTime
    }
  }

  val logger: Logger = Logger(this.getClass)

  def finishExecution() = {
    controller.publish(FinishEvent(participants))
    if (!shouldStop.get) {
      controller.stopQuizDuel(quizDuelId.toString)
    }
  }

  def submitSolution(user: User, task: TaskIdentifier, score: Double) = {
    withPublishScoreboard {
      participants = participants.map(participant => {
        if (participant.userId == user._id) {
          val participantScore = participant.scores
            .find(score => score.task._id == task._id)

          if (participantScore.nonEmpty) {
            participant.copy(scores =
              participant.scores.map(pScore =>
                if (pScore.task._id == task._id)
                  pScore.copy(task, pScore.score + score)
                else pScore
              )
            )
          } else
            participant.copy(scores =
              participant.scores :+ QuizDuelUserScore(task, score)
            )
        } else {
          participant
        }
      })

    }
  }

  def addParticipant(user: QuizDuelUser) = {
    withPublishScoreboard {
      participants.find(_.userId == user.userId) match {
        case Some(_) =>
        case None    => participants = user :: participants
      }
    }
  }

  def skipTask() = LOCK synchronized {
    LOCK.notify()
  }

  def removeParticipant(user: User) = withPublishScoreboard {
    participants = participants.filter(user => user._id == user._id)
  }

  private def pushTask(task: TaskWithIdentifier) = withPublishTask(
    {
      currentTaskStartTime = System.currentTimeMillis
    },
    task
  )

  private def withPublishScoreboard(f: => Any) = {
    f
    controller.publish(ScoreboardEvent(participants, currentTask))
  }

  private def withPublishTask(f: => Any, task: TaskWithIdentifier) = {
    f
    controller.publish(TaskEvent(task, currentTaskDuration))
  }
}
