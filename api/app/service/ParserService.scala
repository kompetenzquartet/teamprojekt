package service

import javax.inject.{Inject, Singleton}
import lib.parser.Parser
import model.course.Course
import model.course.subs.{Chapter, Solution, Task, TaskData}
import parser.error.ParsingError
import parser.model.{FileContent, Meta, TestFile}
import parser.shared.State
import parser.utils.CodeTaskSuite
import play.api.Configuration
import play.api.libs.json._


import scala.util.{Failure, Success, Try}
import org.bson.types.ObjectId
import model.course.subs.SolutionWithoutId

@Singleton
class ParserService @Inject() (config: Configuration) {

  implicit val objectIdFormat: Format[ObjectId] = new Format[ObjectId] {
    def writes(objectId: ObjectId): JsValue =
      if (objectId != null) JsString(objectId.toString) else JsNull
    def reads(json: JsValue): JsResult[ObjectId] = json match {
      case JsString(s) if ObjectId.isValid(s) => JsSuccess(new ObjectId(s))
      case _                                  => JsError("Invalid ObjectId")
    }
  }

  @throws(classOf[IllegalStateException])
  @throws(classOf[ParsingError])
  def parse(
      filesContent: Map[String, List[(String, FileContent)]]
  ): List[Try[Course]] = {
    val parser = new Parser()

    val parsedFiles = filesContent
      .filterNot { case (folder, _) =>
        val splitFolder = folder.split("/")
        splitFolder.contains("java") || splitFolder.contains("org")
      }
      .map { case (folder, contents) =>
        try {
          folder -> contents.map(content =>
            content._2 match {
              case TestFile(file) =>
                try {
                  parser.parse(file)
                } catch {
                  case err: ParsingError =>
                    State.currentFile(s"${folder}/${content._1}")
                    throw err
                }
              case meta: Meta => meta
            }
          )
        } catch {
          case err: ParsingError => throw err
          case err: Throwable =>
            State.currentFile(s"${folder}/")
            throw err
        }
      }
    val courses: Map[String, Option[(Meta, List[CodeTaskSuite])]] =
      parsedFiles.map { case (folder, contents) =>
        try {
          folder -> (for {
            meta <- contents.find(_.isInstanceOf[Meta])
            content <- Some(
              contents
                .filter(_.isInstanceOf[List[CodeTaskSuite]])
                .asInstanceOf[List[List[CodeTaskSuite]]]
                .flatten
            )
          } yield (meta.asInstanceOf[Meta], content))
        } catch {
          case err: Throwable =>
            State.currentFile(s"${folder}/")
            throw err
        }
      }
    courses.map {
      case (folder, Some(course)) =>
        try {
          val chapters = Json
            .toJson(course._2.map(file => Json.parse(file.toJSON)))
            .as[JsArray]
            .value
            .map(chapter =>
              new Chapter(
                (chapter \ "id").get.as[String],
                (chapter \ "title").get.as[String],
                (chapter \ "tasks")
                  .as[JsArray]
                  .value
                  .map(task =>
                    if ((task \ "tag").get.as[String] == "koan-task") {
                      new Task(
                        (task \ "id").get.as[String],
                        (task \ "tag").get.as[String],
                        new TaskData(
                          code = (task \ "data" \ "code").get.as[String],
                          description =
                            (task \ "data" \ "description").get.as[String],
                          mode = (task \ "data" \ "mode").get.as[String],
                          solutions = (task \ "data" \ "solutions")
                            .as[List[String]]
                            .zipWithIndex
                            .map { case (solution, index) =>
                              new Solution(
                                index.toString,
                                solution
                              )
                            },
                          taskTime = (task \ "data" \ "taskTime")
                            .getOrElse(JsNumber(0))
                            .as[Int]
                        )
                      )
                    } else if (
                      (task \ "tag").get.as[String] == "reflection-task"
                    ) {
                      new Task(
                        (task \ "id").get.as[String],
                        (task \ "tag").get.as[String],
                        new TaskData(
                          description =
                            (task \ "data" \ "description").get.as[String]
                        )
                      )
                    } else if ((task \ "tag").get.as[String] == "info-task") {
                      new Task(
                        (task \ "id").get.as[String],
                        (task \ "tag").get.as[String],
                        new TaskData(
                          description =
                            (task \ "data" \ "description").get.as[String]
                        )
                      )
                    } else if ((task \ "tag").get.as[String] == "video-task") {
                      new Task(
                        (task \ "id").get.as[String],
                        (task \ "tag").get.as[String],
                        new TaskData(
                          description =
                            (task \ "data" \ "description").get.as[String],
                          url = (task \ "data" \ "url").get.as[String]
                        )
                      )
                    } else if (
                      (task \ "tag").get.as[String] == "multiple-choice-task"
                    ) {
                      new Task(
                        (task \ "id").get.as[String],
                        (task \ "tag").get.as[String],
                        new TaskData(
                          description =
                            (task \ "data" \ "description").get.as[String],
                          solutions = (task \ "data" \ "solutions")
                            .as[List[SolutionWithoutId]]
                            .zipWithIndex
                            .map { case (solution, index) =>
                              new Solution(
                                index.toString,
                                solution = solution.solution,
                                correctness = solution.correctness,
                              )
                            },
                          url = (task \ "data" \ "url")
                            .getOrElse(JsString(""))
                            .as[String]
                        )
                      )
                    } else if (
                      (task \ "tag").get.as[String] == "single-choice-task"
                    ) {
                      new Task(
                        (task \ "id").get.as[String],
                        (task \ "tag").get.as[String],
                        new TaskData(
                          description =
                            (task \ "data" \ "description").get.as[String],
                          solutions = (task \ "data" \ "solutions")
                            .as[List[SolutionWithoutId]]
                            .zipWithIndex
                            .map { case (solution, index) =>
                              new Solution(
                                index.toString,
                                solution = solution.solution,
                                correctness = solution.correctness,
                              )
                            },
                          url = (task \ "data" \ "url")
                            .getOrElse(JsString(""))
                            .as[String]
                        )
                      )
                    } else {
                      new Task(
                        (task \ "id").get.as[String],
                        (task \ "tag").get.as[String],
                        new TaskData(
                          code = (task \ "data" \ "code").get.as[String],
                          description =
                            (task \ "data" \ "description").get.as[String],
                          codeSolution =
                            (task \ "data" \ "codeSolution").get.as[String],
                          solutions = (task \ "data" \ "solutions")
                            .as[List[String]]
                            .zipWithIndex
                            .map { case (solution, index) =>
                              new Solution(
                                index.toString,
                                solution,
                              )
                            },
                          mode = (task \ "data" \ "mode").get.as[String],
                          taskTime = (task \ "data" \ "taskTime")
                            .getOrElse(JsNumber(0))
                            .as[Int]
                        )
                      )
                    }
                  )
                  .toList
              )
            )
            .toList

          Success(
            Course(
              course._1.id,
              course._1.title,
              course._1.description,
              chapters
            )
          )
        } catch {
          case err: Throwable =>
            State.currentFile(s"${folder}/")
            Failure(
              new IllegalStateException(
                s"${course._1.title}: ${err.getMessage}"
              )
            )
          case _: Throwable =>
            Failure(
              new Exception(
                "An unknown error occurred converting JSON to objects."
              )
            )
        }
      case (folder, _) =>
        Failure(
          new IllegalStateException(
            s"Meta or content in folder '${folder}' not found!"
          )
        )
    }.toList
  }
}
