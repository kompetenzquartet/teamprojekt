package service.random

class RandomCodeGeneratorImpl extends RandomCodeGenerator {

  override def generateCode(digits: Int): String = {
      val random = new scala.util.Random
      random.alphanumeric.filter(_.isDigit).take(digits).mkString
  }
}
