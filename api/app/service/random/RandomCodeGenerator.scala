package service.random

trait RandomCodeGenerator {
  /**
   * Generates a random code
   *
   * @param digits the number of digits the code should have
   * @return the generated code
   */
  def generateCode(digits: Int): String

}
