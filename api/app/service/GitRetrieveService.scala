package service

import java.net.URLEncoder
import java.util.concurrent.TimeoutException
import java.util.{Base64, NoSuchElementException}

import akka.actor.ActorSystem
import akka.stream.ActorMaterializer
import com.typesafe.config.{Config, ConfigFactory}
import javax.inject.{Inject, Singleton}
import parser.model.{FileContent, Meta, TestFile}
import play.api.libs.json.{JsArray, JsValue, Json}
import play.api.libs.ws.WSClient

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration._
import scala.concurrent.{Await, Future}

@Singleton
class GitRetrieveService @Inject() (ws: WSClient) {
  implicit val system = ActorSystem("http-client")
  implicit val materializer = ActorMaterializer()

  val config: Config = ConfigFactory.load
  val parserGitConfig: Config = config.getConfig("parser.github")

  val meta = parserGitConfig.getString("meta")
  val timeout = parserGitConfig.getInt("timeout")
  val branch = parserGitConfig.getString("branch")
  val token = parserGitConfig.getString("token")
  val resultsPerPage = parserGitConfig.getInt("results_per_page")
  val testFolderPath = parserGitConfig.getString("test_folder_path")
  val filesBaseUrl = parserGitConfig.getString("files_base_url")
  val gitFileTreeUrl = parserGitConfig.getString("file_tree_url")

  @throws(classOf[TimeoutException])
  @throws(classOf[NoSuchElementException])
  def retrieveKoanFiles(): Map[String, List[(String, FileContent)]] = {
    val request = ws
      .url(gitFileTreeUrl)
      .withQueryStringParameters(
        "path" -> s"$testFolderPath",
        "recursive" -> "true",
        "per_page" -> s"$resultsPerPage"
      )
      .withHttpHeaders("Private-Token" -> s"$token")

    val response: Future[JsArray] = request.get().map {
      _.json.as[JsArray]
    }

    val gitObjects: List[JsValue] =
      Await.result(response, timeout.millis).value.toList
    val blobs: List[JsValue] = retrieveAllBlobs(gitObjects)
    val folderFiles: Map[String, List[Option[(JsValue, JsValue)]]] =
      getFolderFiles(blobs)

    getFilesContent(folderFiles)
  }

  private def getFolderFiles(
      blobs: List[JsValue]
  ): Map[String, List[Option[(JsValue, JsValue)]]] = {
    var folderFiles = Map.empty[String, List[Option[(JsValue, JsValue)]]]

    blobs.foreach(blob => {
      val fileName = (blob \ "name").get.toString
      val path = (blob \ "path").get.toString.dropRight(fileName.length - 1)
      folderFiles += path -> folderFiles
        .getOrElse(path, List.empty)
        .::(
          Option(
            (blob \ "name").get,
            Json
              .toJson(buildGitFileUrl((blob \ "path").get.as[String].toString))
          )
        )
    })
    folderFiles
  }

  @throws(classOf[NoSuchElementException])
  private def retrieveAllBlobs(gitObjects: List[JsValue]): List[JsValue] = {
    gitObjects.filter(content =>
      (content \ "type").toOption match {
        case Some(t) => t.as[String] == "blob"
        case None    => throw new NoSuchElementException("File type not found!")
      }
    )
  }

  @throws(classOf[TimeoutException])
  @throws(classOf[NoSuchElementException])
  private def getFilesContent(
      folderFiles: Map[String, List[Option[(JsValue, JsValue)]]]
  ): Map[String, List[(String, FileContent)]] = {
    folderFiles.map { case (folder, files) =>
      folder -> files.map {
        case Some((fileName, fileUrl)) =>
          val request = ws
            .url(fileUrl.as[String])
            .withHttpHeaders("Private-Token" -> s"$token")
            .withRequestTimeout(timeout.millis)
          val response: Future[JsValue] = request.get().map {
            _.json
          }

          (Await.result(response, timeout.millis) \ "content").toOption match {
            case Some(content) =>
              val file = new String(
                Base64.getDecoder.decode(
                  content.as[String].replaceAll("\n", "")
                )
              )
              if (fileName.as[String] == meta) {
                parseMetaFile(fileName, file)
              } else {
                (fileName.as[String], TestFile(file))
              }
            case None =>
              throw new NoSuchElementException("File has no content!")
          }
        case None =>
          throw new NoSuchElementException(
            s"File name or url could not be parsed!"
          )
      }
    }
  }

  @throws(classOf[NoSuchElementException])
  private def parseMetaFile(
      fileName: JsValue,
      file: String
  ): (String, FileContent) = {
    val jsonFile = Json.parse(file)
    val meta = for {
      id <- (jsonFile \ "id").toOption
      title <- (jsonFile \ "title").toOption
      description <- (jsonFile \ "description").toOption
    } yield (id.as[Long], title.as[String], description.as[String])
    meta match {
      case Some((id, title, description)) =>
        (fileName.as[String], Meta(id.toString, title, description))
      case None => throw new NoSuchElementException("Meta could not be parsed!")
    }
  }

  private def buildGitFileUrl(filePath: String): String = {
    s"$filesBaseUrl${URLEncoder.encode(filePath, "UTF-8")}?ref=$branch"
  }
}
