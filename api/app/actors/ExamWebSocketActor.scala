package actors

import akka.actor.{Actor, ActorRef, Props}
import controllers.impl.ExamController
import model.exam.ExamStartEvent
import model.user.User
import play.api.libs.json.{JsValue, Json}

object ExamWebSocketActorFactory {
  def create(controller: ExamController, out: ActorRef, user: User): Props = {
    Props(new ExamWebSocketActor(controller, out, user))
  }
}

class ExamWebSocketActor(controller: ExamController, out: ActorRef, user: User) extends Actor {
  override def receive: Receive = {
    case msg: JsValue =>
      val msgType = (msg \ "type").as[String]
      msgType match {
        case "subscribe" => controller.addSubscriber(user, out)
        case _ => out ! Json.obj("msg" -> "Not a valid message type")
      }
    case _ => out ! Json.obj("msg" -> "The Json request is not in a valid format")
  }

  override def postStop(): Unit = controller.removeSubscriber(user)
}
