package actors

import akka.actor.{Actor, ActorRef, Props}
import controllers.impl.QuizDuelController
import model.quiz.{FinishEvent, TaskEvent, ScoreboardEvent}
import model.user.User
import play.api.libs.json.{JsValue, Json}

import scala.swing.Reactor
import model.course.subs.TaskIdentifier
import model.quiz.QuizDuelSubmitMessage

object QuizDuelWebSocketActorFactory {
  def create(controller: QuizDuelController, out: ActorRef, user: User): Props = {
    Props(new QuizduellWebsocketActor(controller: QuizDuelController, out, user))
  }
}

class QuizduellWebsocketActor(controller: QuizDuelController, out: ActorRef, user: User) extends Actor with Reactor {
  listenTo(controller)

  override def receive: Receive = {
    case msg: JsValue =>
      val msgType = (msg \ "type").as[String]

      msgType match {
        case "submit" => controller.submitSolution(user, (msg \ "msg").as[QuizDuelSubmitMessage])
        case _ => out ! Json.obj("msg" -> "Not a valid message type")
      }
    case _ => out ! Json.obj("msg" -> "The Json request is not in a valid format")
  }

  override def preStart(): Unit = controller.addParticipant(user)

  override def postStop(): Unit = controller.removeParticipant(user)

  reactions += {
    case event: TaskEvent => out ! Json.toJson(event)
    case event: ScoreboardEvent => out ! Json.toJson(event)
    case event: FinishEvent => out ! Json.toJson(event)
    case _ => out ! Json.obj("msg" -> "Error in communication from server to frontend in quizduell actor")
  }
}
