name := "codetaskAPI"
organization := "de.htwg-konstanz"

version := "1.0-SNAPSHOT"

lazy val codetaskAPI = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.4"

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"



libraryDependencies += guice
libraryDependencies += ws
libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.5.23",
  "com.typesafe.akka" %% "akka-stream" % "2.5.23",
  "com.typesafe.akka" %% "akka-slf4j" % "2.5.23",
  "com.typesafe.akka" %% "akka-remote" % "2.5.23",
  "com.typesafe.akka" %% "akka-protobuf" % "2.5.23",
)
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.3" % Test
libraryDependencies += "org.mongodb.scala" %% "mongo-scala-driver" % "2.7.0"
libraryDependencies += "com.github.simplyscala" %% "scalatest-embedmongo" % "0.2.4"
libraryDependencies += "org.mindrot" % "jbcrypt" % "0.4"
libraryDependencies += "com.pauldijou" %% "jwt-play-json" % "4.2.0"
libraryDependencies += "com.lightbend.play" %% "play-socket-io" % "1.0.0-beta-2"
libraryDependencies += "org.json4s" %% "json4s-jackson" % "3.6.3"
libraryDependencies += "org.scala-lang.modules" %% "scala-swing" % "2.1.1"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1"
