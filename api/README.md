# API

## How to import

[Wiki-Eintrag](https://gitlab.com/kompetenzquartet/teamprojekt/wikis/Import-API-into-Intellij)

## Parser.scala

_Links zur Dokumentation der verwendeten Bibliotheken:_ <br>
https://docs.oracle.com/javase/8/docs/api/java/util/regex/Pattern.html <br>
https://www.scala-lang.org/api/2.13.4/scala/util/matching/Regex.html <br>
https://www.scala-lang.org/api/2.12.3/scala-parser-combinators/scala/util/parsing/combinator/RegexParsers.html <br>
https://www.scala-lang.org/api/2.12.3/scala-parser-combinators/scala/util/parsing/input/CharArrayReader.html <br>

_Schnelllese Hilfe:_ <br>
"\\\s" ist das selbe wie """\s""" da innerhalb einfacher Hochkommas ein zweites \ erforderlich ist um zu Escapen <br>
.r Wandelt den Regex-String in ein scala.util.matching.Regex-Objekt um. <br>
\\s\* Beliebige Anzahl von Leerzeichen (einschließlich keines). <br>
\\( Öffnende Klammer "(". <br>
\\) Schließende Klammer ")". <br>

_Parser-Kombinatoren aus der Scala-Parserbibliothek:_ <br>
~ Definiert eine Sequenz, dass die linke Seite des ~ Symbols gefolgt von der rechten Seite des ~ Symbols geparst wird. Wenn beides Erfolgreich ist, gibt's es ein Tuple zurück, Ansonsten schlägt die gesamte Sequenz fehl. <br>
<~ Verbindet zwei Parser, führt sie nacheinander aus und gibt das Ergebnis des ersten Parsers zurück. <br>
~> Verbindet zwei Parser, führt sie nacheinander aus und gibt das Ergebnis des zweiten Parsers zurück. <br>
^^ Transformiert das Ergebnis eines Parsers, indem er eine Funktion darauf anwendet. <br>
