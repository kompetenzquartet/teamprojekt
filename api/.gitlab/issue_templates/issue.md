**Feature: <NAME-OF-FEATURE>**

* Scenario: <What is the feature about>
    * **Given** something happens or someone is somewhere
    * **When** something or someone does something
    * **Then** something happens
