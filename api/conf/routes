# Authentication

POST          /auth/htwg                        controllers.impl.UserController.signinHtwg()
POST          /auth/signup                      controllers.impl.UserController.signupCasualUser()
POST          /auth/signin                      controllers.impl.UserController.signinCasualUser()
POST          /auth/refresh/:id                 controllers.impl.UserController.refreshAccessToken(id: String)
GET           /auth/user                        controllers.impl.UserController.getUserFromToken
GET           /auth/is-authenticated            controllers.impl.UserController.isAuthenticated

# User

GET           /users                            controllers.impl.UserController.list
GET           /users/:id                        controllers.impl.UserController.get(id: String)
GET           /users/:id/settings               controllers.impl.UserController.getSettings(id: String)
PATCH         /users/:id                        controllers.impl.UserController.update(id: String)
PATCH         /users/:id/settings               controllers.impl.UserController.updateSettings(id:String)

# Lectures

GET           /lectures                         controllers.impl.LectureController.list
GET           /lectures/:id                     controllers.impl.LectureController.get(id: String)
GET           /users/:userId/semesters/:semesterId/lectures                                controllers.impl.LectureController.getSubscribedLectures(userId: String, semesterId: String)
GET           /users/:userId/semesters/:semesterId/lectures/:lectureId/courses             controllers.impl.LectureController.getSubscribedLectureCourses(userId: String, semesterId: String, lectureId: String)
POST          /lectures                         controllers.impl.LectureController.create
PATCH         /lectures/:id                     controllers.impl.LectureController.update(id: String)
DELETE        /lectures/:id                     controllers.impl.LectureController.delete(id: String)

# Semesters

GET           /semesters                        controllers.impl.SemesterController.list
GET           /semesters/view                   controllers.impl.SemesterController.listView
GET           /semesters/:id                    controllers.impl.SemesterController.get(id: String)
POST          /semesters                        controllers.impl.SemesterController.create
PATCH         /semesters/:id                    controllers.impl.SemesterController.update(id: String)
DELETE        /semesters/:id                    controllers.impl.SemesterController.delete(id: String)

# Courses

GET           /courses                          controllers.impl.CourseController.list
GET           /courses/get-code-solution        controllers.impl.CourseController.getCodeSolution(courseId: String, chapterId: String, taskId: String)
POST          /courses/check-codetask           controllers.impl.CourseController.checkCodeTask
GET           /courses/:id                      controllers.impl.CourseController.get(id: String)
PATCH         /courses/:id                      controllers.impl.CourseController.updateLectures(id: String)
GET           /courses/:id/chapter/:chapterId   controllers.impl.CourseController.getChapter(id: String, chapterId: String)
GET           /courses/subscribed/list          controllers.impl.CourseController.listSubscribedCoursesWithoutSolutions

# Subscriptions

GET           /subscriptions                    controllers.impl.SubscriptionController.list
GET           /subscriptions/:id                controllers.impl.SubscriptionController.get(id: String)
POST          /subscriptions                    controllers.impl.SubscriptionController.create
DELETE        /subscriptions/:id                controllers.impl.SubscriptionController.delete(id: String)
GET           /subscriptions/user/:userId       controllers.impl.SubscriptionController.listUserSubscriptions(userId: String)

# Settings

GET           /settings                         controllers.impl.SettingsController.get
PATCH         /settings                         controllers.impl.SettingsController.update

# QuizTaskSets

GET           /quiztasksets                     controllers.impl.QuizTaskSetController.list
GET           /quiztasksets/:id                 controllers.impl.QuizTaskSetController.get(id: String)
PATCH         /quiztasksets/:id                 controllers.impl.QuizTaskSetController.update(id: String)
POST          /quiztasksets                     controllers.impl.QuizTaskSetController.create
DELETE        /quiztasksets/:id                 controllers.impl.QuizTaskSetController.delete(id: String)

# Parser

POST          /synchronize                      controllers.impl.ParserController.parseKoansRepository
POST          /webhook                          controllers.impl.ParserController.webhookParse

# Quizduel

GET           /quizduel                        controllers.impl.QuizDuelController.getActive
GET           /quizduel/connect                controllers.impl.QuizDuelController.connect

GET           /quizduels                       controllers.impl.QuizDuelController.list
GET           /quizduels/original              controllers.impl.QuizDuelController.listOriginal
POST          /quizduels                       controllers.impl.QuizDuelController.create
GET           /quizduels/:id                   controllers.impl.QuizDuelController.get(id: String)
DELETE        /quizduels/:id                    controllers.impl.QuizDuelController.delete(id: String)
POST          /quizduels/:id/start             controllers.impl.QuizDuelController.start(id: String)
POST          /quizduels/:id/stop              controllers.impl.QuizDuelController.stop(id: String)
POST          /quizduel/skip-task              controllers.impl.QuizDuelController.skipTask

# TaskProgress

GET           /taskProgresses                    controllers.impl.TaskProgressController.list
GET           /taskProgresses/user/:userId             controllers.impl.TaskProgressController.listUserTaskProgresses(userId: String)
GET           /taskProgresses/user/:userId/semester/:semesterId/lecture/:lectureId/course/:courseId/chapter/:chapterId                 controllers.impl.TaskProgressController.listUserChapterTaskProgresses(userId: String, semesterId: String, lectureId: String, courseId: String, chapterId: String)
POST          /taskProgresses                     controllers.impl.TaskProgressController.create
PATCH         /taskProgresses/:id                 controllers.impl.TaskProgressController.update(id: String)

# QuizduellDeadline

POST          /quiz-duel-deadline                controllers.impl.QuizDuelDeadlineController.create
PUT           /quiz-duel-deadline/:id            controllers.impl.QuizDuelDeadlineController.update(id: String)
DELETE        /quiz-duel-deadline/:id            controllers.impl.QuizDuelDeadlineController.delete(id: String)
GET           /quiz-duel-deadlines               controllers.impl.QuizDuelDeadlineController.list
GET           /quiz-duel-deadlines/subscribed    controllers.impl.QuizDuelDeadlineController.listSubscribedQuizDuelDeadlines

# CourseDeadline

POST          /course-deadline                   controllers.impl.CourseDeadlineController.create
PUT           /course-deadline/:id               controllers.impl.CourseDeadlineController.update(id: String)
DELETE        /course-deadline/:id               controllers.impl.CourseDeadlineController.delete(id: String)
GET           /course-deadlines                  controllers.impl.CourseDeadlineController.list
GET           /course-deadlines/subscribed       controllers.impl.CourseDeadlineController.listSubscribedCourseDeadlines


# Exam

POST          /exam                             controllers.impl.ExamController.create
GET           /exam                             controllers.impl.ExamController.list
GET           /exam/:id                         controllers.impl.ExamController.get(id: String)
GET           /exam/user/:userId                controllers.impl.ExamController.listUserExams(userId: String)
GET           /exam/:examId/user/:userId        controllers.impl.ExamController.getUserExam(examId, userId)
DELETE        /exam/:id                         controllers.impl.ExamController.delete(id: String)
PUT           /exam/:id                         controllers.impl.ExamController.update(id: String)
PUT           /exam/assign-tasks/:id            controllers.impl.ExamController.assignTasks(id: String)
PUT           /exam/start/:examId               controllers.impl.ExamController.startExam(examId: String)



POST          /exam-solution/auth/:examId       controllers.impl.UserExamSolutionController.authorizeUser(examId)
POST          /exam-solution                    controllers.impl.UserExamSolutionController.create
GET           /exam-solution/:id                controllers.impl.UserExamSolutionController.get(id: String)
GET           /exam-solution                    controllers.impl.UserExamSolutionController.list
DELETE        /exam-solution/:id                controllers.impl.UserExamSolutionController.delete(id: String)
PUT           /exam-solution/:id                controllers.impl.UserExamSolutionController.update(id: String)
PUT           /exam-solutions/                  controllers.impl.UserExamSolutionController.updateMany()
GET           /exam-solution/user/:examId       controllers.impl.UserExamSolutionController.listUserExamSolutions(examId: String)
GET           /exam-solution/exam/:examId       controllers.impl.UserExamSolutionController.listGroupedByUser(examId: String)

GET            /examws/connect                controllers.impl.ExamController.connect

# Map static resources from the /public folder to the /assets URL path

GET           /assets/*file                     controllers.Assets.versioned(path="/public", file: Asset)
