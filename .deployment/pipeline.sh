#!/bin/sh

SUCCEDED=$(curl -H "X-Gitlab-Token:$DEPLOY_TOKEN" -X POST $DEPLOY_URL | grep "Executing deployment" | wc -l)

if [ "$SUCCEDED" != 1 ] ; then
  echo "Deployment failed!"
  exit 1
fi

echo "Deployment succeeded"
