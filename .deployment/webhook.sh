#!/bin/sh

DEPLOYMENT_FOLDER="teamprojekt/.deployment/"

echo "Create necessary folders"
mkdir -p .traefik
mkdir -p .nginx
mkdir -p hooks

echo "Clone repository"
git clone git@gitlab.com:kompetenzquartet/teamprojekt.git

echo "Copying and preparing files (if not existent)"
cp teamprojekt/.env.dist ./.env
cp -n ${DEPLOYMENT_FOLDER}.traefik/acme.json ./.traefik/acme.json
cp -n ${DEPLOYMENT_FOLDER}/hooks/hooks.json ./hooks/hooks.json

chmod 600 ./.env
chmod 600 ./.traefik/acme.json

echo "Copying .deployment files into main folder"
cp ${DEPLOYMENT_FOLDER}docker-compose.yml ./docker-compose.yml
cp ${DEPLOYMENT_FOLDER}mongo-init.js ./mongo-init.js
cp ${DEPLOYMENT_FOLDER}.traefik/traefik.yml ./.traefik/traefik.yml
cp ${DEPLOYMENT_FOLDER}.nginx/frontend.conf ./.nginx/frontend.conf

echo "Removing cloned folder"
rm -rf teamprojekt

echo "Pulling new containers";
docker-compose pull;

echo "Shutting down containers";
docker-compose down;

echo "Starting containers";
docker-compose up -d;

echo "Cleaning up old images"
docker image prune -f
