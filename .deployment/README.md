# Why is this here?

This folder is copied to the live server.
Since we can't push anything to the server directly (via ssh) we need to use a webhook to fetch new images and restart the containers.
The files in this folder are placed on the server upfront. If you want to make changes to those login to the server via ssh and change the files there (be careful though)

## Special files

During deployment all files are cloned to the server and mostly all files are replaced on the server. An exception are the following files:

- `.traefik/acme.json`
- `hooks/hooks.json`
- `.env.dist` will be renamed to `.env`

Those three files are copied only once and will not overwrite already existing files on the server since these files hold credentials and SSL keys.

> Overwriting those files or changing them without further thinking can lead to a broken application. So be careful if you want to change any of these.

## Change the webhook secret token

The file [hooks.json](hooks/hooks.json) can be changed (on the server). The secret token can be found [here](https://gitlab.com/kompetenzquartet/teamprojekt/-/settings/ci_cd) under **Variables**. Usually it shouldn't be necessary to change this file.

In case you wanna change it, change the line **<YOUR-GENERATED-TOKEN>**:

```json
...
"match":
  {
    "type": "value",
    "value": "<YOUR-GENERATED-TOKEN>",
    ...
  }
```

in the `hooks.json` on the live server.

> Do not store the secret token in the git repository. Means: Do not add the secret token inside the hooks.json file locally and then add it to the tracked files. Since this repository is public this could become a security issue.
