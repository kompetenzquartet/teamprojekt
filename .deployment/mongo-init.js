const MONGO_DATABASE = process.env.MONGO_INITDB_DATABASE;
const MONGO_USERNAME = process.env.MONGO_INITDB_ROOT_USERNAME;
const MONGO_PASSWORD = process.env.MONGO_INITDB_ROOT_PASSWORD;

db = db.getSiblingDB(process.env.MONGO_INITDB_DATABASE);

db.createUser({
    user: MONGO_USERNAME,
    pwd: MONGO_PASSWORD,
    roles: [
        {
            role: "readWrite",
            db: MONGO_DATABASE
        },
        {
            role: "dbAdmin",
            db: MONGO_DATABASE
        }
    ]
});