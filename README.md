# Codetask Main Project

Teamproject for HTWG Konstanz

## Development

> To prevent you from struggle to get something printed into the api or check-api console in Docker: **Use Console.println() instead of println()** in your Scala code.

### Setup

1. `git clone git@gitlab.com:kompetenzquartet/teamprojekt.git`
2. You **need** to be in the HTWG VPN to be able to login since the login is via LDAP

> Make sure to install **Docker** and **Docker-Compose** on your machine

### Install frontend dependencies

1. `cd frontend`
2. `npm install`

> You'll need **node** on your local machine. For linux you can use the [Node Version Manager](https://github.com/nvm-sh/nvm#install--update-script). The used version in the project is **Node v10.16.3**

> You may encounter problems during development that files are not modifiable. This happens because docker possibly creates files in the node_modules folder that are assigned to the root user. To fix this problem invoke the command: `sudo chown -R $USER:$USER .` in the root folder of the project.

### Default credentials

You can find the default credentials in the [.env.dist](.env.dist) file. Those credentials can be used only during local development.

1. Make sure to make a copy of the `.env.dist` file into the same directory and name it `.env` otherwise the containers won't startup.

> Make sure a .env file exists in the project folder on the server with real credentials. This should be done already but needs to be redone in case the `.env` file was removed (for whatever reason)

### Startup containers

1. Run `docker-compose up -d`

> On the first time docker will build the images. This can take a while.

> When adding new dependencies for scala it is good to run `docker-compose build` to rebuild the images with the new dependencies to reduce the startup time of the containers.

### Frontend hotreloade

### Addresses

- **Frontend** at [http://localhost:8000/](http://localhost:8000/)
- **REST API** at [http://api.localhost:8000](http://api.localhost:8000)
- **CHECK API** at [http://check-api.localhost:8000](http://check-api.localhost:8000)
- **Mongo Express** at [http://localhost:8081](http://localhost:8081)

## Deployment

We use **gitlab pipelines** to deploy the software. The [.gitlab-ci.yml](.gitlab-ci.yml) can be modified if anything needs to be changed about what happens in the pipeline.

`[Status 19.02.2020]`

- For now we do only build the containers and push them to the repositories [container registry](https://gitlab.com/kompetenzquartet/teamprojekt/container_registry)
- A webhook is send to our server which triggers the pulling of the newly built containers and a restart of those.

### First time deployment

When deploying for the first time make sure to first get onto the server.
Clone this repository on the server and copy the [webhook.sh](.deployment/webhook.sh) file into the project folder on the server.

### How to start deployment

To start a deployment to the server you need to tag your commit and push that tag on the repository.

1. `git tag x.x.x`
2. `git push x.x.x`

That will start the pipeline.

### Credentials

We use **gitlabs webhooks** to inform our server about changes.

#### Codetask Koans Hook

We have a webhook when a push to the `master` branch happens in the [Codetask Koans](https://gitlab.com/kompetenzquartet/codetask-koans) repository.

The secret token that is validated in the api can be found [here](https://gitlab.com/kompetenzquartet/codetask-koans/hooks/1616009/edit)

#### Deployment Hook

When the build process finishes in the pipeline a final job sends an request to our webhook.

The secret token and token url is validated on the server and can be found [here](https://gitlab.com/kompetenzquartet/teamprojekt/-/settings/ci_cd) under **Variables**

## Create an admin user

This works both for development and in live.

1. After a successful login via the HTWG-VPN, the user is stored in the Mongo DB.
2. Access the web interface of Mongo Express to the users. By default at: http://localhost:8081/db/codetask/users
3. Find the user you want and change the role to "admin"

> To do this in live (deployed) connect via ssh to the server then run the commands in the project folder

## Further Documentation

You can find other documentation on the repositories [Wiki Page](https://gitlab.com/kompetenzquartet/teamprojekt/wikis/home)
