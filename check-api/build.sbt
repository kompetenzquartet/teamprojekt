name := "codetaskCheckAPI"
organization := "de.htwg-konstanz"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.15"

resolvers += "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "4.0.3" % Test
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.1"
libraryDependencies += "org.scala-lang" % "scala-reflect" % scalaVersion.value
libraryDependencies += "org.scala-lang" % "scala-compiler" % scalaVersion.value

PlayKeys.devSettings += "play.server.http.port" -> "9001"

coverageEnabled := false

coverageExcludedFiles := ".*Routes.*"
coverageExcludedPackages := "router.*"



