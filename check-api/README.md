# Codetask Check API
This repository is a simple API that receives a string from the main API of the Codetask project and runs the string as a test. It then returns if the test was successful or not.

## Configuration
The project itself should be configured already. Although you could get in trouble when deploying the app.

### Host not allowed
You can fix this by editing the [application.conf](conf/application.conf) file and add the necessary host in
```
play.filters.hosts {
  allowed = [".check-api:9000", "<another-host>"]
}
```
