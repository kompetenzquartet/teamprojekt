import controllers.ScalaController
import org.scalatestplus.play._
import play.api.test._
import play.api.test.Helpers._
import play.api.libs.json.Json
import utils.CodeTaskTesting

import scala.util.{Failure, Success}

class ScalaControllerSpec extends PlaySpec {

  "ScalaController" should {

    "return OK for valid code" in {
      val controller = new ScalaController(stubControllerComponents())

      val request = FakeRequest(POST, "/").withJsonBody(Json.obj("code" -> "1 + 1 shouldEqual 2"))
      val result = controller.index().apply(request)

      status(result) mustBe OK
    }

    "return BadRequest for invalid code" in {
      val controller = new ScalaController(stubControllerComponents())

      val request = FakeRequest(POST, "/").withJsonBody(Json.obj("code" -> "1 / 0 shouldEqual 0"))
      val result = controller.index().apply(request)

      status(result) mustBe BAD_REQUEST
      contentAsString(result) must include ("/ by zero")
    }

    "return BadRequest for missing code" in {
      val controller = new ScalaController(stubControllerComponents())

      val request = FakeRequest(POST, "/")
      val result = controller.index().apply(request)

      status(result) mustBe BAD_REQUEST
      contentAsString(result) must include ("Missing code to check.")
    }

  }
}
