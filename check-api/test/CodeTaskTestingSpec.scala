import org.scalatest.FlatSpec
import org.scalatest.Matchers
import utils.CodeTaskTesting
import scala.util.{Success, Failure}
import scala.tools.reflect.ToolBoxError

class CodeTaskTestingSpec extends FlatSpec with Matchers {

  "CodeTaskTesting" should "return Success for valid code" in {
    val codeTester = new CodeTaskTesting()
    val result = codeTester.testCode("1 + 1 shouldEqual 2")
    result shouldBe a [Success[_]]
  }

  it should "return Failure for invalid code" in {
    val codeTester = new CodeTaskTesting()
    val result = codeTester.testCode("1 / 0 shouldEqual 0")
    result shouldBe a [Failure[_]]
  }

  it should "return Failure for ToolBoxError" in {
    val codeTester = new CodeTaskTesting()
    val result = codeTester.testCode("object invalid code")
    result shouldBe a [Failure[_]]
    result.failed.get shouldBe a [Error]
  }

}
