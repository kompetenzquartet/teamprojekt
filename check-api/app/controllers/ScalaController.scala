package controllers

import javax.inject._
import play.api.libs.json.Json
import play.api.mvc._
import utils.CodeTaskTesting

import scala.util.{Failure, Success}

@Singleton
class ScalaController @Inject()(val controllerComponents: ControllerComponents) extends BaseController {
  def index(): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    request.body.asJson match {
      case Some(bodyContent) =>
        val code = (bodyContent \ "code").get.as[String]
        val codeTester = new CodeTaskTesting()
        codeTester.testCode(code) match {
          case Success(value) => Ok("")
          case Failure(exception) => BadRequest(exception.getMessage)
        }
      case None => BadRequest(Json.toJson("Missing code to check."))
    }
  }
}
