package utils

import java.lang.reflect.InvocationTargetException

import scala.reflect.runtime.universe
import scala.tools.reflect.{ToolBox, ToolBoxError}
import scala.util.{Failure, Success, Try}

/** Defines the testCode method in the CodeTaskTesting class.
  *
  * This testCode method takes a string parameter with code as input and returns
  * a Try[Unit] result. The method uses Scala reflection to compile and execute
  * the given code by creating a toolbox, generating a test class where the
  * given code is executed, and calling the execute method of the test class. If
  * an error occurs while executing the code, an error is returned in a Failure
  * object. If everything goes well, a Success object is returned."
  */
class CodeTaskTesting {
  def testCode(code: String): Try[Unit] = {
    val cm = universe.runtimeMirror(getClass.getClassLoader)
    val tb = cm.mkToolBox()
    val testString =
      s"""
         |import org.scalatest._
         |import scala.util.{Failure, Success, Try}
         |class Testing extends FlatSpec with Matchers {
         |  ${code}
         |}
         |new Testing().execute()
         |""".stripMargin
    try {
      tb.eval(tb.parse(testString))
      Success()
    } catch {
      case e: InvocationTargetException =>
        Failure(new Error(e.getCause.getMessage))
      case e: ToolBoxError => Failure(new Error(e.message))
      case _: Throwable    => Failure(new Error("Unbekannter Fehler"))
    }
  }
}
